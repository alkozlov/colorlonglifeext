﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using ColorLongLife.DataAccessLayer.Models;
using NUnit.Framework;
using SpreadsheetGear;
using SpreadsheetGear.Data;

namespace ColorLongLife.DataAccessLayer.Tests
{
    [TestFixture]
    [Ignore("Only for local")]
    public class ExcelParser
    {
        Int32 female = 0;
        Int32 male = 1;

        [Test]
        public async Task Emotions()
        {
            IWorkbook workbook =
                SpreadsheetGear.Factory.GetWorkbook(AppDomain.CurrentDomain.BaseDirectory +
                                                    @"\ExcelParserFiles\Categories.xlsx");
            DataSet dataSet = workbook.GetDataSet(GetDataFlags.FormattedText);
            DataTable dataTable = dataSet.Tables[0];
            List<CategoryImportRow> importRows = new List<CategoryImportRow>();
            foreach (DataRow dataRow in dataTable.Rows)
            {
                CategoryImportRow row = new CategoryImportRow
                {
                    Category = ((string) dataRow[0]).Trim(),
                    Subcategory = ((string) dataRow[1]).Trim(),
                    Gender = ((string) dataRow[2]).Trim()
                };
                importRows.Add(row);
            }

            List<string> categoriesNames = importRows.Select(x => x.Category).Distinct().ToList();
            List<string> genders = importRows.Select(x => x.Gender).Distinct().ToList();
            List<Category> categories = new List<Category>();
            foreach (string categoryName in categoriesNames)
            {
                Category category = new Category
                {
                    Name = categoryName,
                    Subcategories = new List<Subcategory>()
                };
                List <Subcategory> subcategories = new List<Subcategory>();
                foreach (string gender in genders)
                {
                    List<Subcategory> subc =
                        importRows.Where(x => x.Category.Equals(categoryName) && x.Gender.Equals(gender))
                            .Select(y => new Subcategory
                            {
                                Name = y.Subcategory,
                                Gender = this.ConvertToGenderInt(y.Gender)
                            }).ToList();
                    subcategories.AddRange(subc);
                }
                category.Subcategories = subcategories;
                categories.Add(category);
            }
            UnitOfWork _unitOfWork = new UnitOfWork();
            _unitOfWork.CategoryRepository.InsertRange(categories);
            await _unitOfWork.SaveAsync();
        }

        private Int32 ConvertToGenderInt(string genderString)
        {
            switch (genderString)
            {
                case "Женщина":
                    return this.female;

                case "Мужчина":
                    return this.male;

                default:
                    return this.female;
            }
        }
    }
}