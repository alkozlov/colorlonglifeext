﻿using System;

namespace ColorLongLife.DataAccessLayer.Tests
{
    public class CategoryImportRow
    {
        public String Category { get; set; }
        public String Subcategory { get; set; }
        public String Gender { get; set; } 
    }
}