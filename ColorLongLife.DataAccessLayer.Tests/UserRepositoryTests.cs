﻿using System;
using System.Collections.Generic;
using System.Runtime.Remoting;
using System.Threading.Tasks;
using ColorLongLife.DataAccessLayer.Models;
using NUnit.Framework;

namespace ColorLongLife.DataAccessLayer.Tests
{
    [TestFixture]
    [Ignore("Only for local")]
    public class UserRepositoryTests
    {
        //Тест для проверки создания админа

        [Test]
        [Ignore("Only for local")]
        public async Task CreateUserAsync()
        {
            try
            {
                User user = new User()
                {
                    Birthday = DateTime.UtcNow,
                    CreateDate = DateTime.UtcNow,
                    Email = "admin@colorlonglife.ru",
                    FirstName = "Федор",
                    LastName = "Иванов",
                    MiddleName = "Александрович",
                    Gender = 1,
                    Growth = 185,
                    Weight = 75,
                    UserAccountType = 1,
                    UserStatus = 1,
                    LastVisitDate = DateTime.UtcNow,
                    Phone = "+375442569847",
                    UserPasswordHash = "JfnnlDI7RTiF9RgfG2JNCw==",
                    IsDeleted = false
                };

                UnitOfWork unitOfWork = new UnitOfWork();

                unitOfWork.UserRepository.Insert(user);
                await unitOfWork.SaveAsync();
            }
            catch (RemotingException ex)
            {
                Console.WriteLine(ex);
                Assert.Fail();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Assert.Fail();
            }
        }

        [Test]
        public async Task AddictionsTest()
        {
            List<Addiction> addictions = new List<Addiction>
            {
                new Addiction { Title = "Болезненная зацикленность на деньгах" },
                new Addiction { Title = "Гордыня" },
                new Addiction { Title = "Живет в прошлом, не может его отпустить" },
                new Addiction { Title = "Жизненные проблемы быстро выбивают из равновесия" },
                new Addiction { Title = "Завышенные требования к партнеру" },
                new Addiction { Title = "Меркантильность" },
                new Addiction { Title = "Много думает, мало делает" },
                new Addiction { Title = "Нежелание серьезных отношений из-за боязни брать на себя ответственность" },
                new Addiction { Title = "Не может прощать обиды прошлого" },
                new Addiction { Title = "Некорректный стиль общения с людьми" },
                new Addiction { Title = "Непонимание себя, своих чувств и желаний" },
                new Addiction { Title = "Нереализованные любовные мечты" },
                new Addiction { Title = "Нерешительность в делах" },
                new Addiction { Title = "Неуверенность в  себе" },
                new Addiction { Title = "Неумение идти на компромисс" },
                new Addiction { Title = "Подверженность скуке и плохому настроению" },
                new Addiction { Title = "Проблемы в любовных отношениях" },
                new Addiction { Title = "Разрушительная эмоциональность" },
                new Addiction { Title = "Ревность" },
                new Addiction { Title = "Сильное влияние матери на вас" },
                new Addiction { Title = "Склонность диктовать свои условия" },
                new Addiction { Title = "Склонность доминировать над партнером" },
                new Addiction { Title = "Склонность к изменам" },
                new Addiction { Title = "Склонность к корысти в любовных отношениях" },
                new Addiction { Title = "Склонность к свободным отношениям" },
                new Addiction { Title = "Склонность контролировать партнера" },
                new Addiction { Title = "Склонность раздражаться на людей" },
                new Addiction { Title = "Склонность резко и язвительно общаться" },
                new Addiction { Title = "Склонность спорить и конфликтовать" },
                new Addiction { Title = "Склонность транжирить деньги" },
                new Addiction { Title = "Страх денежных проблем" },
                new Addiction { Title = "Стремление к высокому социальному статусу" },
                new Addiction { Title = "Стремление к удовольствиям любой ценой" },
                new Addiction { Title = "Финансовые сложности в жизни" },
                new Addiction { Title = "Чрезмерная самоуверенность" },
                new Addiction { Title = "Эгоцентризм" },
                new Addiction { Title = "Эмоциональная напряженность" },
                new Addiction { Title = "Эмоциональная холодность и безразличие" }
            };

            UnitOfWork unitOfWork = new UnitOfWork();
            unitOfWork.AddictionRepository.InsertRange(addictions);
            await unitOfWork.SaveAsync();
        } 
    }
}