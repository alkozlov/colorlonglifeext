﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using ColorLongLife.DataAccessLayer;
using ColorLongLife.DataAccessLayer.Models;

namespace ColorLongLife.WorldParser
{
    class Program
    {
        static void Main(string[] args)
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load("rocid.xml");

            XmlNodeList countryNodes = xmlDocument.SelectNodes("/rocid/country");
            XmlNodeList cityNodes = xmlDocument.SelectNodes("/rocid/city");
            List<XmlNode> ctNodes = new List<XmlNode>();
            for (int i = 0; i < cityNodes.Count; i++)
            {
                ctNodes.Add(cityNodes[i]);
            }

            if (countryNodes != null && countryNodes.Count > 0 && cityNodes != null && cityNodes.Count > 0)
            {
                try
                {
                    List<Country> countries = new List<Country>();

                    for (int i = 0; i < countryNodes.Count; i++)
                    {
                        XmlNode countryNode = countryNodes[i];
                        String countryId = countryNode.SelectSingleNode("country_id").InnerText;
                        String countryName = countryNode.SelectSingleNode("name").InnerText;

                        Country country = new Country
                        {
                            Name = countryName
                        };
                        List<XmlNode> selectedCities =
                            ctNodes.Where(x => x.SelectSingleNode("country_id").InnerText.Equals(countryId)).ToList();
                        List<City> cities = selectedCities.Select(selectedCity => new City
                        {
                            Name = selectedCity.SelectSingleNode("name").InnerText
                        }).ToList();

                        country.Cities = cities;
                        countries.Add(country);
                    }

                    UnitOfWork unitOfWork = new UnitOfWork();
                    unitOfWork.CountryRepository.InsertRange(countries);
                    unitOfWork.SaveAsync().Wait();

                    Console.WriteLine("DONE :)");
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("ERROR!!!!!!!!!!");
                    Console.WriteLine(e.Message);
                }
            }
            else
            {
                Console.WriteLine("Data not found!");
            }

            Console.ReadKey();
        }
    }
}
