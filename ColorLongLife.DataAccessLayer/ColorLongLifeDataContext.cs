﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ColorLongLife.DataAccessLayer.Migrations;
using ColorLongLife.DataAccessLayer.Models;

namespace ColorLongLife.DataAccessLayer
{
    public class ColorLongLifeDataContext : DbContext
    {
        public ColorLongLifeDataContext() : base("ColorLongLifeDataContext")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ColorLongLifeDataContext, Configuration>());
        }

        public DbSet<Article> Articles { get; set; }
        public DbSet<ArticleVote> ArticleVotes { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Chart> Charts { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Faq> Faq { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<TopicTreatment> TopicTreatments { get; set; }
        public DbSet<CustomerRequest> CustomerRequests { get; set; }
        public DbSet<MenuForDictionary> MenuForDictionaries { get; set; }
        public DbSet<Dictionary> Dictionaries { get; set; }
        public DbSet<Emotion> Emotions { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<Doctor> Doctors { get; set; }
        public DbSet<Organ> Organs { get; set; }
        public DbSet<ConsultationSettings> ConsultationSettings { get; set; }
        public DbSet<Consultation> Consultations { get; set; }
        public DbSet<Subcategory> Subcategories { get; set; } 
        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<UseProduct> UseProducts { get; set; }
        public DbSet<ProductCategories> ProductCategories { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Happiness> Happiness { get; set; }
        public DbSet<Diagnostic> Diagnostics { get; set; }
        public DbSet<DiagramSector> DiagramSectors { get; set; }
        public DbSet<ColorType> ColorTypes { get; set; }
        public DbSet<ColorTypeDate> ColorTypeDates { get; set; }
        public DbSet<Addiction> Addictions { get; set; }
        public DbSet<AdditionColorType> AdditionColorTypes { get; set; }
        public DbSet<FeaturePersonality> FeaturePersonality { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}