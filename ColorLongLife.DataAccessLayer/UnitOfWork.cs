﻿using System;
using System.Threading.Tasks;
using ColorLongLife.DataAccessLayer.Models;
using ColorLongLife.DataAccessLayer.Repositories;

namespace ColorLongLife.DataAccessLayer
{
    public class UnitOfWork : IDisposable
    {
        private readonly ColorLongLifeDataContext _context = new ColorLongLifeDataContext();
        private GenericRepository<Article> _articleRepository;
        private GenericRepository<ArticleVote> _articleVoteRepository;
        private GenericRepository<Book> _bookRepository;
        private GenericRepository<Chart> _chartRepository;
        private GenericRepository<Faq> _faqRepository;
        private GenericRepository<Service> _serviceRepository;
        private GenericRepository<User> _userRepository;
        private GenericRepository<Country> _countryRepository;
        private GenericRepository<City> _cityRepository;
        private GenericRepository<TopicTreatment> _topicTreatmentRepository;
        private GenericRepository<CustomerRequest> _сustomerRequestRepository;
        private GenericRepository<Emotion> _emotionRepository;
        private GenericRepository<Position> _positionRepository;
        private GenericRepository<Doctor> _doctorRepository;
        private GenericRepository<Organ> _organRepository;
        private GenericRepository<Subcategory> _subcategoryRepository;
        private GenericRepository<Category> _categoryRepository;
        private GenericRepository<ConsultationSettings> _consultationSettingsRepository;
        private GenericRepository<Consultation> _consultationRepository;
        private GenericRepository<WorkTimeSkip> _workTimeSkipRepository;
        private GenericRepository<Product> _productRepository;
        private GenericRepository<UseProduct> _useProductRepository;
        private GenericRepository<ProductCategories> _productCategoriesRepository;
        private GenericRepository<Cart> _cartRepository;
        private GenericRepository<Order> _orderRepository;
        private GenericRepository<OrderItem> _orderItemRepository;
        private GenericRepository<Happiness> _happinessRepository;
        private GenericRepository<Diagnostic> _diagnosticRepository;
        private GenericRepository<DiagramSector> _diagramSectorRepository { get; set; }
        private GenericRepository<ColorType> _colorTypeRepository { get; set; }
        private GenericRepository<ColorTypeDate> _colorTypeDateRepository { get; set; }
        private GenericRepository<Addiction> _addictionRepository { get; set; }
        private GenericRepository<AdditionColorType> _additionColorTypeRepository { get; set; }
        private GenericRepository<FeaturePersonality> _featurePersonalityRepository { get; set; }

        public GenericRepository<Article> ArticleRepository
        {
            get
            {
                return this._articleRepository ??
                       (this._articleRepository = new GenericRepository<Article>(this._context));
            }
        }

        public GenericRepository<ArticleVote> ArticleVoteRepository
        {
            get
            {
                return this._articleVoteRepository ??
                          (this._articleVoteRepository = new GenericRepository<ArticleVote>(this._context));
            }
        }

        public GenericRepository<Book> BookRepository
        {
            get
            {
                return this._bookRepository ??
                       (this._bookRepository = new GenericRepository<Book>(this._context));
            }
        }

        public GenericRepository<Chart> ChartRepository
        {
            get
            {
                return this._chartRepository ??
                       (this._chartRepository = new GenericRepository<Chart>(this._context));
            }
        }

        public GenericRepository<Faq> FaqRepository
        {
            get
            {
                return this._faqRepository ??
                       (this._faqRepository = new GenericRepository<Faq>(this._context));
            }
        }

        public GenericRepository<Service> ServiceRepository
        {
            get
            {
                return this._serviceRepository ??
                       (this._serviceRepository = new GenericRepository<Service>(this._context));
            }
        }

        public GenericRepository<User> UserRepository
        {
            get
            {
                return this._userRepository ??
                       (this._userRepository = new GenericRepository<User>(this._context));
            }
        }

        public GenericRepository<Country> CountryRepository
        {
            get
            {
                return this._countryRepository ??
                       (this._countryRepository = new GenericRepository<Country>(this._context));
            }
        }

        public GenericRepository<City> CityRepository
        {
            get
            {
                return this._cityRepository ??
                       (this._cityRepository = new GenericRepository<City>(this._context));
            }
        }

        public GenericRepository<TopicTreatment> TopicTreatmentRepository
        {
            get
            {
                return this._topicTreatmentRepository ??
                       (this._topicTreatmentRepository = new GenericRepository<TopicTreatment>(this._context));
            }
        }

        public GenericRepository<CustomerRequest> CustomerRequestRepository
        {
            get
            {
                return this._сustomerRequestRepository ??
                       (this._сustomerRequestRepository = new GenericRepository<CustomerRequest>(this._context));
            }
        }

        public GenericRepository<Emotion> EmotionRepository
        {
            get
            {
                return _emotionRepository ??
                       (this._emotionRepository = new GenericRepository<Emotion>(this._context));
            }
        }

        public GenericRepository<Position> PositionRepository
        {
            get
            {
                return _positionRepository ??
                       (this._positionRepository = new GenericRepository<Position>(this._context));
            }
        }

        public GenericRepository<Doctor> DoctorRepository
        {
            get
            {
                return _doctorRepository ??
                       (_doctorRepository = new GenericRepository<Doctor>(this._context));
            }
        }

        public GenericRepository<Organ> OrganRepository
        {
            get
            {
                return _organRepository ??
                       (this._organRepository = new GenericRepository<Organ>(this._context));
            }
        }

        public GenericRepository<ConsultationSettings> ConsultationsSettingsRepository
        {
            get
            {
                return this._consultationSettingsRepository ??
                       (this._consultationSettingsRepository = new GenericRepository<ConsultationSettings>(this._context));
            }
        }

        public GenericRepository<Consultation> ConsultationsRepository
        {
            get
            {
                return this._consultationRepository ??
                       (this._consultationRepository = new GenericRepository<Consultation>(this._context));
            }
        }

        public GenericRepository<WorkTimeSkip> WorkTimeSkipRepository
        {
            get
            {
                return this._workTimeSkipRepository ??
                       (this._workTimeSkipRepository = new GenericRepository<WorkTimeSkip>(this._context));
            }
        }

        public GenericRepository<Subcategory> SubcategoryRepository
        {
            get
            {
                return _subcategoryRepository ??
                       (_subcategoryRepository = new GenericRepository<Subcategory>(this._context));
            }
        }

        public GenericRepository<Category> CategoryRepository
        {
            get
            {
                return _categoryRepository ??
                       (_categoryRepository = new GenericRepository<Category>(this._context));
            }
        }

        public GenericRepository<Product> ProductRepository
        {
            get
            {
                return _productRepository??
                       (_productRepository = new GenericRepository<Product>(this._context));
            }
        }

        public GenericRepository<UseProduct> UseProductRepository
        {
            get
            {
                return _useProductRepository ??
                       (_useProductRepository = new GenericRepository<UseProduct>(this._context));
            }
        }

        public GenericRepository<ProductCategories> ProductCategoriesRepository
        {
            get
            {
                return _productCategoriesRepository ??
                       (_productCategoriesRepository = new GenericRepository<ProductCategories>(this._context));
            }
        }

        public GenericRepository<Cart> CartRepository
        {
            get
            {
                return _cartRepository ??
                       (_cartRepository = new GenericRepository<Cart>(this._context));
            }
        }

        public GenericRepository<Order> OrderRepository
        {
            get
            {
                return _orderRepository ??
                       (_orderRepository = new GenericRepository<Order>(this._context));
            }
        }

        public GenericRepository<OrderItem> OrderItemRepository
        {
            get
            {
                return _orderItemRepository ??
                       (_orderItemRepository = new GenericRepository<OrderItem>(this._context));
            }
        }

        public GenericRepository<Happiness> HappinessRepository
        {
            get
            {
                return this._happinessRepository ??
                       (this._happinessRepository = new GenericRepository<Happiness>(this._context));
            }
        }

        public GenericRepository<Diagnostic> DiagnosticRepository
        {
            get
            {
                return this._diagnosticRepository ??
                       (this._diagnosticRepository = new GenericRepository<Diagnostic>(this._context));
            }
        }

        public GenericRepository<DiagramSector> DiagramSectorRepository
        {
            get
            {
                return this._diagramSectorRepository ??
                       (this._diagramSectorRepository = new GenericRepository<DiagramSector>(this._context));
            }
        }

        public GenericRepository<ColorType> ColorTypeRepository
        {
            get
            {
                return this._colorTypeRepository ??
                       (this._colorTypeRepository = new GenericRepository<ColorType>(this._context));
            }
        }

        public GenericRepository<ColorTypeDate> ColorTypeDateRepository
        {
            get
            {
                return this._colorTypeDateRepository ??
                       (this._colorTypeDateRepository = new GenericRepository<ColorTypeDate>(this._context));
            }
        }

        public GenericRepository<Addiction> AddictionRepository
        {
            get
            {
                return this._addictionRepository ??
                       (this._addictionRepository = new GenericRepository<Addiction>(this._context));
            }
        }

        public GenericRepository<AdditionColorType> AdditionColorTypeRepository
        {
            get
            {
                return this._additionColorTypeRepository ??
                       (this._additionColorTypeRepository = new GenericRepository<AdditionColorType>(this._context));
            }
        }

        public GenericRepository<FeaturePersonality> FeaturePersonalityRepository
        {
            get
            {
                return this._featurePersonalityRepository ??
                       (this._featurePersonalityRepository = new GenericRepository<FeaturePersonality>(this._context));
            }
        }

        public async Task SaveAsync()
        {
            await this._context.SaveChangesAsync();
        }

        private bool _disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    this._context.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}