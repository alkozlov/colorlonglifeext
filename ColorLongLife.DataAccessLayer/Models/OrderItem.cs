﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColorLongLife.DataAccessLayer.Models
{
    [Table("OrderItems")]
    public class OrderItem
    {
        [Key]
        [Column(Order = 1)]
        public int OrderItemId { get; set; }
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public int Count { get; set; }
        public decimal Price { get; set; }
        public Int32? Discount { get; set; }

        public virtual Order Order { get; set; }
    }
}
