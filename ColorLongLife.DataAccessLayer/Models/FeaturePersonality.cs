﻿using System;

namespace ColorLongLife.DataAccessLayer.Models
{
    public class FeaturePersonality
    {
        public Int32 FeaturePersonalityId { get; set; }
        public Int32 ColorTypeId { get; set; }
        public String Characteristic { get; set; }
        public String Description { get; set; }

        public virtual ColorType ColorType { get; set; }
    }
}