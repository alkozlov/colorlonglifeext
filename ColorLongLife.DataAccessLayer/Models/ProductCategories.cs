﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColorLongLife.DataAccessLayer.Models
{
    [Table("ProductCategories")]
    public class ProductCategories
    {
        [Key]
        [Column(Order = 1)]
        public int ProductCategoriesId { get; set; }
        public string NameProductCategories { get; set; }

        public IEquatable<Product> Products { get; set; }
    }
}
