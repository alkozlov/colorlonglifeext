﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ColorLongLife.DataAccessLayer.Models
{
    [Table("ConsultationSettings")]
    public class ConsultationSettings
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 ConsultationSettingsId { get; set; }

        public DateTime Date { get; set; }

        public TimeSpan StartTime { get; set; }

        public TimeSpan EndTime { get; set; }

        public TimeSpan TimeslotDuration { get; set; }

        public TimeSpan RestDuration { get; set; }

        public Int32 Cost { get; set; }

        public Boolean IsActive { get; set; }

        public virtual IEnumerable<Consultation> Consultations { get; set; }
    }
}