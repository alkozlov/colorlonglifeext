﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ColorLongLife.DataAccessLayer.Models
{
    [Table("Consultations")]
    public class Consultation
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 ConsultationId { get; set; }

        [Required]
        public TimeSpan StartTime { get; set; }

        [Required]
        [ForeignKey("ConsultationSettings")]
        public Int32 ConsultationSettingsId { get; set; }

        [Required]
        [ForeignKey("Doctor")]
        public Int32 DoctorId { get; set; }

        [Required]
        [ForeignKey("User")]
        public Int32 UserId { get; set; }

        [Required]
        public Int32 PaymentStatus { get; set; }

        public virtual ConsultationSettings ConsultationSettings { get; set; }

        public virtual Doctor Doctor { get; set; }

        public virtual User User { get; set; }
    }
}