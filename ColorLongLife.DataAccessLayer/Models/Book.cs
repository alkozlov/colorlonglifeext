﻿using System;
using System.Collections.Generic;

namespace ColorLongLife.DataAccessLayer.Models
{
    public class Book
    {
        public int BookId { get; set; }
        public string Author { get; set; }
        public Int32 Year { get; set; }
        public string ImagePath { get; set; }
        public DateTime DateCreated { get; set; }
        public Boolean IsDelated { get; set; }
        public string Language { get; set; }
        public string Title { get; set; }
        public string Genre { get; set; }
        public string Description { get; set; }
        public string Publisher { get; set; }
        public decimal Price { get; set; }
        public Int32? Discount { get; set; }
        public Int32 Count { get; set; }
    }
}