﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ColorLongLife.DataAccessLayer.Models
{
    [Table("Categories")]
    public class Category
    {
        [Key]
        [Column(Order = 1)]
        public Int32 IdCategory { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }

        public ICollection<Subcategory> Subcategories { get; set; }
    }
}