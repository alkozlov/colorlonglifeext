﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ColorLongLife.DataAccessLayer.Models
{
    [Table("Users")]
    public class User
    {
        [Key]
        [Column(Order = 1)]
        public Int32 UserId { get; set; }
        public String UserPasswordHash { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime LastVisitDate { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String MiddleName { get; set; }
        public String Phone { get; set; }
        public String Email { get; set; }
        public DateTime Birthday { get; set; }
        public Int32 Gender { get; set; }
        public Int32? CountryId { get; set; }
        public Int32? CityId { get; set; }
        public Int32 Weight { get; set; }
        public Int32 Growth { get; set; }
        public Boolean IsDeleted { get; set; }
        public Int32 UserStatus { get; set; }
        public Int32 UserAccountType { get; set; }
        public String ConfirmationToken { get; set; }
        public virtual Country Country { get; set; }
        public virtual City City { get; set; }
    }
}