﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ColorLongLife.DataAccessLayer.Models
{
    [Table("Emotions")]
    public class Emotion
    {
        [Key]
        [Column(Order = 1)]
        public Int32 EmotionId { get; set; }
        public String Title { get; set; }
        public String Description { get; set; }
        public Int32? ParentId { get; set; }
    }
}