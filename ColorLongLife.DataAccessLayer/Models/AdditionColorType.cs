﻿using System;

namespace ColorLongLife.DataAccessLayer.Models
{
    public class AdditionColorType
    {
        public Int32 AdditionColorTypeId { get; set; }
        public Int32 ColorTypeDateId { get; set; }
        public Int32 AdditionId { get; set; }

        public virtual Addiction Addiction { get; set; }
        public virtual ColorTypeDate ColorTypeDate { get; set; }
    }
}