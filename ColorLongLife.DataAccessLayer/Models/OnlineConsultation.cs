﻿using System;

namespace ColorLongLife.DataAccessLayer.Models
{
    public class OnlineConsultation
    {
        public Int32 OnlineConsultationId { get; set; }
        public Int32 UserId { get; set; }
        public DateTime DateRegistration { get; set; }
        public Boolean IsActive { get; set; }
        public Int32 ConsultantId { get; set; } 
    }
}