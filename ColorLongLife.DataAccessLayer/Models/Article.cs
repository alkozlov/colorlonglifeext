﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ColorLongLife.DataAccessLayer.Models
{
    [Table("Articles")]
    public class Article
    {
        [Key]
        [Column(Order = 1)]
        public int ArticleId { get; set; }
        public DateTime DateCreated { get; set; }
        public string Url { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public bool? Publish { get; set; }

        public virtual ICollection<ArticleVote> ArticleVotes { get; set; }
    }
}