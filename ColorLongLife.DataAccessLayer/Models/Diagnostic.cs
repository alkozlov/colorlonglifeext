﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ColorLongLife.DataAccessLayer.Models
{
    [Table("Diagnostics")]
    public class Diagnostic
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 Id { get; set; }

        [ForeignKey("User")]
        public Int32 UserId { get; set; }

        [ForeignKey("Happiness")]
        public Int32 HappinessId { get; set; }

        public DateTime CreateDateTimeUtc { get; set; }

        public Int32? Rating { get; set; }

        public virtual User User { get; set; }

        public virtual Happiness Happiness { get; set; }

        public virtual ICollection<DiagramSector> DiagramSectors { get; set; }
    }
}