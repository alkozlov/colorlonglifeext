﻿using System;
using System.Collections.Generic;

namespace ColorLongLife.DataAccessLayer.Models
{
    public class ColorTypeDate
    {
        public Int32 ColorTypeDateId { get; set; }
        public Int32 ColorTypeId { get; set; }
        public Int32 Day { get; set; }
        public Int32 Month { get; set; }

        public virtual ColorType ColorType { get; set; }
        public virtual ICollection<AdditionColorType> AdditionColorTypes { get; set; }
    }
}