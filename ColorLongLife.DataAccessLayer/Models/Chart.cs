﻿using System;
using System.Collections.Generic;

namespace ColorLongLife.DataAccessLayer.Models
{
    public class Chart
    {
        public int ChartId { get; set; }
        public Int32? UserId { get; set; }
        public string ChartName { get; set; }
        public string ChartData { get; set; }
        public System.DateTime DateCreated { get; set; }
        public Int32? OrderItemIdChart { get; set; }
        public Int32? OrderItemIdModule { get; set; }

        public virtual User User { get; set; }
    }
}