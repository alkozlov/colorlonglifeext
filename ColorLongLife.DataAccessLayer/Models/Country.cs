﻿using System;
using System.Collections.Generic;

namespace ColorLongLife.DataAccessLayer.Models
{
    public class Country
    {
        public Int32 CountryId { get; set; }
        public String Name { get; set; }

        public virtual ICollection<City> Cities { get; set; }
    }
}