﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ColorLongLife.DataAccessLayer.Models
{
    [Table("DiagramSectors")]
    public class DiagramSector
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 Id { get; set; } 

        public Byte SectorType { get; set; }

        public String Color { get; set; }

        public String Opacity { get; set; }

        [ForeignKey("Category")]
        public Int32? CategoryId { get; set; }

        [ForeignKey("Subcategory")]
        public Int32? SubcategoryId { get; set; }

        [ForeignKey("Emotion")]
        public Int32? EmotionId { get; set; }

        [ForeignKey("Diagnostic")]
        public Int32 DiagnosticId { get; set; }

        public virtual Category Category { get; set; }

        public virtual Subcategory Subcategory { get; set; }

        public virtual Emotion Emotion { get; set; }

        public virtual Diagnostic Diagnostic { get; set; }
    }
}