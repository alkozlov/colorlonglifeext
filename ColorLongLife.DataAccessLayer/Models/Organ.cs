﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ColorLongLife.DataAccessLayer.Models
{
    [Table("Organ")]
    public class Organ
    {
        [Key]
        [Column(Order = 1)]
        public Int32 OrganId { get; set; }
        public String TitleOrgan { get; set; }
        public String Description { get; set; }
        public Int32 DoctorId { get; set; }

        public virtual Doctor Doctor { get; set; }
    }
}