﻿using System;
using System.Collections.Generic;

namespace ColorLongLife.DataAccessLayer.Models
{
    public class Service
    {
        public int ServiceId { get; set; }
        public string Title { get; set; }
        public DateTime DateCreated { get; set; }
        public bool IsDeleted { get; set; }
        public string Language { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public Int32? Discount { get; set; }
    }
}