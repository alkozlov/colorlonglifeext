﻿using System;

namespace ColorLongLife.DataAccessLayer.Models
{
    public class Happiness
    {
        public Int32 HappinessId { get; set; }
        public String Title { get; set; }
        public String Description { get; set; }
        public String PathToImage { get; set; } 
    }
}