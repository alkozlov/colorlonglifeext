﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ColorLongLife.DataAccessLayer.Models
{
    public class ColorType
    {
        [Key]
        public Int32 ColorTypeId { get; set; }
        public Int32 FirstNumber { get; set; }
        public Int32 SecondNumber { get; set; }
        public String Problem { get; set; }

        public virtual ICollection<ColorTypeDate> ColorTypeDates { get; set; }
        public virtual ICollection<FeaturePersonality> FeaturePersonalities { get; set; }
    }
}