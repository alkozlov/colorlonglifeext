﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColorLongLife.DataAccessLayer.Models
{
    [Table("Orders")]
    public class Order
    {
        [Key]
        [Column(Order = 1)]
        public int OrderId { get; set; }
        public int UserId { get; set; }
        public DateTime OrderDateCreated { get; set; }
        public string Address { get; set; }
        public string PostCode { get; set; }
        public Int32 Status { get; set; }

        public virtual ICollection<OrderItem> OrderItems { get; set; }
        public virtual User User { get; set; }
    }
}
