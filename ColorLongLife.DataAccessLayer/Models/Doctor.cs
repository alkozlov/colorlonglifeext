﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.ExceptionServices;

namespace ColorLongLife.DataAccessLayer.Models
{
    [Table("Doctor")]
    public class Doctor
    {
        [Key]
        [Column(Order = 1)]
        public Int32 DoctorId { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String MiddleName { get; set; }
        public String Description { get; set; }
        public String PhotoStorageKey { get; set; }
        public Int32 PositionId { get; set; }

        public IEquatable<Organ> Organs { get; set; }
        public virtual Position Position { get; set; }
        public virtual IEnumerable<Consultation> Consultations { get; set; }
        public virtual IEnumerable<WorkTimeSkip> WorkTimeSkips { get; set; }
    }
}