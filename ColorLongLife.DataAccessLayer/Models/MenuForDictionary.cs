﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ColorLongLife.DataAccessLayer.Models
{
    [Table("MenuForDictionary")]
    public class MenuForDictionary
    {
        [Key]
        [Column(Order = 1)]
        public Int32 MenuId { get; set; }
        public String Name { get; set; }

        public virtual ICollection<Dictionary> Dictionaries { get; set; }
    }
}