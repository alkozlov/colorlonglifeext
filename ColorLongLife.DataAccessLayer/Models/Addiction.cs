﻿using System;
using System.Collections.Generic;

namespace ColorLongLife.DataAccessLayer.Models
{
    public class Addiction
    {
        public Int32 AddictionId { get; set; }
        public String Title { get; set; }

        public virtual ICollection<AdditionColorType> AdditionColorType { get; set; }
    }
}