﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColorLongLife.DataAccessLayer.Models
{
    //[Table("BuyDecryption")]
    public class BuyDecryption
    {
        [Key]
        [Column(Order = 1)]
        public Int32 BuyDecryptionId { get; set; }
        public String NameDecryption { get; set; }

    }
}
