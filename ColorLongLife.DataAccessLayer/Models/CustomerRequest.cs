﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ColorLongLife.DataAccessLayer.Models
{
    [Table("CustomerRequest")]
    public class CustomerRequest
    {
        [Key]
        [Column(Order = 1)]
        public Int32 CustomerRequestId { get; set; }
        public Int32 TopicId { get; set; }
        public String Message { get; set; }
        public String Email { get; set; }
        public DateTime DateSend { get; set; }

        public virtual TopicTreatment TopicTreatment { get; set; }
    }
}