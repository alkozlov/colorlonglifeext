﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ColorLongLife.DataAccessLayer.Models
{
    [Table("Products")]
    public class Product
    {
        [Key]
        [Column (Order = 1)]
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public string FullDescription { get; set; }
        public string PathToImage { get; set; }
        public string ProducerName { get; set; }
        public string CountriIsMade { get; set; }
        public Decimal Price { get; set; }
        public int UseProductId { get; set; }
        public int ProductCategoriesId { get; set; }

        public virtual UseProduct UseProduct { get; set; }
        public virtual ProductCategories ProductCategories { get; set; }

    }
}
