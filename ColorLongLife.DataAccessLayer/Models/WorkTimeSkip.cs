﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ColorLongLife.DataAccessLayer.Models
{
    [Table("WorkTimeSkips")]
    public class WorkTimeSkip
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 WorkTimeSkipId { get; set; }

        [Required]
        [ForeignKey("Doctor")]
        public Int32 DoctorId { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public TimeSpan Start { get; set; }

        [Required]
        public TimeSpan Duration { get; set; }

        [Required]
        public Int32 WorkTimeSkipReason { get; set; }

        [MaxLength(256)]
        public String Description { get; set; }

        public virtual Doctor Doctor { get; set; }
    }
}