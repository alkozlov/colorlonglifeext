﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColorLongLife.DataAccessLayer.Models
{
    [Table("UseProducts")]
    public class UseProduct
    {
        [Key]
        [Column (Order=1)]
        public int UseProductId { get; set; }
        public String NameUseProduct { get; set; }

        public IEquatable<Product> Products { get; set; }
    }
}
