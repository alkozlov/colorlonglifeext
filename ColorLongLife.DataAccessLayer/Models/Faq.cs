﻿using System;

namespace ColorLongLife.DataAccessLayer.Models
{
    public class Faq
    {
        public int FaqId { get; set; }
        public string AuthorEmail { get; set; }
        public string AuthorFullName { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public bool Publish { get; set; }
        public DateTime DateCreated { get; set; }
    }
}