﻿namespace ColorLongLife.DataAccessLayer.Models
{
    public class ArticleVote
    {
        public int ArticleVoteId { get; set; }
        public int ArticleId { get; set; }
        public int Rate { get; set; }
        public string Ip { get; set; }
        public System.DateTime Date { get; set; }

        public virtual Article Article { get; set; }
    }
}