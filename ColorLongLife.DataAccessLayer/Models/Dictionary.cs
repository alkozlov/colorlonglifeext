﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ColorLongLife.DataAccessLayer.Models
{
    [Table("Dictionary")]
    public class Dictionary
    {
        [Key]
        [Column(Order = 1)]
        public Int32 DictionaryId { get; set; }
        public String Title { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public Int32 MenuId { get; set; }

        public virtual MenuForDictionary MenuForDictionary { get; set; }
    }
}