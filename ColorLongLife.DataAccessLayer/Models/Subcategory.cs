﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ColorLongLife.DataAccessLayer.Models
{
    [Table("Subcategories")]
    public class Subcategory
    {
        [Key]
        [Column(Order = 1)]
        public Int32 IdSubcategory { get; set; }
        public String Name { get; set; }
        public Int32 Gender { get; set; }

        public Int32 IdCategory { get; set; }
        public virtual Category Category { get; set; }
    }
}