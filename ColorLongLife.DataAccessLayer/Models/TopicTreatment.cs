﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColorLongLife.DataAccessLayer.Models
{
    [Table("TopicTreatments")]
    public class TopicTreatment
    {
        [Key]
        [Column(Order = 1)]
        public Int32 TopicId { get; set; }
        public String Name { get; set; }

        public virtual ICollection<CustomerRequest> CustomerRequestses { get; set; }
    }
}
