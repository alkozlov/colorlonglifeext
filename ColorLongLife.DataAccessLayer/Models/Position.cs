﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ColorLongLife.DataAccessLayer.Models
{
    [Table("Position")]
    public class Position
    {
        [Key]
        [Column(Order = 1)]
        public Int32 PositionId { get; set; }
        public String TitlePosition { get; set; }
        public String Description { get; set; }

        public IEquatable<Doctor> Doctors { get; set; }
    }
}