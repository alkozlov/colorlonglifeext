﻿using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using ColorLongLife.DataAccessLayer.Models;

namespace ColorLongLife.DataAccessLayer
{
    internal class ColorLongLifeCreateDatabaseIfNotExists : CreateDatabaseIfNotExists<ColorLongLifeDataContext>
    {
        protected override void Seed(ColorLongLifeDataContext context)
        {
            #region

            User user = new User()
            {
                Birthday = DateTime.UtcNow,
                CreateDate = DateTime.UtcNow,
                Email = "admin@colorlonglife.ru",
                FirstName = "Федор",
                LastName = "Иванов",
                MiddleName = "Александрович",
                Gender = 1,
                Growth = 185,
                Weight = 75,
                UserAccountType = 1,
                UserStatus = 1,
                LastVisitDate = DateTime.UtcNow,
                Phone = "+375442569847",
                UserPasswordHash = "JfnnlDI7RTiF9RgfG2JNCw==",
                IsDeleted = false
            };
            context.Users.AddOrUpdate(user);

            #endregion

            base.Seed(context);
        }
    }
}