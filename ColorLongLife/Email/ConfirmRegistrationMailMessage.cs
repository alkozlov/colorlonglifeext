﻿using System;
using System.Net.Mail;
using ColorLongLife.DataAccessLayer.Models;

namespace ColorLongLife.Email
{
    public class ConfirmRegistrationMailMessage : BaseMailMessage
    {
        private readonly String _subject = "Подтверждение регистрации.";
        private String _recipient;
        private String _recipientFirstName;
        private String _recipientLastName;
        private String _siteUrl;
        private String _confirmationToken;

        public ConfirmRegistrationMailMessage(User user, String siteUrl) : base()
        {
            _recipient = user.Email;
            _recipientFirstName = user.FirstName;
            _recipientLastName = user.LastName;
            _siteUrl = siteUrl;
            _confirmationToken = user.ConfirmationToken;
        }

        public override MailMessage GetMailMessage()
        {
            MailMessage mailMessage = new MailMessage();
            mailMessage.To.Add(_recipient);
            mailMessage.Subject = _subject;
            mailMessage.IsBodyHtml = false;
            String confirmationUrl = String.Concat(_siteUrl, "/authorization/activatepanel?keytoken=", _confirmationToken);
            String body = String.Format("{0} {1}, для подтверждения регистрации пройдите по ссылке {2}",
                _recipientFirstName, _recipientLastName, confirmationUrl);
            mailMessage.Body = body;

            return mailMessage;
        }
    }
}