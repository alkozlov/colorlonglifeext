﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using ColorLongLife.Utilities;

namespace ColorLongLife.Email
{
    public static class Emailer
    {
        public static async Task SendMailAsync(BaseMailMessage baseMailMessage)
        {
            try
            {
                String smtpServer = ConfigurationHelper.GetSmtpServer();
                Int32 smtpPort = ConfigurationHelper.GetSmtpPort();
                String senderEmail = ConfigurationHelper.GetEmail();
                String password = ConfigurationHelper.GetPassword();

                MailMessage mail = baseMailMessage.GetMailMessage();
                mail.From = new MailAddress(senderEmail);
                SmtpClient smtpClient = new SmtpClient(smtpServer, smtpPort);
                smtpClient.Credentials = new NetworkCredential(senderEmail, password);
                smtpClient.EnableSsl = true;
                await smtpClient.SendMailAsync(mail);
            }
            catch (SmtpFailedRecipientException e)
            {
                Console.WriteLine(e);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}