﻿using System.Net.Mail;

namespace ColorLongLife.Email
{
    public abstract class BaseMailMessage
    {
        protected BaseMailMessage()
        {
            
        }

        public abstract MailMessage GetMailMessage();
    }
}