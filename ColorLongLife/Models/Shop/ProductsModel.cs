﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ColorLongLife.DataAccessLayer.Models;

namespace ColorLongLife.Models.Shop
{
    public class ProductsModel
    {
        [Required(ErrorMessage = "Укажите имя.")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Длина строки должна быть от 2 до 50 символов.")]
        public String FirstName { get; set; }
        public IEnumerable<Product> Products { get; set; }
        public PagingInfoModel PagingInfo { get; set; }
        public int CurrentCategoryId { get; set; }
        //public Int32 ProductsId { get; set; }
        //public Int32 UseProductsId { get; set; }
        //public Int32 ProductCategoriesId { get; set; }
        //public IEnumerable<UseProduct> UseProducts { get; set; }
        //public IEnumerable<ProductCategories> ProductCategories { get; set; }
        //public ProductsModel()
        //{
        //    Products = new List<Product>();
        //    UseProducts = new List<UseProduct>();
        //    ProductCategories = new List<ProductCategories>();
        //}
    }
}