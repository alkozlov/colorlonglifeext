﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ColorLongLife.DataAccessLayer.Models;

namespace ColorLongLife.Models.Shop
{
    public class CardsProductModel
    {
        [Required(ErrorMessage = "Укажите имя.")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Длина строки должна быть от 2 до 50 символов.")]
        public String FirstName { get; set; }

        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public string FullDescription { get; set; }
        public string PathToImage { get; set; }
        public string ProducerName { get; set; }
        public string CountriIsMade { get; set; }
        public Decimal Price { get; set; }
        public string NameUseProduct { get; set; }
        public string NameProductCategories { get; set; }
    }
}