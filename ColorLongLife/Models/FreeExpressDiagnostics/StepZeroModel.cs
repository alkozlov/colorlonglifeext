﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ColorLongLife.DataAccessLayer.Models;

namespace ColorLongLife.Models.FreeExpressDiagnostics
{
    public class StepZeroModel
    {
        [Required(ErrorMessage = "Укажите имя.")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Длина строки должна быть от 2 до 50 символов.")]
        public String FirstName { get; set; }

        public IEnumerable<Category> Categories { get; set; }

        public StepZeroModel()
        {
            Categories = new List<Category>();
        } 

    }
}