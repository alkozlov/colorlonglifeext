﻿using System;

namespace ColorLongLife.Models.FreeExpressDiagnostics
{
    public class EmotionModel
    {
        public String Title { get; set; }
        public String Description { get; set; }
    }
}