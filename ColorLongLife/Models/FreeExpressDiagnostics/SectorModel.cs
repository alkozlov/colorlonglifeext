﻿using System;

namespace ColorLongLife.Models.FreeExpressDiagnostics
{
    public class SectorModel
    {
        public Int32 CategoryId { get; set; }

        public Int32 SubcategoryId { get; set; }

        public Int32 EmotionId { get; set; }

        public String Color { get; set; }

        public String Opacity { get; set; }
    }
}