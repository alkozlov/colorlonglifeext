﻿using System;
using System.Collections.Generic;

namespace ColorLongLife.Models.FreeExpressDiagnostics
{
    public class ExpressDiagnosticModel
    {
        public Int32 HappinessId { get; set; }

        public String CircleColor { get; set; }

        public String CircleOpacity { get; set; }

        public List<SectorModel> Sectors { get; set; }

        public ExpressDiagnosticModel()
        {
            Sectors = new List<SectorModel>();
        } 
    }
}