﻿using System;

namespace ColorLongLife.Models.HealthPanel
{
    public class TimeslotModel
    {
        public TimeSpan StartTime { get; set; }

        public Int32 DoctorId { get; set; }

        public Boolean IsReserved { get; set; }
    }
}