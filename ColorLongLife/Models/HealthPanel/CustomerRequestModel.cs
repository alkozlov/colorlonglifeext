﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ColorLongLife.Models.HealthPanel
{
    public class CustomerRequestModel
    {
        [Required(ErrorMessage = "Укажите тему.")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Укажите тему.")]
        public Int32 TopicId { get; set; }

        [Required(ErrorMessage = "Укажите сообщение.")]
        [StringLength(500, MinimumLength = 10, ErrorMessage = "Длина строки должна быть от 10 до 500 символов.")]
        public String Message { get; set; }

        [Required(ErrorMessage = "Укажите эл. почту.")]
        [StringLength(50, MinimumLength = 7, ErrorMessage = "Длина эл. почты должна быть от 7 до 50 символов.")]
        [EmailAddress(ErrorMessage = "Эл. почта введена некорректно.")]
        public String Email { get; set; }
    }
}