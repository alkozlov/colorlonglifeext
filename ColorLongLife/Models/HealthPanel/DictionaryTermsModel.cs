﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ColorLongLife.Models.HealthPanel
{
    public class DictionaryTermsModel
    {
        [Required(ErrorMessage = "Укажите пункт меню.")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Длина строки должна быть от 2 до 50 символов.")]
        public String Title { get; set; }

        [Required(ErrorMessage = "Укажите название термина.")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Длина строки должна быть от 2 до 50 символов.")]
        public String Name { get; set; }

        [Required(ErrorMessage = "Укажите описание термина.")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Длина строки должна быть от 10 до 500 символов.")]
        public String Description { get; set; }
    }
}