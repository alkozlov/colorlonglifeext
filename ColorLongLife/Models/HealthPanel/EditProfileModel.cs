﻿using System;
using System.ComponentModel.DataAnnotations;
using ColorLongLife.Utilities;

namespace ColorLongLife.Models.HealthPanel
{
    public class EditProfileModel
    {
        [Required(ErrorMessage = "Пользователь не найден.")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Неизвестный пользователь.")]
        public Int32 UserId { get; set; }

        [Required(ErrorMessage = "Укажите фамилию.")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Длина строки должна быть от 2 до 50 символов.")]
        public String FirstName { get; set; }

        [Required(ErrorMessage = "Укажите имя.")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов.")]
        public String LastName { get; set; }

        public String MiddleName { get; set; }

        [Required(ErrorMessage = "Укажите день рождения, например: 25.11.1990.")]
        [DataType(DataType.Date)]
        public DateTime Birthday { get; set; }

        public Gender Gender { get; set; }

        [Required(ErrorMessage = "Укажите ваш вес.")]
        [Range(40, 300, ErrorMessage = "Вес может быть от 40 до 300кг.")]
        public Int32 Weight { get; set; }

        [Required(ErrorMessage = "Укажите ваш рост.")]
        [Range(100, 250, ErrorMessage = "Рост должен быть от 100 до 250см.")]
        public Int32 Growth { get; set; }

        [Required(ErrorMessage = "Укажите ваш номер телефона.")]
        [StringLength(20, MinimumLength = 7, ErrorMessage = "Длина строки должна быть от 7 до 20 символов.")]
        public String Phone { get; set; }

        [Required(ErrorMessage = "Укажите эл. почту.")]
        [StringLength(50, MinimumLength = 7, ErrorMessage = "Длина эл. почты должна быть от 7 до 50 символов.")]
        [EmailAddress(ErrorMessage = "Эл. почта введена некорректно.")]
        public String Email { get; set; }

        public String OldPassword { get; set; }

        public String NewPassword { get; set; }

        public String ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Выберите страну.")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Выберите страну.")]
        public Int32 CountryId { get; set; }

        [Required(ErrorMessage = "Выберите город.")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Выберите город.")]
        public Int32 CityId { get; set; }
    }
}