﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ColorLongLife.Models.HealthPanel
{
    public class SendAFileResponseModel
    {
        [Required(ErrorMessage = "Укажите имя.")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов.")]
        public String FirstName { get; set; }
    }
}