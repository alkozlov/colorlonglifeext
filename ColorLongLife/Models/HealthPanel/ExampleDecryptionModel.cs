﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ColorLongLife.Models.HealthPanel
{
    public class ExampleDecryptionModel
    {
        [Required(ErrorMessage = "Укажите имя.")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Длина строки должна быть от 2 до 50 символов.")]
        public String FirstName { get; set; }
    }
}