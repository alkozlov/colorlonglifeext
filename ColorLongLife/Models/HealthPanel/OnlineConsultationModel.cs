﻿using System;
using System.Collections.Generic;

namespace ColorLongLife.Models.HealthPanel
{
    public class OnlineConsultationModel
    {
        public String FirstName { get; set; }

        public IEnumerable<DayOfWeekModel> Days;

        public OnlineConsultationModel()
        {
            this.Days = new List<DayOfWeekModel>();
        }
    }
}