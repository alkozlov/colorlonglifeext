﻿using System;
using System.Collections.Generic;

namespace ColorLongLife.Models.HealthPanel
{
    public class DayOfWeekModel
    {
        public DateTime Date { get; set; }

        public String ShortName { get; set; }

        public IEnumerable<TimeslotModel> Timeslots { get; set; }

        public DayOfWeekModel()
        {
            this.Timeslots = new List<TimeslotModel>();
        }
    }
}