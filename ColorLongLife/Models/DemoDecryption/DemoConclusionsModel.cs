﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ColorLongLife.Models.DemoDecryption
{
    public class DemoConclusionsModel
    {
        [Required(ErrorMessage = "Укажите фамилию.")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Длина строки должна быть от 2 до 50 символов.")]
        public String FirstName { get; set; }
    }
}