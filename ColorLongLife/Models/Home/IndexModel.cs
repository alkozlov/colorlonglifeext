﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ColorLongLife.Models.Home
{
    public class IndexModel
    {
        [Required(ErrorMessage = "Укажите имя.")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов.")]
        public String FirstName { get; set; }

        [Required(ErrorMessage = "Укажите фамилию.")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Длина строки должна быть от 2 до 50 символов.")]
        public String LastName { get; set; }

        public String MiddleName { get; set; }
    }
}