﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ColorLongLife.Models.Authorization
{
    public class SignInModel
    {
        [Required(ErrorMessage = "Введите электронную почту.")]
        [StringLength(50, MinimumLength = 7, ErrorMessage = "Длина эл. почты должна быть от 7 до 50 символов.")]
        [EmailAddress(ErrorMessage = "Проверьте введенный адресс эл. почты.")]
        public String Email { get; set; }
        [Required(ErrorMessage = "Введите пароль.")]
        [StringLength(50, MinimumLength = 6, ErrorMessage = "Длина пароля должна быть от 6 до 50 символов.")]
        public String Password { get; set; }
    }
}