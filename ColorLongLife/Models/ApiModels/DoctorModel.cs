﻿using System;

namespace ColorLongLife.Models.ApiModels
{
    public class DoctorModel
    {
        public Int32 DoctorId { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String MiddleName { get; set; }
        public String Description { get; set; }
        public String PathToImage { get; set; }
    }
}