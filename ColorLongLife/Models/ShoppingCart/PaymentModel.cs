﻿using System;
using System.ComponentModel.DataAnnotations;
using ColorLongLife.DataAccessLayer.Models;

namespace ColorLongLife.Models.ShoppingCart
{
    public class PaymentModel
    {
        [Required(ErrorMessage = "Пользователь не найден.")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Неизвестный пользователь.")]
        public Int32 UserId { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }

        public DateTime DateCreated { get; set; }

        [Required(ErrorMessage = "Укажите ваш номер телефона.")]
        [StringLength(20, MinimumLength = 7, ErrorMessage = "Длина строки должна быть от 7 до 20 символов.")]
        public String Phone { get; set; }

        public String Email { get; set; }

        [Required(ErrorMessage = "Выберите страну.")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Выберите страну.")]
        public Int32 CountryId { get; set; }

        [Required(ErrorMessage = "Выберите город.")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Выберите город.")]
        public Int32 CityId { get; set; }

        [Required(ErrorMessage = "Укажите адрес.")]
        [StringLength(50, MinimumLength = 10, ErrorMessage = "Длина строки должна быть от 10 до 100 символов.")]
        public String Address { get; set; }

        [Required(ErrorMessage = "Укажите почтовый код.")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 15 символов.")]
        public String Postcode { get; set; }

        public Int32 Status { get; set; }
        public User User { get; set; }
    }
}