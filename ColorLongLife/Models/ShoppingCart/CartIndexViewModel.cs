﻿using ColorLongLife.DataAccessLayer.Models;

namespace ColorLongLife.Models.ShoppingCart
{
    public class CartIndexViewModel
    {
        public Cart Cart { get; set; }
        public string ReturnUrl { get; set; }
    }
}