﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace ColorLongLife.Utilities
{
    public class CryptographyHelper
    {
        public static String HashPassword(String password)
        {
            MD5CryptoServiceProvider crypto = new MD5CryptoServiceProvider();
            byte[] data = Encoding.UTF8.GetBytes(password);
            byte[] hashData = crypto.ComputeHash(data);
            String result = Convert.ToBase64String(hashData);
            return result;
        }

        public static string GenerateActivationToken()
        {
            long i = 1;
            foreach (byte b in Guid.NewGuid().ToByteArray())
            {
                i *= ((int)b + 1);
            }
            return $"{i - DateTime.Now.Ticks:x}";
        }
    }
}