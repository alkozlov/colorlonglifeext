﻿namespace ColorLongLife.Utilities
{
    public enum AccountStatus : int
    {
        PrepareActivation = 0,
        Active = 1,
        Locked = 2,
        Deleted = 3
    }
}