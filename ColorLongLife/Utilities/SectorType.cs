﻿namespace ColorLongLife.Utilities
{
    public enum SectorType : byte
    {
        Sector = 0,
        Circle = 1
    }
}