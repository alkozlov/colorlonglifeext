﻿using System;
using System.Configuration;

namespace ColorLongLife.Utilities
{
    public class ConfigurationHelper
    {
        public static String GetSmtpServer()
        {
            String smtpServer = ConfigurationManager.AppSettings["SmtpServer"];
            return smtpServer;
        }

        public static Int32 GetSmtpPort()
        {
            Int32 smtpPort = Int32.Parse(ConfigurationManager.AppSettings["SmtpPort"]);
            return smtpPort;
        }

        public static String GetEmail()
        {
            String email = ConfigurationManager.AppSettings["Email"];
            return email;
        }

        public static String GetPassword()
        {
            String password = ConfigurationManager.AppSettings["Password"];
            return password;
        }

        public static String GetSiteUrl()
        {
            String password = ConfigurationManager.AppSettings["SiteUrl"];
            return password;
        }
    }
}