﻿using System;
using System.Collections.Generic;
using ColorLongLife.DataAccessLayer.Models;

namespace ColorLongLife.Utilities.BusinessUnits
{
    public class ScheduleTimeSlot
    {
        public TimeSpan StartTime { get; set; }

        public TimeSpan Duration { get; set; }

        public IEnumerable<Doctor> AvailableDoctors { get; set; }

        public ScheduleTimeSlot()
        {
            this.AvailableDoctors = new List<Doctor>();
        }
    }
}