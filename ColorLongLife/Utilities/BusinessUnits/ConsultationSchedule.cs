﻿using System;
using System.Collections.Generic;
using ColorLongLife.DataAccessLayer.Models;

namespace ColorLongLife.Utilities.BusinessUnits
{
    public class ConsultationSchedule
    {
        public DateTime Date { get; set; }

        public IEnumerable<ScheduleTimeSlot> TimeSlots { get; set; }

        public ConsultationSchedule()
        {
            this.TimeSlots = new List<ScheduleTimeSlot>();
        }
    }
}