﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColorLongLife.DataAccessLayer;
using ColorLongLife.DataAccessLayer.Models;

namespace ColorLongLife.Utilities.BusinessUnits
{
    public class ConsultationDispatcher
    {
        private UnitOfWork _unitOfWork;

        public ConsultationDispatcher()
        {
            this._unitOfWork = new UnitOfWork();
        }

        public ConsultationSchedule GetConsultationSchedule(DateTime date)
        {
            // 1 - Get consultation settings with exists consultations
            DateTime shortDate = date.Date;
            ConsultationSettings consultationSettings =
                this._unitOfWork.ConsultationsSettingsRepository.Get(x => x.Date.Equals(shortDate), null,
                    "Consultations").SingleOrDefault();
            if (consultationSettings == null)
            {
                throw new NullReferenceException("Consultation settings for specified date don't found.");
            }

            // Get doctors
            var doctors = this._unitOfWork.DoctorRepository.Get(null, null, "Consultations,WorkTimeSkips").ToList();
            ConsultationSchedule consultationSchedule = this.BuildSchedule(consultationSettings, doctors);

            return consultationSchedule;
        }

        private ConsultationSchedule BuildSchedule(ConsultationSettings consultationSettings, List<Doctor> doctors)
        {
            ConsultationSchedule consultationSchedule = new ConsultationSchedule();
            consultationSchedule.Date = consultationSettings.Date;

            // Create timeslots
            var startTime = consultationSettings.StartTime;
            List<ScheduleTimeSlot> scheduleTimeSlots = new List<ScheduleTimeSlot>();
            while (startTime < consultationSettings.EndTime)
            {
                ScheduleTimeSlot scheduleTimeSlot = new ScheduleTimeSlot
                {
                    StartTime = startTime,
                    Duration = consultationSettings.TimeslotDuration,
                    AvailableDoctors = doctors.Where(
                        doctor =>
                            !doctor.Consultations.Any(
                                x =>
                                    x.ConsultationSettingsId.Equals(consultationSettings.ConsultationSettingsId) &&
                                    x.StartTime == startTime) &&
                            !doctor.WorkTimeSkips.Any(
                                x =>
                                    x.Date + x.Start <= consultationSettings.Date + startTime &&
                                    x.Date + x.Start + x.Duration >= consultationSettings.Date + startTime)).ToList()
                };
                scheduleTimeSlots.Add(scheduleTimeSlot);
                startTime += consultationSettings.TimeslotDuration + consultationSettings.RestDuration;
            }
            consultationSchedule.TimeSlots = scheduleTimeSlots;

            return consultationSchedule;
        }
    }
}