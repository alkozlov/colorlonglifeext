﻿using System;

namespace ColorLongLife.Utilities
{
    public enum Currency : int
    {
        USD = 0,
        EUR = 1,
        RUB = 2
    }
}