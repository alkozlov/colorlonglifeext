﻿namespace ColorLongLife.Utilities
{
    public enum MenuForDictionary : int
    {
        QualitiesPersonality = 0,
        PsychosomaticProcesses = 1
    }
}