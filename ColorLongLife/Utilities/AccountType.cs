﻿namespace ColorLongLife.Utilities
{
    public enum AccountType : int
    {
        Customer = 0,
        Admin = 1
    }
}