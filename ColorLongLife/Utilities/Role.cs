﻿namespace ColorLongLife.Utilities
{
    public enum Role : int
    {
        Admin = 0,
        User = 1
    }
}