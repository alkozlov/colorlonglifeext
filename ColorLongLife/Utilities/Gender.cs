﻿namespace ColorLongLife.Utilities
{
    public enum Gender : int
    {
        /// <summary>
        /// Женский пол
        /// </summary>
        Female = 0,

        /// <summary>
        /// Мужской пол
        /// </summary>
        Male = 1
    }
}