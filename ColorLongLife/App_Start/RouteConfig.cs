﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ColorLongLife
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //    name: "Index",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Cart", action = "Index", id = UrlParameter.Optional }
            //);

            routes.MapRoute(
                name: "SignUp",
                url: "signup",
                defaults: new { controller = "Authorization", action = "SignUp" }
            );

            routes.MapRoute(
                name: "SignIn",
                url: "signin",
                defaults: new { controller = "Authorization", action = "SignIn" }
            );

            routes.MapRoute(
                name: "SignOut",
                url: "signout",
                defaults: new { controller = "Authorization", action = "SignOut" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

        }
    }
}
