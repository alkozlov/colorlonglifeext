﻿using System;
using System.Web.Mvc;
using ColorLongLife.Models.Home;
using ColorLongLife.DataAccessLayer.Models;

namespace ColorLongLife.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        public HomeController()
        {
            ViewBag.IsDisplayTopMenu = true;
            ViewBag.IsNotAutorized = false;
            ViewBag.Caption = "Персональный кабинет здоровья";
            HtmlHelper.ClientValidationEnabled = true;
            HtmlHelper.UnobtrusiveJavaScriptEnabled = true;
        }

        // GET: Home
        public ActionResult Index()
        {
            User user = GetUser();
            if (user != null)
            {
                IndexModel model = new IndexModel()
                {
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    MiddleName = user.MiddleName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return RedirectToAction("Index", "HealthPanel");
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }
    }
}