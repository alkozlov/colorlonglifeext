﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using ColorLongLife.Models.ApiModels;

namespace ColorLongLife.Controllers
{
    [RoutePrefix("cll/api/v1/onlineconsultation")]
    public class OnlineConsultationApiController : BaseApiController
    {
        [HttpGet]
        [Route("doctors")]
        public IEnumerable<DoctorModel> GetDoctors([FromUri] String checkDate)
        {
            List<DoctorModel> doctors = new List<DoctorModel>();
            doctors.AddRange(new List<DoctorModel>
            {
                new DoctorModel
                {
                    DoctorId = 0,
                    FirstName = "Иван",
                    LastName = "Иванов",
                    MiddleName = "Иванович",
                    Description = "Лечу от паноса, сглаза прямой кишки, запора.",
                    PathToImage = "/Content/img/spec1.png"
                },

                new DoctorModel
                {
                    DoctorId = 1,
                    FirstName = "Петр",
                    LastName = "Петров",
                    MiddleName = "Петрович",
                    Description = "Лечу от радикулита правой пятки и острых вирусных инфекций памяти.",
                    PathToImage = "/Content/img/spec2.png"
                },
                new DoctorModel
                {
                    DoctorId = 2,
                    FirstName = "Семен",
                    LastName = "Семенов",
                    MiddleName = "Семенович",
                    Description = "Лечу простуду методом \"Червячка (автор доктор Попов)\".",
                    PathToImage = "/Content/img/spec1.png"
                }
            });

            return doctors;
        }
    }
}
