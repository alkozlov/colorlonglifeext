﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using System.Web.Http.Results;
using System.Web.Mvc;
using ColorLongLife.DataAccessLayer.Models;
using ColorLongLife.Models.FreeExpressDiagnostics;
using ColorLongLife.Utilities;

namespace ColorLongLife.Controllers
{
    public class FreeExpressDiagnosticsController : BaseController
    {
        public FreeExpressDiagnosticsController()
        {
            ViewBag.IsDisplayTopMenu = true;
            ViewBag.IsNotAutorized = false;
            ViewBag.Caption = "Персональный кабинет экспресс-тестирования";
            HtmlHelper.ClientValidationEnabled = true;
            HtmlHelper.UnobtrusiveJavaScriptEnabled = true;
        }
        //
        // GET: /FreeExpressDiagnostics/
        public ActionResult Index()
        {
            User user = GetUser();
            if (user != null)
            {
                IndexModel model = new IndexModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult StepZero()
        {
            User user = GetUser();
            var category = _unitOfWork.CategoryRepository.Get();
            var emo = _unitOfWork.EmotionRepository.Get();
            if (user != null)
            {
                StepZeroModel model = new StepZeroModel()
                {
                    FirstName = user.FirstName,
                    Categories = category
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                ViewBag.HappinessList = _unitOfWork.HappinessRepository.Get().ToList();
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult InformationAboutTest()
        {
            User user = GetUser();
            if (user != null)
            {
                InformationAboutTestModel model = new InformationAboutTestModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        [HttpGet]
        public ActionResult ExpressDiagnosticsResults()
        {
            User user = GetUser();
            if (user != null)
            {
                ExpressDiagnosticsResultsModel resultModel = new ExpressDiagnosticsResultsModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(resultModel);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        [HttpPost]
        public async Task<ActionResult> ExpressDiagnosticsResults(ExpressDiagnosticModel model)
        {
            User user = GetUser();
            if (user != null)
            {
                // Validate express diagnostic results
                Happiness happines = this._unitOfWork.HappinessRepository.GetById(model.HappinessId);
                if (happines == null)
                {
                    return new EmptyResult();
                }
                var categoryIds = model.Sectors.Select(x => x.CategoryId);
                var subcategoryIds = model.Sectors.Select(x => x.SubcategoryId);
                var emotionIds = model.Sectors.Select(x => x.EmotionId).Distinct();
                Int32 categoriesCount = this._unitOfWork.CategoryRepository.Get(x => categoryIds.Contains(x.IdCategory)).Count();
                Int32 subcategoriesCount = this._unitOfWork.SubcategoryRepository.Get(x => subcategoryIds.Contains(x.IdSubcategory)).Count();
                Int32 emotionsCount = this._unitOfWork.EmotionRepository.Get(x => emotionIds.Contains(x.EmotionId)).Count();
                if (categoryIds.Count() != categoriesCount
                    || subcategoryIds.Count() != subcategoriesCount
                    || emotionIds.Count() != emotionsCount)
                {
                    return new EmptyResult();
                }

                Diagnostic diagnostic = new Diagnostic
                {
                    UserId = user.UserId,
                    HappinessId = model.HappinessId,
                    CreateDateTimeUtc = DateTime.UtcNow
                };
                List<DiagramSector> diagramSectors = model.Sectors.Select(sector => new DiagramSector
                {
                    SectorType = (byte) SectorType.Sector,
                    Color = sector.Color,
                    Opacity = sector.Opacity,
                    CategoryId = sector.CategoryId,
                    SubcategoryId = sector.SubcategoryId,
                    EmotionId = sector.EmotionId
                }).ToList();
                diagnostic.DiagramSectors = diagramSectors;
                this._unitOfWork.DiagnosticRepository.Insert(diagnostic);
                await this._unitOfWork.SaveAsync();

                return RedirectToAction("ExpressDiagnosticsResults", "FreeExpressDiagnostics");
            }
            else
            {
                //return RedirectToAction("SignIn", "Authorization");
                return new EmptyResult();
            }
        }

        public ActionResult ExpressDiagnosticsExample()
        {
            User user = GetUser();
            if (user != null)
            {
                ExpressDiagnosticsExampleModel model = new ExpressDiagnosticsExampleModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult StepOne()
        {
            User user = GetUser();
            if (user != null)
            {
                StepOneModel model = new StepOneModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult ArchiveResults()
        {
            User user = GetUser();
            if (user != null)
            {
                ArchiveResultsModel model = new ArchiveResultsModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        [HttpGet]
        public JsonResult SubcategoriesGet(Int32 categoryId)
        {
            string mainUser = HttpContext.User.Identity.Name;
            var user = _unitOfWork.UserRepository.Get(x => x.Email.Equals(mainUser)).FirstOrDefault();
            List<Subcategory> subcategories = _unitOfWork.SubcategoryRepository.Get(x => x.IdCategory.Equals(categoryId) && x.Gender == user.Gender).ToList();
            return Json(subcategories.Select(x => new
            {
                SubcategoryId = x.IdSubcategory,
                Name = x.Name,
                Gender = x.Gender
            }).ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult EmotionGet()
        {
            IEnumerable<Emotion> parentEmotions = _unitOfWork.EmotionRepository.Get(x => !x.ParentId.HasValue);
            IEnumerable<Emotion> childEmotions =
                _unitOfWork.EmotionRepository.Get(
                    x => parentEmotions.Any(parent => x.ParentId.HasValue && x.ParentId.Value.Equals(parent.EmotionId)));

            return Json(parentEmotions.Select(x => new
            {
                EmotionId = x.EmotionId,
                Title = x.Title,
                Description = x.Description,
                childs = childEmotions.Where(child => child.ParentId.HasValue && child.ParentId.Value.Equals(x.EmotionId)).Select(emotion => new
                {
                    EmotionId = emotion.EmotionId,
                    Title = emotion.Title,
                    Description = emotion.Description
                })
            }).ToList(), JsonRequestBehavior.AllowGet);
        }

    }
}