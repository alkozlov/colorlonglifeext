﻿using System;
using System.Data.Entity.Core;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using ColorLongLife.DataAccessLayer.Models;
using ColorLongLife.Models.ShoppingCart;

namespace ColorLongLife.Controllers
{
    public class ShoppingCartController : BaseController
    {
        public ShoppingCartController()
        {
            ViewBag.IsDisplayTopMenu = true;
            ViewBag.IsNotAutorized = false;
            ViewBag.Caption = "Персональный кабинет здоровья";
            HtmlHelper.ClientValidationEnabled = true;
            HtmlHelper.UnobtrusiveJavaScriptEnabled = true;
        }
        public ViewResult Index(Cart cart, string returnUrl)
        {
            User user = GetUser();
            ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
            return View(new CartIndexViewModel()
            {
                Cart = cart,
                ReturnUrl = returnUrl
            });
        }
        public RedirectToRouteResult AddToCart(Cart cart, int productId, string pathAndQuery)
        {
            Product product = _unitOfWork.ProductRepository.Get(p => p.ProductId == productId).FirstOrDefault();
            if (product != null)
            {
                cart.AddItem(product, 1);
            }
            return RedirectToAction("Index", new { pathAndQuery });
        }

        public RedirectToRouteResult RemoveFromCart(Cart cart, int productId, string returnUrl)
        {
            Product product = _unitOfWork.ProductRepository.Get(p => p.ProductId == productId).FirstOrDefault();
            if (product != null)
            {
                cart.RemoveLine(product);
            }
            return RedirectToAction("Index", new { returnUrl });
        }
        public PartialViewResult Summary(Cart cart)
        {
            return PartialView(cart);
        }
        public RedirectToRouteResult ClearAll(Cart cart)
        {
            cart.Clear();
            return RedirectToAction("Index");
        }

        [HttpGet, ActionName("Payment")]
        public ActionResult PaymentGet()
        {
            var mainUser = HttpContext.User.Identity.Name;
            User user = _unitOfWork.UserRepository.Get(
                    x => String.Compare(x.Email, mainUser, StringComparison.OrdinalIgnoreCase) == 0 && !x.IsDeleted)
                    .FirstOrDefault();

            if (user == null)
            {
                throw new ObjectNotFoundException("Пользователь не найден.");
            }

            PaymentModel model = new PaymentModel()
            {
                UserId = user.UserId,
                FirstName = user.FirstName,
                LastName = user.LastName,
                CountryId = user.CountryId ?? 1,
                CityId = user.CityId ?? 1,
                Phone = user.Phone,
            };

            #region Countries and cities

            var countries = GetCountrySelectList();
            ViewBag.Countries = countries;
            ViewBag.Cities = GetCitiesSelectList(user.CountryId ?? 1);

            #endregion

            ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
            return View(model);
        }

        [HttpPost, ActionName("Payment")]
        public async Task<ActionResult> PaymentPost(PaymentModel model)
        {
            SelectList countries;
            if (ModelState.IsValid)
            {
                User user = GetUser();//await _unitOfWork.UserRepository.GetByIdAsync(model.UserId);
                

                // 1 - Если пользователь не найден
                if (user == null)
                {
                    countries = GetCountrySelectList();
                    ViewBag.Countries = countries;
                    ViewBag.Cities = GetCitiesSelectList(Int32.Parse(countries.First().Value));
                    ModelState.AddModelError("user", "Неизвестный пользователь.");

                    return View(model);
                }

                // 2 - Если все данные валидны, то обновляем пользователя и сохраняем измененые данные в базу
                Order order = new Order
                {
                    UserId = model.UserId,
                    OrderDateCreated = DateTime.UtcNow,
                    Address = model.Address,
                    PostCode = model.Postcode,
                    Status = 1
                };
                _unitOfWork.OrderRepository.Insert(order);

                await _unitOfWork.SaveAsync();
                ViewBag.IsUpdateSuccess = true;
                countries = GetCountrySelectList();
                ViewBag.Countries = countries;
                ViewBag.Cities = GetCitiesSelectList(model.CountryId);

                return View(model);
            }

            #region Countries and cities

            countries = GetCountrySelectList();
            ViewBag.Countries = countries;
            ViewBag.Cities = GetCitiesSelectList(model.CountryId);

            #endregion

            User users = GetUser();
            ViewBag.UserName = String.Format("{0} {1} {2}", users.LastName, users.FirstName, users.MiddleName);
            return View(model);
        }
    }
}