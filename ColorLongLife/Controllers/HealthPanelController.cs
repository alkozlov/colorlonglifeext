﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using ColorLongLife.DataAccessLayer;
using ColorLongLife.DataAccessLayer.Models;
using ColorLongLife.Extensions;
using ColorLongLife.Models.HealthPanel;
using ColorLongLife.Utilities;

namespace ColorLongLife.Controllers
{
    [Authorize]
    public class HealthPanelController : BaseController
    {
        private const Int32 ConsultationDaysInWeek = 5;

        public HealthPanelController()
        {
            ViewBag.IsDisplayTopMenu = true;
            ViewBag.IsNotAutorized = false;
            ViewBag.Caption = "Персональный кабинет здоровья";
            HtmlHelper.ClientValidationEnabled = true;
            HtmlHelper.UnobtrusiveJavaScriptEnabled = true;
        }

        // GET: HealthPanel
        public ActionResult Index()
        {
            User user = GetUser();
            if (user != null)
            {
                IndexModel model = new IndexModel()
                {
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    MiddleName = user.MiddleName,
                    Phone = user.Phone,
                    Birthday = user.Birthday,
                    Email = user.Email
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        [HttpGet, ActionName("EditProfile")]
        public ActionResult EditProfileGet()
        {
            var mainUser = HttpContext.User.Identity.Name;
            User user =
                _unitOfWork.UserRepository.Get(
                    x => String.Compare(x.Email, mainUser, StringComparison.OrdinalIgnoreCase) == 0 && !x.IsDeleted)
                    .FirstOrDefault();

            if (user == null)
            {
                throw new ObjectNotFoundException("Пользователь не найден.");
            }

            EditProfileModel model = new EditProfileModel()
            {
                UserId = user.UserId,
                FirstName = user.FirstName,
                LastName = user.LastName,
                MiddleName = user.MiddleName,
                Birthday = user.Birthday,
                CountryId = user.CountryId ?? 1,
                CityId = user.CityId ?? 1,
                Email = user.Email,
                Gender = (Gender)user.Gender,
                Weight = user.Weight,
                Growth = user.Growth,
                Phone = user.Phone,
                OldPassword = string.Empty,
                NewPassword = string.Empty,
                ConfirmPassword = string.Empty
            };

            #region Countries and cities

            var countries = GetCountrySelectList();
            ViewBag.Countries = countries;
            ViewBag.Cities = GetCitiesSelectList(user.CountryId ?? 1);

            #endregion
            ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
            return View(model);
        }

        [HttpPost, ActionName("EditProfile")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditProfilePost(EditProfileModel model)
        {
            SelectList countries;
            if (ModelState.IsValid)
            {
                User user = await _unitOfWork.UserRepository.GetByIdAsync(model.UserId);

                // 1 - Если пользователь не найден
                if (user == null)
                {
                    countries = GetCountrySelectList();
                    ViewBag.Countries = countries;
                    ViewBag.Cities = GetCitiesSelectList(Int32.Parse(countries.First().Value));
                    model.OldPassword = string.Empty;
                    model.NewPassword = string.Empty;
                    model.ConfirmPassword = string.Empty;
                    ModelState.AddModelError("user", "Неизвестный пользователь.");

                    return View(model);
                }

                // 2 - если пользователь изменил свой email, то проверяем нет ли в базе такого
                Boolean isEmailChanged = false;
                #region Duplicate email validation

                if (String.Compare(user.Email, model.Email, StringComparison.OrdinalIgnoreCase) != 0)
                {
                    User tempUser =
                        _unitOfWork.UserRepository.Get(
                            x =>
                                String.Compare(x.Email, model.Email, StringComparison.OrdinalIgnoreCase) == 0 &&
                                !x.IsDeleted)
                            .SingleOrDefault();
                    if (tempUser != null)
                    {
                        countries = GetCountrySelectList();
                        ViewBag.Countries = countries;
                        ViewBag.Cities = GetCitiesSelectList(model.CountryId);
                        model.OldPassword = string.Empty;
                        model.NewPassword = string.Empty;
                        model.ConfirmPassword = string.Empty;
                        ModelState.AddModelError("user.Email", "Такой email уже используется");

                        return View(model);
                    }

                    isEmailChanged = true;
                }

                #endregion

                // 3 - Если пользователь решил изменить пароль
                #region Password validation

                if (!String.IsNullOrEmpty(model.OldPassword) &&
                    !String.IsNullOrEmpty(model.NewPassword) &&
                    !String.IsNullOrEmpty(model.ConfirmPassword))
                {
                    String oldPasswordHash = CryptographyHelper.HashPassword(model.OldPassword);
                    if (oldPasswordHash == user.UserPasswordHash && model.NewPassword == model.ConfirmPassword)
                    {
                        user.UserPasswordHash = CryptographyHelper.HashPassword(model.NewPassword);
                    }
                    else
                    {
                        ModelState.AddModelError("user",
                            oldPasswordHash != user.UserPasswordHash
                                ? "Вы ввели неверный пароль."
                                : "Новый пароль и его подтверждение должны совпадать.");

                        countries = GetCountrySelectList();
                        ViewBag.Countries = countries;
                        ViewBag.Cities = GetCitiesSelectList(model.CountryId);
                        model.OldPassword = string.Empty;
                        model.NewPassword = string.Empty;
                        model.ConfirmPassword = string.Empty;
                    }
                }

                #endregion

                // 4 - Проверяем День Рождения
                #region Birthday validation

                DateTime currentDate = DateTime.UtcNow;
                if (model.Birthday < currentDate.AddYears(-100) || model.Birthday > currentDate.AddYears(-16))
                {
                    ModelState.AddModelError("model.Birthday", "Возможно вы ввели неверную дату рождения.");
                    countries = GetCountrySelectList();
                    ViewBag.Countries = countries;
                    ViewBag.Cities = GetCitiesSelectList(model.CountryId);
                    model.OldPassword = string.Empty;
                    model.NewPassword = string.Empty;
                    model.ConfirmPassword = string.Empty;
                    ModelState.AddModelError("user", "Неизвестный пользователь.");

                    return View(model);
                }

                #endregion

                // 5 - Если все данные валидны, то обновляем пользователя и сохраняем измененые данные в базу
                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
                user.MiddleName = model.MiddleName;
                user.Birthday = model.Birthday;
                user.CountryId = model.CountryId;
                user.CityId = model.CityId;
                user.Email = model.Email;
                user.Gender = (int) model.Gender;
                user.Growth = model.Growth;
                user.Weight = model.Weight;
                user.Phone = model.Phone;

                _unitOfWork.UserRepository.Update(user);
                await _unitOfWork.SaveAsync();
                ViewBag.IsUpdateSuccess = true;
                countries = GetCountrySelectList();
                ViewBag.Countries = countries;
                ViewBag.Cities = GetCitiesSelectList(model.CountryId);
                model.OldPassword = string.Empty;
                model.NewPassword = string.Empty;
                model.ConfirmPassword = string.Empty;

                if (isEmailChanged)
                {
                    FormsAuthentication.SetAuthCookie(user.Email, true);
                }
                return View(model);
            }

            #region Countries and cities

            countries = GetCountrySelectList();
            ViewBag.Countries = countries;
            ViewBag.Cities = GetCitiesSelectList(model.CountryId);

            #endregion

            User users = GetUser();
            ViewBag.UserName = String.Format("{0} {1} {2}", users.LastName, users.FirstName, users.MiddleName);
            return View(model);
        }

        [HttpGet, ActionName("DeleteProfile")]
        public async Task<ActionResult> DeleteProfileGet(String userId)
        {
            if (!String.IsNullOrEmpty(userId))
            {
                if (String.Compare(HttpContext.User.Identity.Name, userId, StringComparison.OrdinalIgnoreCase) == 0)
                {
                    User user =
                        _unitOfWork.UserRepository.Get(
                            x =>
                                String.Compare(x.Email, userId, StringComparison.OrdinalIgnoreCase) == 0 && !x.IsDeleted)
                            .SingleOrDefault();
                    if (user != null)
                    {
                        user.UserStatus = 3;
                        _unitOfWork.UserRepository.Update(user);
                        await _unitOfWork.SaveAsync();
                        FormsAuthentication.SignOut();
                        return Redirect("~/SignUp");
                    }
                }
            }

            HttpStatusCodeResult result = new HttpNotFoundResult("User Not Found!");

            return result;
        }

        //public ActionResult UpdateSuccess()
        //{
        //    return View();
        //}

        public ActionResult DictionaryTerms()
        {
            DictionaryTermsModel model = new DictionaryTermsModel();
            User user = GetUser();
            ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
            return View(model);
        }

        #region SendAFile
        [HttpGet]
        public ActionResult SendAFile()
        {
            User user = GetUser();
            if (user != null)
            {
                SendAFileModel model = new SendAFileModel()
                {
                    FirstName = user.FirstName,
                    LastName = user.LastName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult SendAFile(HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
            {
                // получаем имя файла
                string fileName = Path.GetFileName(file.FileName);
                // сохраняем файл в папку Files в проекте
                file.SaveAs(Server.MapPath("~/Files/" + fileName));
                User user = GetUser();
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return RedirectToAction("SendAFileResponse");
            }
            else
            {
                return View();
            }

        }

        public ActionResult SendAFileResponse()
        {
            User user = GetUser();
            if (user != null)
            {
                SendAFileResponseModel model = new SendAFileResponseModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }
        #endregion

        #region DownloadProgram
        [HttpGet, ActionName("DownloadProgram")]
        public ActionResult DownloadProgramGet()
        {
            DownloadProgramModel model = new DownloadProgramModel();
            User user = GetUser();
            ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
            return View(model);
        }

        [HttpPost, ActionName("DownloadProgram")]
        public ActionResult DownloadProgramPost(HttpPostedFileBase file)
        {
            string path = Server.MapPath("~/Content/img/info.png");
            FileStream fs = new FileStream(path, FileMode.Open);
            string file_type = "application/png";
            string file_name = "info.png";
            return File(fs, file_type, file_name);
        }
        #endregion

        #region Write to us
        [HttpGet, ActionName("CustomerRequest")]
        public ActionResult CustomerRequestGet()
        {
            CustomerRequestModel model = new CustomerRequestModel();
            var topics = GetTopicSelectList();
            ViewBag.Topics = topics.Any() ? topics : new SelectList(new List<TopicTreatment>());
            User user = GetUser();
            ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
            return View(model);
        }

        [HttpPost, ActionName("CustomerRequest")]
        public async Task<ActionResult> CustomerRequestPost(CustomerRequestModel model)
        {
            if (model == null)
                ModelState.AddModelError("model", "Все поля должны быть заполнены");

            if (ModelState.IsValid)
            {
                CustomerRequest dataUser = new CustomerRequest
                {
                    Email = model.Email,
                    DateSend = DateTime.UtcNow,
                    Message = model.Message,
                    TopicId = model.TopicId
                };
                
                _unitOfWork.CustomerRequestRepository.Insert(dataUser);
                await _unitOfWork.SaveAsync();
                return RedirectToAction("RegistrationSuccess", "Authorization");
            }
            User user = GetUser();
            ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
            return View(model);
        }
        #endregion

        #region Helpers

        [HttpGet, ActionName("Cities")]
        [AllowAnonymous]
        public JsonResult CitiesGet(Int32 countryId)
        {
            List<City> cities = _unitOfWork.CityRepository.Get(x => x.CountryId.Equals(countryId)).ToList();
            return cities.Any()
                ? Json(cities.Select(x => new
                {
                    cityId = x.CityId,
                    name = x.Name
                }).OrderBy(x => x.name).ToList(), JsonRequestBehavior.AllowGet)
                : Json(null, JsonRequestBehavior.AllowGet);
        }

        private SelectList GetTopicSelectList()
        {
            IEnumerable<TopicTreatment> topics = _unitOfWork.TopicTreatmentRepository.Get();
            if (topics.Any())
                return new SelectList(topics, "TopicId", "Name");

            return new SelectList(new List<TopicTreatment>());
        }
        #endregion

        [HttpGet, ActionName("OnlineConsultation")]
        public ActionResult OnlineConsultation(String period)
        {
            OnlineConsultationModel model = new OnlineConsultationModel();
            User currentUser = this._unitOfWork.UserRepository.Get(
                x =>
                    String.Compare(x.Email, this.HttpContext.User.Identity.Name, StringComparison.InvariantCulture) == 0
                    && !x.IsDeleted).SingleOrDefault();
            if (currentUser != null)
            {
                model.FirstName = currentUser.FirstName;
            }

            DateTime consulatationStartDate = DateTime.UtcNow;
            // If period is wrong then use current date
            if (!String.IsNullOrEmpty(period))
            {
                DateTime.TryParse(period, out consulatationStartDate);
            }

            // Get days current week
            DateTime startWeekDate = consulatationStartDate.StartOfWeek(DayOfWeek.Monday).Date;
            List<DayOfWeekModel> days = new List<DayOfWeekModel>();
            for (int i = 0; i < ConsultationDaysInWeek; i++)
            {
                DateTime date = startWeekDate.AddDays(i);
                List<TimeslotModel> timeslots = new List<TimeslotModel>();
                TimeSpan nextTimeslot = new TimeSpan(0, 9, 0, 0);
                while (nextTimeslot.Hours <= 18)
                {
                    TimeslotModel timeslot = new TimeslotModel
                    {
                        StartTime = nextTimeslot
                    };
                    timeslots.Add(timeslot);
                    nextTimeslot += new TimeSpan(0, Constants.DefaultTimeslotDuration, 0) +
                                    new TimeSpan(0, Constants.DefaultRestDuration, 0);
                }
                DayOfWeekModel dayOfWeek = new DayOfWeekModel
                {
                    Timeslots = timeslots,
                    Date = date,
                    ShortName = date.DayOfWeek.ToString("G")
                };
                days.Add(dayOfWeek);
            }
            model.Days = days;
            User user = GetUser();
            ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
            return View(model);
        }
    }
}