﻿using System;
using System.Web.Mvc;
using ColorLongLife.DataAccessLayer.Models;
using ColorLongLife.Models.MyFamily;

namespace ColorLongLife.Controllers
{
    public class MyFamilyController : BaseController
    {
        public MyFamilyController()
        {
            ViewBag.IsDisplayTopMenu = true;
            ViewBag.IsNotAutorized = false;
            ViewBag.Caption = "Персональный кабинет здоровья";
            HtmlHelper.ClientValidationEnabled = true;
            HtmlHelper.UnobtrusiveJavaScriptEnabled = true;
        }
        public ActionResult Index()
        {
            User user = GetUser();
            if (user != null)
            {
                IndexModel model = new IndexModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult Introduction()
        {
            User user = GetUser();
            if (user != null)
            {
                IntroductionModel model = new IntroductionModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult FunctionBrain()
        {
            User user = GetUser();
            if (user != null)
            {
                FunctionBrainModel model = new FunctionBrainModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult ActionAndInformation()
        {
            User user = GetUser();
            if (user != null)
            {
                ActionAndInformationModel model = new ActionAndInformationModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult Communicability()
        {
            User user = GetUser();
            if (user != null)
            {
                CommunicabilityModel model = new CommunicabilityModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult PsychoEmotionalPattern()
        {
            User user = GetUser();
            if (user != null)
            {
                PsychoEmotionalPatternModel model = new PsychoEmotionalPatternModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult PersonalityCharacteristics()
        {
            User user = GetUser();
            if (user != null)
            {
                PersonalityCharacteristicsModel model = new PersonalityCharacteristicsModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult Conclusions()
        {
            User user = GetUser();
            if (user != null)
            {
                ConclusionsModel model = new ConclusionsModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult HealthPFS()
        {
            User user = GetUser();
            if (user != null)
            {
                HealthPFSModel model = new HealthPFSModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult HealthOrganism()
        {
            User user = GetUser();
            if (user != null)
            {
                HealthOrganismModel model = new HealthOrganismModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult HealthFUS()
        {
            User user = GetUser();
            if (user != null)
            {
                HealthFUSModel model = new HealthFUSModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult HealthBrain()
        {
            User user = GetUser();
            if (user != null)
            {
                HealthBrainModel model = new HealthBrainModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult HealthRecommendations()
        {
            User user = GetUser();
            if (user != null)
            {
                HealthRecommendationsModel model = new HealthRecommendationsModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }
	}
}