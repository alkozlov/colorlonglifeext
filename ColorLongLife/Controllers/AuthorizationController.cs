﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;
using ColorLongLife.DataAccessLayer;
using ColorLongLife.DataAccessLayer.Models;
using ColorLongLife.Email;
using ColorLongLife.Models.Authorization;
using ColorLongLife.Utilities;

namespace ColorLongLife.Controllers
{
    public class AuthorizationController : BaseController
    {
        public AuthorizationController()
        {
            ViewBag.IsDisplayTopMenu = false;
            ViewBag.IsNotAutorized = true;
            HtmlHelper.ClientValidationEnabled = true;
            HtmlHelper.UnobtrusiveJavaScriptEnabled = true;
        }

        [HttpGet, ActionName("SignUp")]
        public ActionResult SignUpGet()
        {
            SignUpModel model = new SignUpModel();
            model.Birthday = new DateTime(1990, 10, 15);

            var countries = GetCountrySelectList();
            ViewBag.Countries = countries;
            ViewBag.Cities = GetCitiesSelectList(Int32.Parse(countries.First().Value));
            //model.Birthday = DateTime.UtcNow;

            return View(model);
        }

        [HttpPost, ActionName("SignUp")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SignUpPost(SignUpModel model)
        {
            SelectList tempList;

            // 1 - Валидация
            if (ModelState.IsValid)
            {
                // 2 - Проверяем на дублирование почты пользователя
                User user = _unitOfWork.UserRepository.Get(x => x.Email.Equals(model.Email) && !x.IsDeleted).SingleOrDefault();
                
                if (user != null)
                {
                    ModelState.AddModelError("user", "Пользователь с таким адресом электронной почты уже зарегистрирован.");
                    model.Password = string.Empty;
                    model.ConfirmPassword = string.Empty;
                    tempList = GetCountrySelectList();
                    ViewBag.Countries = tempList;
                    ViewBag.Cities = GetCitiesSelectList(Int32.Parse(tempList.First().Value));

                    return View(model);
                }

                //DateTime birthday;
                //var isDateValid = DateTime.TryParse(model.Birthday, out birthday);
                //if (!isDateValid) ModelState.AddModelError("Birthday", "Birthday needs to be a valid date.");


                // 3 - Валидация дня рождения
                DateTime currentDate = DateTime.UtcNow;
                if (model.Birthday < currentDate.AddYears(-100) || model.Birthday > currentDate.AddYears(-16))
                {
                    ModelState.AddModelError("model.Birthday", "Допустимый возраст пользователей от 16 и старше.");
                    model.Password = string.Empty;
                    model.ConfirmPassword = string.Empty;
                    tempList = GetCountrySelectList();
                    ViewBag.Countries = tempList;
                    ViewBag.Cities = GetCitiesSelectList(Int32.Parse(tempList.First().Value));

                    return View(model);
                }

                // 4 - Шифруем пароль
                String hashedPassword = CryptographyHelper.HashPassword(model.Password);

                // 5 - Генерируем ключ активации
                String activationToken = CryptographyHelper.GenerateActivationToken();

                // 6 - Создаем пользователя и сохраняем его в БД
                User dataUser = new User
                {
                    FirstName = model.FirstName,
                    MiddleName = model.MiddleName,
                    LastName = model.LastName,
                    CountryId = model.CountryId,
                    CityId = model.CityId,
                    Birthday = model.Birthday,
                    CreateDate = DateTime.UtcNow,
                    Email = model.Email,
                    ConfirmationToken = activationToken,
                    UserPasswordHash = hashedPassword,
                    Gender = (int)model.Gender,
                    Growth = model.Growth,
                    Weight = model.Weight,
                    IsDeleted = false,
                    UserAccountType = (int)AccountType.Customer,
                    UserStatus = (int)AccountStatus.PrepareActivation,
                    LastVisitDate = DateTime.UtcNow,
                    Phone = model.Phone
                };
                _unitOfWork.UserRepository.Insert(dataUser);
                await _unitOfWork.SaveAsync();

                // 7 - Отправляем письмо для подтверждения регистрации
                if (HttpContext.Request.Url != null)
                {
                    String siteUrl = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.AbsolutePath, "");
                    BaseMailMessage mailMessage =
                        new ConfirmRegistrationMailMessage(dataUser, siteUrl);
                    await Emailer.SendMailAsync(mailMessage);
                }

                return RedirectToAction("RegistrationSuccess");
            }

            tempList = GetCountrySelectList();
            ViewBag.Countries = tempList;
            ViewBag.Cities = GetCitiesSelectList(Int32.Parse(tempList.First().Value));

            model.Password = string.Empty;
            model.ConfirmPassword = string.Empty;
            return View(model);
        }

        public ActionResult RegistrationSuccess()
        {
            return View();
        }

        [HttpGet, ActionName("SignIn")]
        public ActionResult SignInGet()
        {
            SignInModel model = new SignInModel();
            return View(model);
        }

        [HttpPost, ActionName("SignIn")]
        [ValidateAntiForgeryToken]
        public ActionResult SignInPost(SignInModel model)
        {
            if (ModelState.IsValid)
            {
                IEnumerable<User> users = _unitOfWork.UserRepository.Get(x => x.Email == model.Email && !x.IsDeleted);
                User user = users.FirstOrDefault();
                if (user == null)
                {
                    ModelState.AddModelError("user", "Пользователь не найден.");
                    model.Password = string.Empty;
                    return View(model);
                }
                var password = CryptographyHelper.HashPassword(model.Password);
                if (user.UserPasswordHash == password)
                {
                    FormsAuthentication.SetAuthCookie(user.Email, true);
                    return RedirectToAction("Index", "HealthPanel");
                }
                else
                {
                    if (user.UserPasswordHash != password)
                    {
                        model.Password = string.Empty;
                        ModelState.AddModelError("user", "Неверный пароль");
                    }
                }
            }
            model.Password = string.Empty;
            return View(model);
        }

        [HttpGet]
        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();

            return Redirect("signin");
        }

        [HttpGet]
        public async Task<ActionResult> ActivatePanel(string keyToken)
        {
            IEnumerable<User> users = _unitOfWork.UserRepository.Get(x => x.ConfirmationToken == keyToken);
            User user = users.FirstOrDefault();

            if (user == null)
            {
                ModelState.AddModelError("user", "Неизвестный пользователь.");

                return HttpNotFound();
            }

            user.UserStatus = (int)AccountStatus.Active;
            _unitOfWork.UserRepository.Update(user);
            await _unitOfWork.SaveAsync();

            return Redirect("Signin");
        }
    }
}