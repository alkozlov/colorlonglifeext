﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ColorLongLife.DataAccessLayer.Models;
using ColorLongLife.Models.DiagnosticResults;

namespace ColorLongLife.Controllers
{
    [Authorize]
    public class DiagnosticResultsController : BaseController
    {
        public DiagnosticResultsController()
        {
            ViewBag.IsDisplayTopMenu = true;
            ViewBag.IsNotAutorized = false;
            ViewBag.Caption = "Персональный кабинет здоровья";
            HtmlHelper.ClientValidationEnabled = true;
            HtmlHelper.UnobtrusiveJavaScriptEnabled = true;
        }

        public ActionResult Index()
        {
            User user = GetUser();
            if (user != null)
            {
                IndexModel model = new IndexModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult FunctionBrain()
        {
            User user = GetUser();
            if (user != null)
            {
                FunctionBrainModel model = new FunctionBrainModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult ActionsAndInformation()
        {
            User user = GetUser();
            if (user != null)
            {
                ActionsAndInformationModel model = new ActionsAndInformationModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult Communicability()
        {
            User user = GetUser();
            if (user != null)
            {
                CommunicabilityModel model = new CommunicabilityModel()
                {
                    FirstName = user.FirstName,
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult PsychoEmotionalPattern()
        {
            User user = GetUser();
            if (user != null)
            {
                PsychoEmotionalPatternModel model = new PsychoEmotionalPatternModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult PersonalityCharacteristics()
        {
            User user = GetUser();
            if (user != null)
            {
                PersonalityCharacteristicsModel model = new PersonalityCharacteristicsModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult Conclusions()
        {
            User user = GetUser();
            if (user != null)
            {
                ConclusionsModel model = new ConclusionsModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult HealthPFS()
        {
            User user = GetUser();
            if (user != null)
            {
                HealthPFSModel model = new HealthPFSModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult HealthOrganism()
        {
            User user = GetUser();
            if (user != null)
            {
                HealthOrganismModel model = new HealthOrganismModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult HealthFUS()
        {
            User user = GetUser();
            if (user != null)
            {
                HealthFUSModel model = new HealthFUSModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult HealthBrain()
        {
            User user = GetUser();
            if (user != null)
            {
                HealthBrainModel model = new HealthBrainModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult HealthRecommendations()
        {
            User user = GetUser();
            if (user != null)
            {
                HealthRecommendationsModel model = new HealthRecommendationsModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult RecommendedCorrection()
        {
            User user = GetUser();
            if (user != null)
            {
                RecommendedCorrectionModel model = new RecommendedCorrectionModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult MyCorretion()
        {
            User user = GetUser();
            if (user != null)
            {
                MyCorretionModel model = new MyCorretionModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult LongCorretion()
        {
            User user = GetUser();
            if (user != null)
            {
                LongCorretionModel model = new LongCorretionModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }
    }
}