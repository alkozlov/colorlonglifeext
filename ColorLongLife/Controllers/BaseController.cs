﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ColorLongLife.DataAccessLayer;
using ColorLongLife.DataAccessLayer.Models;

namespace ColorLongLife.Controllers
{
    public abstract class BaseController : Controller
    {
        protected UnitOfWork _unitOfWork;

        protected BaseController()
        {
            _unitOfWork = new UnitOfWork();
        }

        #region Helpers

        protected SelectList GetCountrySelectList()
        {
            IEnumerable<Country> countries = this._unitOfWork.CountryRepository.Get();
            if (countries.Any())
            {
                return new SelectList(countries, "CountryId", "Name");
            }

            return new SelectList(new List<Country>());
        }

        protected SelectList GetCitiesSelectList(Int32 countryId)
        {
            IEnumerable<City> cities = this._unitOfWork.CityRepository.Get(x => x.CountryId.Equals(countryId),
                query => query.OrderBy(x => x.Name));
            if (cities.Any())
            {
                return new SelectList(cities, "CityId", "Name");
            }

            return new SelectList(new List<City>());
        }
        #endregion

        protected User GetUser()
        {
            UnitOfWork unitOfWork = new UnitOfWork();
            var mainUser = HttpContext.User.Identity.Name;
            User user = _unitOfWork.UserRepository.Get(x => String.Compare(x.Email, mainUser, StringComparison.OrdinalIgnoreCase) == 0 && x.UserStatus!= 3)
                    .FirstOrDefault();
            return user;
        }
    }
}