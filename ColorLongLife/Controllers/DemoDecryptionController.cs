﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ColorLongLife.DataAccessLayer.Models;
using ColorLongLife.Models.DemoDecryption;

namespace ColorLongLife.Controllers
{
    [Authorize]
    public class DemoDecryptionController : BaseController
    {
        public DemoDecryptionController()
        {
            ViewBag.IsDisplayTopMenu = true;
            ViewBag.IsNotAutorized = false;
            ViewBag.Caption = "Персональный кабинет здоровья";
            HtmlHelper.ClientValidationEnabled = true;
            HtmlHelper.UnobtrusiveJavaScriptEnabled = true;
        }

        public ActionResult Index()
        {
            User user = GetUser();
            if (user != null)
            {
                IndexModel model = new IndexModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = $"{user.LastName} {user.FirstName} {user.MiddleName}";
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult DemoFunctionBrain()
        {
            User user = GetUser();
            if (user != null)
            {
                DemoFunctionBrainModel model = new DemoFunctionBrainModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult DemoActionsAndInformation()
        {
            User user = GetUser();
            if (user != null)
            {
                DemoActionsAndInformationModel model = new DemoActionsAndInformationModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        #region
        public ActionResult DemoCommunicability()
        {
            User user = GetUser();
            if (user != null)
            {
                DemoCommunicabilityModel model = new DemoCommunicabilityModel()
                {
                    FirstName = user.FirstName,
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }
        #endregion
        public ActionResult DemoPsychoEmotionalPattern()
        {
            User user = GetUser();
            if (user != null)
            {
                DemoPsychoEmotionalPatternModel model = new DemoPsychoEmotionalPatternModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult DemoPersonalityCharacteristics()
        {
            User user = GetUser();
            if (user!=null)
            {
                DemoPersonalityCharacteristicsModel model = new DemoPersonalityCharacteristicsModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }
        
        public ActionResult DemoConclusions()
        {
            User user = GetUser();
            if (user != null)
            {
                DemoConclusionsModel model = new DemoConclusionsModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult DemoHealthPFS()
        {
            User user = GetUser();
            if (user != null)
            {
                DemoHealthPFSModel model = new DemoHealthPFSModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult DemoHealthOrganism()
        {
            User user = GetUser();
            if (user != null)
            {
                DemoHealthOrganismModel model = new DemoHealthOrganismModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult DemoHealthFUS()
        {
            User user = GetUser();
            if (user != null)
            {
                DemoHealthFUSModel model = new DemoHealthFUSModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult DemoHealthBrain()
        {
            User user = GetUser();
            if (user != null)
            {
                DemoHealthBrainModel model = new DemoHealthBrainModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult DemoHealthRecommendations()
        {
            User user = GetUser();
            if (user != null)
            {
                DemoHealthRecommendationsModel model = new DemoHealthRecommendationsModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult DemoRecommendedCorrection()
        {
            User user = GetUser();
            if (user != null)
            {
                DemoRecommendedCorrectionModel model = new DemoRecommendedCorrectionModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult DemoMyCorretion()
        {
            User user = GetUser();
            if (user != null)
            {
                DemoMyCorretionModel model = new DemoMyCorretionModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult DemoLongCorretion()
        {
            User user = GetUser();
            if (user != null)
            {
                DemoLongCorretionModel model = new DemoLongCorretionModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }
	}
}