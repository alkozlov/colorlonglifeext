﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using ColorLongLife.DataAccessLayer.Models;
using ColorLongLife.Models.Shop;

namespace ColorLongLife.Controllers
{
    public class ShopController : BaseController
    {
        private int PageSizeServices = 2;
        private int PageSizeBad = 6;

        public ShopController()
        {
            ViewBag.IsDisplayTopMenu = true;
            ViewBag.IsNotAutorized = false;
            ViewBag.Caption = "Персональный кабинет здоровья";
            HtmlHelper.ClientValidationEnabled = true;
            HtmlHelper.UnobtrusiveJavaScriptEnabled = true;
        }

        //
        // GET: /Shop/
        public ActionResult Index(int page=1)
        {
            User user = GetUser();
            if (user != null)
            {
                ProductsModel model = new ProductsModel()
                {
                    FirstName = user.FirstName,
                    Products =
                    _unitOfWork.ProductRepository.Get(p => p.ProductCategories.ProductCategoriesId == 2)
                        .Skip((page - 1) * PageSizeServices)
                        .Take(PageSizeServices),
                    PagingInfo = new PagingInfoModel
                    {
                        CurrentPage = page,
                        ItemsPerPage = PageSizeServices,
                        TotalItems = _unitOfWork.ProductRepository.Get(p => p.ProductCategoriesId == 2).Count()
                    },
                    //CurrentCategoryId = prodcategoryId
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }
        public ActionResult BADs(string prodcategory, int page=1)
        {
            User user = GetUser();
            if (user != null)
            {
                ProductsModel model = new ProductsModel()
                {
                    FirstName = user.FirstName,
                    Products =
                    _unitOfWork.ProductRepository.Get(p => p.ProductCategoriesId == 1)
                        .Skip((page - 1) * PageSizeBad)
                        .Take(PageSizeBad),
                    PagingInfo = new PagingInfoModel
                    {
                        CurrentPage = page,
                        ItemsPerPage = PageSizeBad,
                        TotalItems = _unitOfWork.ProductRepository.Get(p => p.ProductCategoriesId == 1).Count()
                    }
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }

        public ActionResult ReccomendsBADs()
        {
            User user = GetUser();
            if (user != null)
            {
                ProductsModel model = new ProductsModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }
        public ActionResult CardsProduct(String id)
        {
            int productIds = Convert.ToInt32(id);
            User user = GetUser();
            var product = _unitOfWork.ProductRepository.Get(x=>x.ProductId== productIds).FirstOrDefault();
            if (user != null)
            {
                if (product != null)
                {
                    CardsProductModel model = new CardsProductModel()
                    {
                        FirstName = user.FirstName,
                        ProductId = product.ProductId,
                        ProductName = product.ProductName,
                        Description = product.Description,
                        FullDescription = product.FullDescription,
                        PathToImage = product.PathToImage,
                        ProducerName = product.ProducerName,
                        CountriIsMade = product.CountriIsMade,
                        Price = product.Price,
                        NameUseProduct = product.UseProduct.NameUseProduct,
                        NameProductCategories = product.ProductCategories.NameProductCategories
                    };
                    ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                    return View(model);
                }
                else
                {
                    return RedirectToAction("BADs", "Shop");
                }
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }
        public ActionResult BuyBook()
        {
            User user = GetUser();
            if (user != null)
            {
                BuyBookModel model = new BuyBookModel()
                {
                    FirstName = user.FirstName
                };
                ViewBag.UserName = String.Format("{0} {1} {2}", user.LastName, user.FirstName, user.MiddleName);
                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Authorization");
            }
        }
    }
}