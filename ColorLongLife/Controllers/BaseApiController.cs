﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ColorLongLife.Controllers
{
    public class BaseApiController : ApiController
    {
        protected HttpResponseException CreateResponseException(HttpStatusCode statusCode, String message)
        {
            HttpResponseMessage response = this.Request.CreateErrorResponse(statusCode, message);
            HttpResponseException responseException = new HttpResponseException(response);

            return responseException;
        }
    }
}
