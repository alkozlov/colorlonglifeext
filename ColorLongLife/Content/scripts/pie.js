﻿// Sectors
var piePieces = new Array();
// Categories
var pieLabels = new Array();
// Step
var pieNumber = new Array();
var highlights = new Array();
var labels = new Array();
var colors = new Array();
var inputlabels = new Array();
var inputcolors = new Array();
var paper;
var papersize = 600;

var centercircle;
var centercircletxt;
var centercircleclr = '#ffffff';
var centercircletxtstr;
var inputcentercircleclr;
var inputcentercircletxtstr;

var nextsectortofill = 0;
var emptyString = "";
var centerstartstring = "";
var centertoottip = "";
var sectortoottip = new Array();

var proceedid;
var hiddenid;

var noconflictmsgholder;
var conflictmsgholder;

var listblankitemtext = 'blank';

var hashstring = '';

var msg0 = '';
var msg1 = '';
var msg2 = '';
var msg3 = '';
var msg4 = '';
var msg5 = '';
var msg6 = '';
var msg7 = '';
var msg8 = '';

var msg10 = '';
var msg11 = '';

var bgclrblockcolor;
var isChartRecreation;

Raphael.fn.pieChart = function (cx, cy, r, labels, stroke) {

    //prompt('pie chart here');


    var paper = this,
        rad = Math.PI / 180;

    function sector(cx, cy, r, startAngle, endAngle, params) {
        var x1 = cx + r * Math.cos(-startAngle * rad),
            x2 = cx + r * Math.cos(-endAngle * rad),
            y1 = cy + r * Math.sin(-startAngle * rad),
            y2 = cy + r * Math.sin(-endAngle * rad);
        return paper.path(params).moveTo(cx, cy).lineTo(x2, y2).arcTo(r, r, (endAngle - startAngle > 180 ? 1 : 0), 0, x1, y1).andClose();
    }

    var angle = 90,
        total = 0;

    // clear location.hash
    //window.location.hash = 'reset=1';

    //alert('here');

    process = function (j) {
        var value = 'value' + j,
            angleplus = 360 / labels.length,
            popangle = angle - (angleplus / 2),
            color = Raphael.getColor(.7),
            ms = 200,
            delta = 50;

        var p = sector(cx, cy, r, angle, angle - angleplus, { fill: colors[j], stroke: stroke });
        p.node.id = 'sector_' + j;

        var fontcolor = "#cccccc";
        var fontsize = "10px";
        var fontweight = "normal";

        if (labels[j] != emptyString) {
            fontsize = '12px';
            fontcolor = getFontContrastColor(colors[j]);
            fontweight = "bold";
        }

        var txt = new Array();;
        var textarr = labels[j].capitalize().split(' ');
        for (var i = 0; i < textarr.length; i++) {
            txt[i] = paper.text(cx + (r - delta) * Math.cos(-popangle * rad), -14 * (textarr.length - 1) / 2 + 14 * i + cy + (r - delta) * Math.sin(-popangle * rad), textarr[i]).attr({ fill: fontcolor, stroke: "none", "font-family": '"Arial"', "font-size": fontsize, "font-weight": fontweight });
            txt[i].node.id = 'label' + j + '_' + i;
        }
        var num = paper.text(cx + (r / 3) * Math.cos(-popangle * rad), cy + (r / 3) * Math.sin(-popangle * rad), j + 1).attr({ fill: getFontContrastColor(colors[j]), stroke: "none", "font-family": '"Arial"', "font-size": "12px" });



        piePieces[piePieces.length] = new Object(p);
        pieLabels[pieLabels.length] = new Object(txt);
        pieNumber[pieNumber.length] = new Object(num);


        //p.node.onmouseover = new Function ("evt", "sectorMouseover(evt, '" + j + "')" );
        //p.node.onmouseout  = new Function ("evt", "sectorMouseout (evt, '" + j + "')" );
        p.node.onclick = new Function("evt", "sectorClick    (evt, '" + j + "', '" + cx + "', '" + r + "', '" + delta + "', '" + rad + "', '" + cy + "', '" + popangle + "')");


        //num.node.onmouseover = new Function ("evt", "sectorMouseover(evt, '" + j + "')" );
        num.node.onclick = new Function("evt", "sectorClick    (evt, '" + j + "', '" + cx + "', '" + r + "', '" + delta + "', '" + rad + "', '" + cy + "', '" + popangle + "')");

        // attach tooltip to sector
        //			if ( ( sectortoottip[j] != null ) && ( ! isExpress() ) )
        //			{	
        //				if( sectortoottip[j] != "" )
        //				{
        //					p.node.onmouseover = function () { showtooltip(sectortoottip[j]); };
        //					p.node.onmouseout  = function () { hidetooltip(); };
        //				}
        //			}


        if ((j == nextsectortofill - 1) && (!isExpress()) && (j < piePieces.length) && (sectortoottip[j] != null) && (sectortoottip[j] != "")) {
            p.node.onmouseover = function () { showtooltip(sectortoottip[j]); };
            p.node.onmouseout = function () { hidetooltip(); };
        }


        for (var i = 0; i < txt.length; i++) {
            //txt[i].node.onmouseover = new Function ("evt", "sectorMouseover(evt, '" + j + "')" );
            txt[i].node.onclick = new Function("evt", "sectorClick    (evt, '" + j + "', '" + cx + "', '" + r + "', '" + delta + "', '" + rad + "', '" + cy + "', '" + popangle + "')");
            txt[i].node.id = "sector_text_" + j + "_" + i;

            // attach tooltip to sectors text
            //				if ( ( sectortoottip[j] != null ) && ( ! isExpress() ) )
            //				{	
            //					if( sectortoottip[j] != "" )
            //					{
            //						txt[i].node.onmouseover = function () { showtooltip(sectortoottip[j]); };
            //						txt[i].node.onmouseout  = function () { hidetooltip(); };
            //					}
            //				}
        }



        angle -= angleplus;
    };

    for (var i = 0; i < labels.length; i++) {
        process(i);
    }

    if (centercircle != null)
        centercircle.remove();
    if (centercircletxt != null)
        for (var i = 0; i < centercircletxt.length; i++)
            centercircletxt[i].remove();

    centercircle = paper.circle(cx, cy, r / 4);
    centercircle.attr({ 'fill': centercircleclr });
    centercircle.node.id = 'sector_center';
    centercircle.node.onclick = function () { fillCenterCircle(cx, cy); };
    //centercircle.node.onmouseover = new Function ("evt", "showtooltip('tooltip')" );

    // if express, attach tootip
    if (isExpress())
        //if ( centerstartstring.toLowerCase() == centercircletxtstr.toLowerCase() )
    {
        centercircle.node.onmouseover = function () { showtooltip(centertoottip); };
        centercircle.node.onmouseout = function () { hidetooltip(); };
    }
    if (centercircletxtstr != null) {
        var centertxttemparr = centercircletxtstr.capitalize().split(' ');
        centercircletxt = new Array();

        for (var i = 0; i < centertxttemparr.length; i++) {
            centercircletxt[i] = paper.text(cx, cy - 14 * centertxttemparr.length / 2 + 14 * (i + 1), centertxttemparr[i]).attr({ fill: getFontContrastColor(centercircleclr), stroke: "none", "font-family": '"Arial"', "font-size": "12px", "font-weight": "bold" });
            centercircletxt[i].node.onclick = function () { fillCenterCircle(cx, cy); };
            centercircletxt[i].node.id = 'sector_center_text' + i;

            // if express, attach tootip
            if (centerstartstring.toLowerCase() == centercircletxtstr.toLowerCase()) {
                centercircletxt[i].node.onmouseover = function () { showtooltip(centertoottip); };
                centercircletxt[i].node.onmouseout = function () { hidetooltip(); };
            }
        }
    }


    /////////////////////////////////////////////////////////////////////////
    // init hash
    /////////////////////////////////////////////////////////////////////////
    var ihash = '';
    for (var i = 0; i < labels.length; i++)
        ihash += i + 'str=' + labels[i] + '&' + i + 'clr=' + colors[i].replace('#', '').toLowerCase() + '&';
    ihash += 'centerstr=' + centercircletxtstr + '&centerclr=' + centercircleclr.replace('#', '').toLowerCase();
    window.location.hash = ihash;
    hashstring = window.location.hash;
    /////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////
};


//////////////////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////////////////
sectorMouseover = function (e, sectorid) {
    var txt = pieLabels[sectorid];
    var numx = pieNumber[sectorid];

    for (var k = 0; k < txt.length; k++)
        txt[k].attr({ "font-size": "13px", "font-weight": "bold" });
}


//////////////////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////////////////
sectorMouseout = function (e, sectorid) {
    var txt = pieLabels[sectorid];
    var numx = pieNumber[sectorid];

    if (labels[sectorid] != emptyString)
        for (var k = 0; k < txt.length; k++)
            txt[k].attr({ "font-size": "12px", "font-weight": "normal" });
    else
        for (var k = 0; k < txt.length; k++)
            txt[k].attr({ "font-size": "10px", "font-weight": "normal" });
}


//////////////////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////////////////
sectorClick = function (e, sectorid, cx, r, delta, rad, cy, popangle) {
    if ((selectedColor == null) && (selectedKey == null) && (document.getElementById('dragclear') == null))
        return;

    //alert('click');

    // 
    // 
    // 
    if ((nextsectortofill - 1 < sectorid) && (document.getElementById('dragclear') == null)) {
        var msgtxt = msg0;

        //alert(labels[nextsectortofill-1] + "\n" + colors[nextsectortofill-1]);

        // user alert messages
        // 'please complete sector #1...' 'please color sector #2...'
        if ((labels[nextsectortofill - 1] != null) && (labels[nextsectortofill - 1] == emptyString))
            msgtxt = msg1;
        if ((colors[nextsectortofill - 1] != null) && (cutHex(colors[nextsectortofill - 1]).toLowerCase() == 'ffffff'))
            msgtxt = msg2;
        if ((labels[nextsectortofill - 1] != null) && (labels[nextsectortofill - 1] == emptyString) && (colors[nextsectortofill - 1] != null) && (cutHex(colors[nextsectortofill - 1]).toLowerCase() == 'ffffff'))
            msgtxt = msg3;
        if (nextsectortofill == 0)
            msgtxt = msg4;
        else
            msgtxt += msg5 + '"' + nextsectortofill + '".';

        if ((labels[nextsectortofill - 1] != null) && (labels[nextsectortofill - 1] == emptyString))
            msgtxt += msg6;
        if ((colors[nextsectortofill - 1] != null) && (cutHex(colors[nextsectortofill - 1]).toLowerCase() == 'ffffff'))
            msgtxt += msg7;


        //if ((labels[sectorid] != emptyString) && (cutHex(colors[sectorid]).toLowerCase() != 'ffffff') && ( nextsectortofill-1 == sectorid )) 

        alert(msgtxt);

        selectedColor = null;
        selectedKey = null;
        return;
    }


    var txt = pieLabels[sectorid];
    var numx = pieNumber[sectorid];
    var pppx = piePieces[sectorid];
    var previousselectedlistitemtxt;

    // color case
    if (selectedColor != null) {
        var rgbhsvclr1 = new RGBHSV(false);
        var rgbhsvclr2 = new RGBHSV(false);

        rgbhsvclr1.setRGB(HexToR(selectedColor), HexToG(selectedColor), HexToB(selectedColor));

        rgbhsvclr2.setRGB(HexToR(selectedColor), HexToG(selectedColor), HexToB(selectedColor));
        rgbhsvclr2.setHSV(rgbhsvclr2.H + 180, rgbhsvclr2.S * 100, rgbhsvclr2.V * 100);


        //alert(selectedColor + "\n" + 
        //	  rgbhsvclr1.value() + " - " + rgbhsvclr1.H + ", " + rgbhsvclr1.S*100 + ", " + rgbhsvclr1.V*100  + "\n" + 
        //	  rgbhsvclr2.value() + " - " + rgbhsvclr2.H + ", " + rgbhsvclr2.S*100 + ", " + rgbhsvclr2.V*100 );

        // if color already used
        for (var i = 0; i < colors.length; i++) {
            if ((selectedColor == colors[i]) || (selectedColor == centercircleclr)) {
                alert(msg8);
                return;
            }
        }

        // set color			
        pppx.attr({ fill: selectedColor });
        colors[sectorid] = selectedColor;
        pieNumber[sectorid].attr({ 'fill': getFontContrastColor(selectedColor) });

        // set hash
        setHashValue(sectorid + "clr", selectedColor.replace('#', '').toLowerCase());

        for (var k = 0; k < txt.length; k++) {
            if ((labels[sectorid] != emptyString) || (selectedKey != null))
                txt[k].attr({ 'fill': getFontContrastColor(selectedColor) });
        }
    }

    // text case
    if (selectedKey != null) {
        for (var k = 0; k < txt.length; k++) {
            txt[k].remove();
        }
        txt = new Array();

        var tmp = selectedKey.capitalize().split(' ');
        for (var k = 0; k < tmp.length; k++) {
            try {
                //txt[k] = paper.text(1*cx + (1*r - 1*delta) * Math.cos(-popangle * rad), -14*(tmp.length-1)/2 + 14*k + 1*cy + (r - delta) * Math.sin(-popangle * rad), tmp[k]).attr({fill: "#CCCCCC", stroke: "none", "font-family": '"Arial"',"font-size": "14px", "font-weight": "bold"});
                txt[k] = paper.text(1 * cx + (1 * r - 1 * delta) * Math.cos(-popangle * rad), -14 * (tmp.length - 1) / 2 + 14 * k + 1 * cy + (r - delta) * Math.sin(-popangle * rad), tmp[k]).attr({ fill: "#CCCCCC", stroke: "none", "font-family": '"Arial"', "font-size": "12px", "font-weight": "bold" });

                txt[k].attr({ "fill": getFontContrastColor(colors[sectorid]) });
                //txt[k].node.onmouseover = new Function ("evt", "sectorMouseover(evt, '" + sectorid + "')" );
                txt[k].node.onclick = new Function("evt", "sectorClick    (evt, '" + sectorid + "', '" + cx + "', '" + r + "', '" + delta + "', '" + rad + "', '" + cy + "', '" + popangle + "')");
                txt[k].node.id = "sector_text_" + sectorid + "_" + k;

                // attach tooltip to sectors text
                //					if ( ( sectortoottip[sectorid] != null ) && ( ! isExpress() ) )
                //					{
                //						if( sectortoottip[sectorid] != "" )
                //						{
                //							txt[k].node.onmouseover = function () { showtooltip(sectortoottip[sectorid]); };
                //							txt[k].node.onmouseout  = function () { hidetooltip(); };
                //						}
                //					}

            }
            catch (err) {
                alert(err);
            }
        }

        previousselectedlistitemtxt = labels[sectorid];
        labels[sectorid] = selectedKey;
        pieLabels[sectorid] = txt;

        setHashValue(sectorid + "str", selectedKey);


        //if ( document.getElementById('orderedfilling').checked )
        //	nextsectortofill++;
    }

    // if sector completed
    if ((labels[sectorid] != emptyString) && (cutHex(colors[sectorid]).toLowerCase() != 'ffffff') && (nextsectortofill - 1 == sectorid)) {
        try { hidetooltip(); } catch (err) { }
        nextsectortofill++;

        // if not express diagnostic, attach tooltips
        // to next sectors to be filled, one by one
        if (!isExpress()) {
            // delete tooltip
            piePieces[sectorid].node.onmouseover = null;
            piePieces[sectorid].node.onmouseout = null;

            // attach tooltip
            if ((!isExpress()) && (sectorid * 1 + 1 < piePieces.length) && (sectortoottip[sectorid * 1 + 1] != null) && (sectortoottip[sectorid * 1 + 1] != "")) {
                piePieces[sectorid * 1 + 1].node.onmouseover = function () { showtooltip(sectortoottip[sectorid * 1 + 1]); };
                piePieces[sectorid * 1 + 1].node.onmouseout = function () { hidetooltip(); };
            }

        }
    }
    manageList(labels[sectorid], colors[sectorid], previousselectedlistitemtxt);
    selectedColor = null;
    selectedKey = null;
    mouseReleaseTool();

    if (document.getElementById('dragclr') != null)
        document.body.removeChild(document.getElementById('dragclr'));
    if (document.getElementById('dragkey') != null)
        document.body.removeChild(document.getElementById('dragkey'));
    if (document.getElementById('dragclear') != null) {
        // to clear sectors text
        if (document.getElementById('dragclear').getAttribute('mode') == 'text') {
            // release list item, unselect
            manageList(null, null, labels[sectorid]);
            for (var k = 0; k < txt.length; k++)
                txt[k].remove();
            txt = new Array();
            labels[sectorid] = emptyString;
            txt[0] = paper.text(1 * cx + (r - delta) * Math.cos(-popangle * rad), 1 * cy + (r - delta) * Math.sin(-popangle * rad), labels[sectorid].capitalize()).attr({ fill: '#CCCCCC', stroke: "none", "font-family": '"Arial"', "font-size": '10px', "font-weight": 'bold' });
            //txt[0].node.onmouseover = new Function ("evt", "sectorMouseover(evt, '" + sectorid + "')" );
            txt[0].node.onclick = new Function("evt", "sectorClick    (evt, '" + sectorid + "', '" + cx + "', '" + r + "', '" + delta + "', '" + rad + "', '" + cy + "', '" + popangle + "')");
            pieLabels[sectorid] = txt;

            setHashValue(sectorid + "str", emptyString);
        }

        // to clear sectors color
        if (document.getElementById('dragclear').getAttribute('mode') == 'color') {
            // uncolor list item
            manageList(labels[sectorid], "#000000", labels[sectorid]);

            for (var k = 0; k < txt.length; k++) {
                if (labels[sectorid] == emptyString)
                    txt[k].attr({ 'fill': '#CCCCCC' });
                else
                    txt[k].attr({ 'fill': '#000000' });

            }
            pieNumber[sectorid].attr({ 'fill': '#000000' });
            pppx.attr({ fill: '#FFFFFF' });
            colors[sectorid] = '#FFFFFF';

            setHashValue(sectorid + "clr", 'ffffff');
        }

        document.body.removeChild(document.getElementById('dragclear'));
    }


    resolveProceedButton();

}

//////////////////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////////////////
resolveProceedButton = function () {
    //alert('gere');
    if (isChartCompleted()) {
        // if chart completed and is Express - enable 'Proceed' link
        if (isExpress()) {
            enableProceedLinkButton();
            document.getElementById(proceedid).title = '';
        }
            // if chart is not Express and all the sectors are completed - show the 'Background color' block 
        else {
            document.getElementById('bgclrblock').style.display = 'block';
            document.getElementById(proceedid).title = msg11;
        }
    }
    else {	//document.getElementById('proceed').disabled = true;
        disableProceedLinkButton();
        document.getElementById(proceedid).title = '';
        document.getElementById('bgclrblock').style.display = 'none';
    }
}


//////////////////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////////////////
isChartCompleted = function () {
    for (var k = 0; k < labels.length; k++) {
        if (labels[k].toLowerCase() == emptyString)
            return false;
        if (cutHex(colors[k]).toLowerCase() == 'ffffff')
            return false;
        if (cutHex(centercircleclr).toLowerCase() == 'ffffff')
            return false;
    }
    return true;
}



//////////////////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////////////////
isExpress = function () {
    //alert(centerstartstring.toLowerCase() + "\n" + centercircletxtstr.toLowerCase());

    if (centerstartstring.toLowerCase() == centercircletxtstr.toLowerCase())
        return true;
    return false;
}




//////////////////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////////////////
displayFullScreenMsgStarter = function () {
    try {
        // scroll down the page when loaded
        //scroll(0, getDocHeight());
        window.scroll(0, 180);

        if (readCookie('showonstart') != null) {
            // if cookie "showonstart" is true, show BSMessage 
            if (readCookie('showonstart') == 'true') {
                displayFullScreenMsg(true);
                document.getElementById('showonstart').checked = false;
            }
        }
            // if no cookie found show message
        else {
            displayFullScreenMsg(true);
        }
    }
    catch (err) { alert(err); }
}


//////////////////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////////////////
displayFullScreenMsg = function (isonstart, msgtxtid) {
    // if no msgtxtid specified
    if (msgtxtid == null)
        msgtxtid = 'messagetext';

    if (document.getElementById(msgtxtid) != null) {
        var msgscreen = document.getElementById('blackscreen');
        var dodisplayblackscreen = true;
        if (msgscreen.style.display == 'block')
            dodisplayblackscreen = false;
        msgscreen.style.cursor = 'default';
        msgscreen.style.display = 'block';
        msgscreen.style.opacity = 5 / 10;
        msgscreen.style.filter = 'alpha(opacity:' + 5 * 10 + ')'; // ie/Win

        var msgtext = document.getElementById('blackscreenmsg');

        msgtext.style.left = ((document.body.clientWidth - 780) / 2) + 'px';

        //if (msgtext.innerHTML == '')
        {
            msgtext.innerHTML = document.getElementById(msgtxtid).innerHTML;
            //document.getElementById(msgtxtid).innerHTML = '';
        }

        var instrdiv = document.getElementById('instructions');

        var otop = instrdiv.offsetTop;
        var oleft = instrdiv.offsetLeft;
        var parentel = instrdiv.offsetParent;

        while (parentel) {
            otop += parentel.offsetTop;
            oleft += parentel.offsetLeft;
            parentel = parentel.offsetParent;
        }

        var steps = 30;
        var toptoadd = otop / steps;
        var lefttoadd = oleft / steps;
        var widthtoadd = (instrdiv.offsetWidth - document.body.clientWidth) / steps;
        var heighttoadd = (instrdiv.offsetHeight - getDocHeight()) / steps;

        //alert(widthtoadd);
        if (dodisplayblackscreen) {
            for (var i = steps; i >= 0; i--) {
                setTimeout("shrinkBlackScreen(" + toptoadd * i + ", " + lefttoadd * i + ", " +
                                                (document.body.clientWidth + widthtoadd * i) * 1 + ", " +
                                                (getDocHeight() + heighttoadd * i) * 1 + ")",
                                                20 * (steps - i + 1));
            }
        }
        //alert(f_clientHeight());
        //setTimeout("new function(){ var elembsm = document.getElementById('blackscreenmsg');elembsm.style.display = 'block'; elembsm.style.top = (getDocHeight() - f_clientHeight()/2 - elembsm.offsetHeight/2 - f_scrollTop()) + 'px'; }", 20*(steps+2));
        if (dodisplayblackscreen) {
            setTimeout("new function(){ var elembsm = document.getElementById('blackscreenmsg');elembsm.style.display = 'block'; elembsm.style.top = ( f_clientHeight()/2 - elembsm.offsetHeight/2 + f_scrollTop()) + 'px'; }", 20 * (steps + 2));
            msgscreen.onclick = new Function("evt", null);
        }
        //			msgscreen.innerHTML = '';
        //			scroll(0, getDocHeight());
        //			
        //			if ( isonstart )
        //			{
        //				if (readCookie('showonstart') == 'false') 
        //				{
        //					setTimeout("new function(){hideFullScreenMsg();}", 20 * (steps + 3));
        //					document.getElementById('showonstart').checked = true;
        //				}
        //				else
        //					document.getElementById('showonstart').checked = false;
        //			}
    }
}

//////////////////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////////////////
hideFullScreenMsg = function () {
    try {
        var instrdiv = document.getElementById('instructions');
        var donotshowonstart = document.getElementById('showonstart');

        //alert("checkbox - " + showonstart.checked);

        if (donotshowonstart.checked)
            writeCookie("showonstart", "false", 120);
        else
            writeCookie("showonstart", "true", 120);


        //writeCookie("showonstart", "true", 1);

        //alert($('#blackscreenmsg'));
        //$('#blackscreenmsg').fadeOut('slow');
        document.getElementById('blackscreenmsg').style.display = 'none';

        var otop = instrdiv.offsetTop;
        var oleft = instrdiv.offsetLeft;
        var parentel = instrdiv.offsetParent;

        while (parentel) {
            otop += parentel.offsetTop;
            oleft += parentel.offsetLeft;
            parentel = parentel.offsetParent;
        }

        var steps = 30;
        var toptoadd = otop / steps;
        var lefttoadd = oleft / steps;
        var widthtoadd = (instrdiv.offsetWidth - document.getElementById('blackscreen').offsetWidth) / steps;
        var heighttoadd = (instrdiv.offsetHeight - document.getElementById('blackscreen').offsetHeight) / steps;

        for (var i = 1; i < steps + 1; i++) {
            var locw = (document.getElementById('blackscreen').offsetWidth + widthtoadd * i) * 1;
            var loch = (document.getElementById('blackscreen').offsetHeight + heighttoadd * i) * 1;
            setTimeout("shrinkBlackScreen(" + toptoadd * i + ", " + lefttoadd * i + ", " +
                                            locw + ", " +
                                            loch + ")",
                                            20 * (i + 1));
        }

        //document.getElementById('blackscreen').onclick = new Function ("evt", "displayFullScreenMsg(false);" );	
        document.getElementById('blackscreen').onclick = function () { displayFullScreenMsg(false); hidetooltip(); };
        document.getElementById('blackscreen').style.cursor = 'pointer';

        //setTimeout("new function(){document.getElementById('instructionbutton').style.display = 'block';}", 20*(steps+2));
        //setTimeout("new function(){document.getElementById('blackscreen').style.opacity = 10/10;document.getElementById('blackscreen').style.filter  = 'alpha(opacity:' + 10 * 10 + ')';}", 20*(steps+2));
        setTimeout("new function(){document.getElementById('blackscreen').style.display = 'none';}", 20 * (steps + 2));

        //msgscreen.style.opacity = 5/10;
        //msgscreen.style.filter  = 'alpha(opacity:' + 5 * 10 + ')'; // ie/Win
        //alert(document.body.scrollTop);			
    }
    catch (err) {
        alert(err);
    }
}


//////////////////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////////////////
shrinkBlackScreen = function (otop, oleft, owidth, oheight) {
    try {
        otop -= otop % 1;
        oleft -= oleft % 1;
        owidth -= owidth % 1;
        oheight -= oheight % 1;

        var blScreen = document.getElementById('blackscreen');
        blScreen.style.top = otop + "px";
        blScreen.style.left = oleft + "px";
        blScreen.style.width = owidth + "px";
        blScreen.style.height = oheight + "px";
    }
    catch (err) {
        alert(err);
    }
}



//////////////////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////////////////
getDocHeight = function () {
    var D = document;
    return Math.max
    (
        Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
        Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
        Math.max(D.body.clientHeight, D.documentElement.clientHeight)
    );
}


function f_clientHeight() {
    return f_filterResults(
        window.innerHeight ? window.innerHeight : 0,
        document.documentElement ? document.documentElement.clientHeight : 0,
        document.body ? document.body.clientHeight : 0
    );
}
function f_scrollTop() {
    return f_filterResults(
        window.pageYOffset ? window.pageYOffset : 0,
        document.documentElement ? document.documentElement.scrollTop : 0,
        document.body ? document.body.scrollTop : 0
    );
}
function f_filterResults(n_win, n_docel, n_body) {
    var n_result = n_win ? n_win : 0;
    if (n_docel && (!n_result || (n_result > n_docel)))
        n_result = n_docel;
    return n_body && (!n_result || (n_result > n_body)) ? n_body : n_result;
}


////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////
function writeCookie(name, value, hours) {
    var expire = "";
    if (hours != null) {
        expire = new Date((new Date()).getTime() + hours * 3600000);
        expire = "; expires=" + expire.toGMTString();
    }
    document.cookie = name + "=" + escape(value) + expire + "; path=/";

    //alert('cook - ' + escape(value));
}

////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////
function readCookie(name) {
    var cookieValue = "";
    var search = name + "=";
    if (document.cookie.length > 0) {
        offset = document.cookie.indexOf(search);
        if (offset != -1) {
            offset += search.length;
            end = document.cookie.indexOf(";", offset);
            if (end == -1)
                end = document.cookie.length;
            cookieValue = unescape(document.cookie.substring(offset, end))
        }
    }
    return cookieValue;
}





////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////
initPage = function () {
    //alert('onot page');
    try {
        document.getElementById('holder').innerHTML = '';
        paper = Raphael("holder", papersize, papersize);
        //prompt('asd');
    }
    catch (err) {
        //var myStackTrace = err.stack || err.stacktrace || "";
        //prompt('[PIE] ' + err);
        //printStackTrace();

        // error from IE8, [VML]
        try {
            document.getElementById('holder').innerHTML = document.getElementById('inputstringie8msg').innerHTML;
        }
        catch (err) { }
        //document.getElementById('notsupported').style.display = 'block';


        disableProceedLinkButton();
    }

    initStrings();
    initChart();
    displayFullScreenMsgStarter();
    initBgColorBlock(document.getElementById('holder'));

    try {
        initcolortablefontcolor();
        //sortList();
    }
    catch (err) { }
}
//);

////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////
window.onload = function ()
    //$(document).ready(function() 
    //$(window).load(function ()
{
    initPage();
}

//setTimeout("window.onload()", 5000);
//alert('asdasd');


////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////
initBgColorBlock = function (holder) {
    // try to remove if already exists, for 'reset'
    try {
        if ((document.getElementById('bgclrblock') != null) && (isChartRecreation == false)) {
            holder.removeChild(document.getElementById('bgclrblock'));
            bgclrblockcolor = null;
            //alert('');
        }
    }

    catch (err) {
        //alert(err);
    }

    var bgclrblock = document.createElement('div');
    bgclrblock.id = 'bgclrblock';
    bgclrblock.style.position = 'absolute';
    bgclrblock.style.bottom = '30px';
    bgclrblock.style.left = '30px';
    bgclrblock.style.width = '80px';
    bgclrblock.style.height = '80px';
    bgclrblock.style.border = 'solid 2px #000';
    if (isChartRecreation == false)
        bgclrblock.style.background = 'url(../images/bgclrblockbg.gif) #FFF';

    // if 'bgclrblockcolor' is not null -> it is chart recreation
    if (bgclrblockcolor != null) {
        bgclrblock.style.display = 'block';
        bgclrblock.style.backgroundColor = bgclrblockcolor;
    }
    else {
        bgclrblock.style.display = 'none';
        //bgclrblock.onclick = function () { colorBgColorBlock(); };
        bgclrblock.onclick = new Function("evt", "colorBgColorBlock()");
        bgclrblock.onmouseover = function () { showtooltip(msg10); };
        bgclrblock.onmouseout = function () { hidetooltip(); };
    }
    holder.appendChild(bgclrblock);
}


////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////
colorBgColorBlock = function () {
    var bl = document.getElementById('bgclrblock');

    // color 
    if (selectedColor != null) {
        // set color
        bl.style.background = 'none';
        bl.style.backgroundColor = selectedColor;
        bgclrblockcolor = selectedColor;
        selectedColor = null;

        // 
        if (isChartCompleted()) {
            //resolveProceedButton();
            enableProceedLinkButton();
            document.getElementById(proceedid).title = '';
        }

        // set hash
        // setHashValue(sectorid + "clr", selectedColor.replace('#', '').toLowerCase());
    }

}

////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////
initStrings = function () {
    try { emptyString = document.getElementById('inputstringemptystring').innerHTML; } catch (err) { }
    try { centerstartstring = document.getElementById('inputstringcenterstartstring').innerHTML; } catch (err) { }
    try { centertoottip = document.getElementById('inputstringcentertoottip').innerHTML; } catch (err) { }

    try { msg0 = document.getElementById('inputstringmsg0').innerHTML; } catch (err) { }
    try { msg1 = document.getElementById('inputstringmsg1').innerHTML; } catch (err) { }
    try { msg2 = document.getElementById('inputstringmsg2').innerHTML; } catch (err) { }
    try { msg3 = document.getElementById('inputstringmsg3').innerHTML; } catch (err) { }
    try { msg4 = document.getElementById('inputstringmsg4').innerHTML; } catch (err) { }
    try { msg5 = document.getElementById('inputstringmsg5').innerHTML; } catch (err) { }
    try { msg6 = document.getElementById('inputstringmsg6').innerHTML; } catch (err) { }
    try { msg7 = document.getElementById('inputstringmsg7').innerHTML; } catch (err) { }
    try { msg8 = document.getElementById('inputstringmsg8').innerHTML; } catch (err) { }
    try { msg10 = document.getElementById('inputstringmsg10').innerHTML; } catch (err) { }
    try { msg11 = document.getElementById('inputstringmsg11').innerHTML; } catch (err) { }

    try {
        for (var i = 0; i < 16; i++) {
            sectortoottip[sectortoottip.length] = document.getElementById('inputstringsectortooltip_' + i).innerHTML;
        }
    }
    catch (err) { }
}

////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////
initChart = function () {
    //var isChartRecreation = true;
    isChartRecreation = true;

    labels = new Array();
    colors = new Array();

    //alert(centercircletxtstr);

    if ((centercircletxtstr == null) || (centercircletxtstr.length == 0))
        centercircletxtstr = centerstartstring.capitalize();


    // get query string if exists
    //if ( getQueryString('startkey') != null )
    //{
    //	var qstr = getQueryString('startkey');

    //qstr = unescape(qstr.replace(/\+/g, " "));
    //qstr = decode( getQueryString('startkey') );

    //	alert( unescape(qstr) );
    //	centercircletxtstr = replaceAll( qstr , '_', ' ');
    //}

    // init labels and colors for the first display
    for (var i = 0; i < 8; i++) {
        labels[labels.length] = emptyString;
        colors[colors.length] = '#ffffff';
    }

    // if given labels and colors (recreation of the chart)
    if (((inputlabels != null) && (inputlabels.length > 0)) &&
         ((inputcolors != null) && (inputcolors.length > 0))) {
        labels = new Object(inputlabels);
        colors = new Object(inputcolors);
    }
    else
        isChartRecreation = false;

    // if given center color and text (recreation of the chart)
    if (((inputcentercircletxtstr != null) && (inputcentercircletxtstr.length > 0)) &&
         ((inputcentercircleclr != null) && (inputcentercircleclr.length > 0))) {
        centercircletxtstr = new Object(inputcentercircletxtstr);
        centercircleclr = new Object(inputcentercircleclr);
    }
    else
        isChartRecreation = false;

    //if ( isChartRecreation )
    //	doAnalize();


    for (var i = 0; i < piePieces.length; i++)
        piePieces[i].remove();
    for (var i = 0; i < pieLabels.length; i++)
        for (var j = 0; j < pieLabels[i].length; j++)
            pieLabels[i][j].remove();
    for (var i = 0; i < pieNumber.length; i++)
        pieNumber[i].remove();
    for (var i = 0; i < highlights.length; i++)
        highlights[i].remove();

    if (centercircle != null) {
        centercircle.remove();
        centercircle = null;
        centercircleclr = '#ffffff';
    }
    /*
    // do not remove
    //
    if (centercircletxt != null) 
    {
        //centercircletxt.remove();
        centercircletxtstr = null;
    }*/

    piePieces = new Array();
    pieLabels = new Array();
    pieNumber = new Array();
    highlights = new Array();
    nextsectortofill = 0;

    paper.pieChart(papersize / 2, papersize / 2, papersize / 2.4, labels, "#000000");

    // if recreation, analize it and highlight conflicting sectors
    // but only if express
    //alert('express? - ' + isExpress());

    if ((isChartRecreation) && (isExpress())) {
        //alert("here");
        doAnalize();
    }
    // only for diagnose page
    try {
        document.getElementById('addsector').disabled = false;
        document.getElementById('remsector').disabled = false;

        initKeyList();
        disableProceedLinkButton();
    }
    catch (err) { }

    // hide background color block
    try {
        initBgColorBlock(document.getElementById('holder'));
        //document.getElementById('bgclrblock').style.display = 'none';
        //document.getElementById('bgclrblock').style.bac = 'none';
    }
    catch (err) { }

    // in case the browsers keep input value when user press "back" button
    try {
        document.getElementById(hiddenid).value = '';
    }
    catch (err) { }
}





////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////
initKeyList = function () {
    var keylistitems = document.getElementById('keylist').getElementsByTagName('li');
    var countblank = 0;
    for (var i = 0; i < keylistitems.length; i++) {
        var vl = trim(keylistitems[i].innerHTML.capitalize());

        if ((vl == '') || (vl == '&nbsp;') || (vl.indexOf(listblankitemtext) != -1)) {
            vl = listblankitemtext + countblank;
            keylistitems[i].innerHTML = '&nbsp;';
            countblank++;
        }
        keylistitems[i].id = 'listitem' + makeValidKeyListId(vl);


        if (vl.indexOf(listblankitemtext) == -1)
            keylistitems[i].onclick = new Function("evt", "selectKey(evt, '" + vl + "')");
        keylistitems[i].ondblclick = new Function("evt", "editKey(evt, '" + vl + "')");

        if (keylistitems[i].className == 'selecteditem') {
            keylistitems[i].className = '';
            keylistitems[i].style.color = '#000000';
        }
        keylistitems[i].innerHTML = keylistitems[i].innerHTML.toLowerCase().capitalize();
    }
}




////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////
fillCenterCircle = function (cx, cy) {
    //alert('here center circle');

    var prevtxtlistitem;

    if ((selectedKey != null) && (centercircletxtstr.toLowerCase() == centerstartstring.toLowerCase())) {
        if (document.getElementById('dragkey') != null)
            document.body.removeChild(document.getElementById('dragkey'));
        selectedKey = null;
    }

    if ((selectedColor != null) || (selectedKey != null) || (document.getElementById('dragclear') != null)) {
        if (selectedColor != null) {
            centercircle.attr({ 'fill': selectedColor });
            centercircleclr = selectedColor;
            try {
                for (var i = 0; i < centercircletxt.length; i++)
                    centercircletxt[i].attr({ 'fill': getFontContrastColor(centercircleclr) });
            }
            catch (err) { }

            // set hash
            setHashValue("centerclr", selectedColor.replace('#', '').toLowerCase());

        }

        if (selectedKey != null) {
            try {
                for (var i = 0; i < centercircletxt.length; i++)
                    centercircletxt[i].remove();
            }
            catch (err) { }

            centercircletxt = new Array();
            var keyarr = selectedKey.capitalize().split(' ');
            for (var i = 0; i < keyarr.length; i++) {
                centercircletxt[i] = paper.text(cx, cy - 14 * keyarr.length / 2 + 14 * (i + 1), keyarr[i]).attr({ fill: getFontContrastColor(centercircleclr), stroke: "none", "font-family": '"Arial"', "font-size": "12px", "font-weight": "bold" });
                centercircletxt[i].node.onclick = function () { fillCenterCircle(cx, cy); };
            }
            prevtxtlistitem = centercircletxtstr;
            centercircletxtstr = selectedKey;

            //set hash
            setHashValue("centerstr", selectedKey);

        }

        manageList(centercircletxtstr, centercircleclr, prevtxtlistitem);

        // if center sector completed
        if ((cutHex(centercircleclr).toLowerCase() != 'ffffff') && (nextsectortofill == 0)) {
            nextsectortofill++;

            // attach tooltip
            if ((!isExpress()) && (sectortoottip[0] != null) && (sectortoottip[0] != "")) {
                piePieces[0].node.onmouseover = function () { showtooltip(sectortoottip[0]); };
                piePieces[0].node.onmouseout = function () { hidetooltip(); };
            }
        }

        selectedColor = null;
        selectedKey = null;
        mouseReleaseTool();

        if (document.getElementById('dragclr') != null)
            document.body.removeChild(document.getElementById('dragclr'));

        if (document.getElementById('dragkey') != null)
            document.body.removeChild(document.getElementById('dragkey'));

        if (document.getElementById('dragclear') != null) {
            //manageList(null, null, centercircletxtstr);

            /*
            // do not remove
            //
            for (var k = 0; k < centercircletxt.length; k++)
                centercircletxt[k].remove();
            centercircletxt = null;
            centercircletxtstr = null;
            */

            if (document.getElementById('dragclear').getAttribute('mode') == 'color') {
                for (var k = 0; k < centercircletxt.length; k++)
                    centercircletxt[k].attr({ 'fill': '#000000' });

                centercircle.attr({ fill: '#FFFFFF' });
                centercircleclr = '#FFFFFF';

                //set hash
                setHashValue("centerclr", 'ffffff');
            }
            document.body.removeChild(document.getElementById('dragclear'));
        }

        resolveProceedButton();
    }
}



////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////
manageList = function (text, clr, prevtxt) {
    try {
        //var prevlistitm = document.getElementById('listitem' + prevtxt.replace(' ', '_'));
        var prevlistitm = document.getElementById('listitem' + makeValidKeyListId(prevtxt));

        //replaceAll(msg, ' ', '_')

        var vl = trim(prevlistitm.innerHTML.toLowerCase());
        prevlistitm.onclick = new Function("evt", "selectKey(evt, \"" + vl + "\")");
        prevlistitm.className = '';
        prevlistitm.style.color = '#000000';
    }
    catch (err) {
        //alert(err); 
    }

    try {
        //var listitm = document.getElementById('listitem' + text.replace(' ', '_'));
        var listitm = document.getElementById('listitem' + makeValidKeyListId(text));

        listitm.className = "selecteditem";
        listitm.style.color = clr;
        listitm.onclick = null;
    }
    catch (err) {
        //alert(err); 
    }
}


////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////
//var doClearSector = false;
clearSelectors = function (e, arg) {
    selectedColor = null;
    selectedKey = null;

    mouseReleaseTool();
    if (document.getElementById('dragclr') != null) {
        document.body.removeChild(document.getElementById('dragclr'));
        return;
    }
    if (document.getElementById('dragkey') != null) {
        document.body.removeChild(document.getElementById('dragkey'));
        return;
    }

    mouseDownToolClear(e, arg);
}


////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////
addSector = function () {
    if (labels.length < 16) {
        // remove all existing pieces
        for (var i = 0; i < piePieces.length; i++) {
            piePieces[i].remove();
            for (var j = 0; j < pieLabels[i].length; j++)
                pieLabels[i][j].remove();

        }
        piePieces = new Array();
        pieLabels = new Array();
        pieNumber = new Array();
        labels[labels.length] = emptyString;
        colors[colors.length] = '#ffffff';

        paper.pieChart(papersize / 2, papersize / 2, papersize / 2.4, labels, "#000000");
    }
    if (labels.length == 16) {
        document.getElementById('addsector').disabled = true;
    }
    if (labels.length > 4) {
        document.getElementById('remsector').disabled = false;
    }

    resolveProceedButton();
}
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////
removeSector = function () {
    // 4 - min sectors
    if (labels.length > 4) {
        // remove all existing pieces
        for (var i = 0; i < piePieces.length; i++) {
            piePieces[i].remove();
            for (var j = 0; j < pieLabels[i].length; j++)
                pieLabels[i][j].remove();
        }
        piePieces = new Array();
        pieLabels = new Array();
        pieNumber = new Array();

        // release list item if needed
        if (labels[labels.length - 1] != emptyString)
            manageList(null, null, labels[labels.length - 1]);

        labels.pop();
        colors.pop();

        paper.pieChart(papersize / 2, papersize / 2, papersize / 2.4, labels, "#000000");


        if (labels.length == 4) {
            document.getElementById('remsector').disabled = true;
        }
        if (labels.length < 16) {
            document.getElementById('addsector').disabled = false;
        }
    }

    resolveProceedButton();
}
////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////
addKey = function () {
    var list = document.getElementById('keylist');
    var liarr = list.getElementsByTagName('li');
    var newkey = document.createElement('li');
    var source = document.getElementById('keysource');
    varsval = trim(source.value).replace('  ', ' ');
    varsval = replaceAll(varsval, '"', "'");

    //alert(varsval);		
    //newkey.innerHTML = varsval.capitalize();
    newkey.innerHTML = varsval;
    newkey.id = 'listitem' + makeValidKeyListId(varsval);
    newkey.onclick = new Function("evt", "selectKey(evt, \"" + varsval + "\")");
    newkey.ondblclick = new Function("evt", "editKey(evt, \"" + varsval + "\")");


    if (source.value.length > 0) {
        for (var i = 0; i < liarr.length; i++)
            if (liarr[i].innerHTML.toLowerCase() == varsval.toLowerCase())
                return;

        //			var temp = list.innerHTML;
        //			list.innerHTML = '';
        //	        list.appendChild(newkey);
        //			list.innerHTML = list.innerHTML + temp;
        //			initKeyList();

        source.value = '';
        list.insertBefore(newkey, list.firstChild);
    }
}
////////////////////////////////////////////////////////////////////////





////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////
sortList = function () {
    var list = document.getElementById('keylist');
    var items = list.getElementsByTagName('li');

    for (var j = 0; j < items.length - 1; j++)
        for (var i = 0; i < items.length - 1; i++)
            if (items[i].innerHTML > items[i + 1].innerHTML)
                list.insertBefore(items[i + 1], items[i]);
}



////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////
var selectedKey;
selectKey = function (e, msg) {
    selectedKey = msg;
    mouseDownToolKey(e, msg);
}
////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////
makeValidKeyListId = function (msg) {
    msg = replaceAll(msg, '/', '_');
    msg = replaceAll(msg, '\\', '_');

    return replaceAll(replaceAll(replaceAll(msg, '"', '\''), ' ', '_'), "'", "_").toLowerCase();
}
////////////////////////////////////////////////////////////////////////




////////////////////////////////////////////////////////////////////////
// on double click
////////////////////////////////////////////////////////////////////////
var tokeeplistmouseover;
editKey = function (e, msg) {
    //alert(msg);

    try { document.body.removeChild(document.getElementById('dragkey')); } catch (err) { }
    try { document.body.removeChild(document.getElementById('tooltip')); } catch (err) { }

    msg = replaceAll(msg, '"', "'");

    var listitemid = 'listitem' + makeValidKeyListId(msg);
    //alert(listitemid);
    var listitem = document.getElementById(listitemid);

    tokeeplistmouseover = listitem.onmouseover;
    listitem.onmouseover = null;

    if (listitem.className == 'selecteditem')
        return;

    //listitem.innerHTML = '<input onkeypress="userKeyPressed(event, \'' + msg + '\')" id="listiteminput' + replaceAll(msg, ' ', '_') + '" type="text" value="' + msg.capitalize() + '"/>';		
    var inp = document.createElement("input");
    inp.type = "text";
    inp.value = replaceAll(msg, '"', "'");
    inp.id = "listiteminput" + makeValidKeyListId(msg);
    inp.onkeypress = new Function("evt", "userKeyPressed(evt, \"" + msg + "\") ");

    //listitem.innerHTML = '<input onkeypress="userKeyPressed(event, \"' + msg + '\")" id="listiteminput' + makeValidKeyListId(msg) + '" type="text" value="' + replaceAll(msg, '"', "'") + '"/>';
    listitem.innerHTML = "";
    listitem.appendChild(inp);
    listitem.onclick = null;

    var listiteminput = document.getElementById('listiteminput' + makeValidKeyListId(msg));
    listiteminput.focus();
    listiteminput.select();

    document.onclick = new Function("evt", "editNewKey(evt, \"" + msg + "\")");
}
////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////
editNewKey = function (ev, msg) {
    var origEl;
    ev = ev || window.event;
    try {
        origEl = ev.target || ev.srcElement;
    }
    catch (err) { }

    //alert(origEl.id);

    var liel = document.getElementById('listitem' + makeValidKeyListId(msg));
    var liin = document.getElementById('listiteminput' + makeValidKeyListId(msg));
    var newKey = replaceAll(liin.value, '"', "'");

    //alert(liin.id + "\n" + origEl.id);
    //alert(ev.type);

    if ((origEl != null) && (liin.id == origEl.id) && (ev.type == 'click'))
        return;

    liel.innerHTML = newKey;
    liel.id = 'listitem' + makeValidKeyListId(newKey);

    if (newKey.indexOf(listblankitemtext) == -1) {
        liel.onclick = new Function("evt", "selectKey(evt, \"" + newKey + "\")");
        liel.ondblclick = new Function("evt", "editKey(  evt, \"" + newKey + "\")");

        //alert(liel.className);

        if (liel.className == 'blank')
            liel.className = '';
    }

    liel.onmouseover = tokeeplistmouseover;
    document.onclick = null;
}



////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////
userKeyPressed = function (e, msg) {
    var keynum;
    var keychar;
    var numcheck;

    if (window.event) // IE
    {
        keynum = e.keyCode;
    }
    else
        if (e.which) // Netscape/Firefox/Opera
        {
            keynum = e.which;
        }

    if (keynum == 13)
        editNewKey(e, msg);

    //keychar = String.fromCharCode(keynum);
    //numcheck = /\d/;
    //alert( !numcheck.test(keychar) );
    //alert( keynum );
}

////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////
userKeyPressedInput = function (e, msg) {
    var keynum;
    var keychar;
    var numcheck;

    if (window.event) // IE
    {
        keynum = e.keyCode;
    }
    else
        if (e.which) // Netscape/Firefox/Opera
        {
            keynum = e.which;
        }

    if (keynum == 13)
        addKey();
}



////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////
var selectedColor;
selectcolor = function (e, clr) {
    clr = cutHex(clr);
    selectedColor = '#' + clr;

    var n = 10;
    var R = HexToR(clr);
    var G = HexToG(clr);
    var B = HexToB(clr);

    var RR = (255 - R) / n;
    var GG = (255 - G) / n;
    var BB = (255 - B) / n;


    for (var i = n - 1; i > 0; i--) {
        var tcell = document.getElementById('transp' + i + '0');
        var hex = RGBtoHex(R + RR * (n - i), G + GG * (n - i), B + BB * (n - i));

        tcell.style.backgroundColor = '#' + hex;
        tcell.onclick = new Function("evt", "selectsubcolor(evt, '" + hex + "')");

        tcell.style.color = getFontContrastColor(hex);
    }
    mouseDownToolClr(e, cutHex(clr));
}
////////////////////////////////////////////////////////////////////////





////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////
var iscolorpickerinit = false;
showColorPicker = function () {
    document.getElementById('popupcolorpicker').style.display = 'block';

    if (!iscolorpickerinit) {
        cp1 = new Refresh.Web.ColorPicker('cp1', { startMode: 'g', startHex: 'ffcc00' });
        iscolorpickerinit = true;
    }
    document.getElementById('colorpickerarrowmappoint').style.display = 'block';
    document.getElementById('colorpickerarrowrangearrows').style.display = 'block';
}
////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////
hideColorPicker = function () {
    document.getElementById('popupcolorpicker').style.display = 'none';
    document.getElementById('colorpickerarrowmappoint').style.display = 'none';
    document.getElementById('colorpickerarrowrangearrows').style.display = 'none';
}
////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////
pickColor = function (e) {
    document.getElementById('popupcolorpicker').style.display = 'none';
    document.getElementById('colorpickerarrowmappoint').style.display = 'none';
    document.getElementById('colorpickerarrowrangearrows').style.display = 'none';

    var bgcolor = getStyle(document.getElementById('cp1_Preview'), 'background-color');
    if (!bgcolor)
        bgcolor = getStyle(document.getElementById('cp1_Preview'), 'backgroundColor');

    if (bgcolor.toLowerCase().indexOf('rgb') != -1) {
        bgcolor = getHexFromRGBString(bgcolor);
    }

    //alert(bgcolor);

    document.getElementById('customcolor').style.backgroundColor = '#' + cutHex(bgcolor);
    document.getElementById('customcolor').style.color = getFontContrastColor(bgcolor);
    selectcolor(e, bgcolor);
}
////////////////////////////////////////////////////////////////////////




////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////
getFontContrastColor = function (clr) {
    var R = HexToR(clr);
    var G = HexToG(clr);
    var B = HexToB(clr);

    var luma = R * 0.3 + G * 0.59 + B * 0.11;

    if (luma * 1000000 + luma * 1000 + luma > 127627627)
        return '#000000';
    else
        return '#ffffff';
}
////////////////////////////////////////////////////////////////////////





////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////
selectsubcolor = function (e, clr) {
    //alert('fefe');
    selectedColor = '#' + clr;

    //alert(e + ': ' + clr);

    mouseDownToolClr(e, clr);
}
////////////////////////////////////////////////////////////////////////




////////////////////////////////////////////////////////////////////////
initcolortablefontcolor = function () {
    var tab = document.getElementById('colortable');

    var tds = tab.getElementsByTagName('td');

    for (var i = 0; i < tds.length; i++) {
        var bgcolor = getStyle(tds[i], 'background-color');
        if (!bgcolor)
            bgcolor = getStyle(tds[i], 'backgroundColor');

        if (bgcolor.toLowerCase().indexOf('rgb') != -1) {
            bgcolor = getHexFromRGBString(bgcolor);
        }
        tds[i].style.color = getFontContrastColor(bgcolor);
    }
}
////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////
getStyle = function (el, styleProp) {
    if (el.currentStyle)
        var y = el.currentStyle[styleProp];
    else if (window.getComputedStyle)
        var y = document.defaultView.getComputedStyle(el, null).getPropertyValue(styleProp);
    return y;
}

////////////////////////////////////////////////////////////////////////
getHexFromRGBString = function (str) {
    var arr = str.split('(')[1].split(')')[0].split(',');
    return RGBtoHex(arr[0], arr[1], arr[2]);
}






////////////////////////////////////////////////////////////////////////
function HexToR(h) { return parseInt((cutHex(h)).substring(0, 2), 16) }
function HexToG(h) { return parseInt((cutHex(h)).substring(2, 4), 16) }
function HexToB(h) { return parseInt((cutHex(h)).substring(4, 6), 16) }
function cutHex(h) { return (h.charAt(0) == "#") ? h.substring(1, 7) : h }
////////////////////////////////////////////////////////////////////////






////////////////////////////////////////////////////////////////////////
function RGBtoHex(R, G, B) {
    return toHex(R) + toHex(G) + toHex(B)
}

////////////////////////////////////////////////////////////////////////
function toHex(N) {
    if (N == null)
        return "00";

    N = parseInt(N);

    if (N == 0 || isNaN(N))
        return "00";

    N = Math.max(0, N);
    N = Math.min(N, 255);
    N = Math.round(N);

    return "0123456789ABCDEF".charAt((N - N % 16) / 16) + "0123456789ABCDEF".charAt(N % 16);
}








////////////////////////////////////////////////////////////////////////
function trim(s) {
    return s.replace(/^\s*/, "").replace(/\s*$/, "");
}









/////////////////////////////////////////////////////////
// 
/////////////////////////////////////////////////////////
releaseDraggedElement = function (ev) {

    //alert('release');

    //event ? event : window.event; 
    ev = ev || window.event;

    var trg;
    try {
        trg = ev.target || ev.srcElement;
    }
    catch (err) { }

    document.onmouseup = null;


    try { document.body.removeChild(document.getElementById('dragkey')); } catch (err) { }
    try { document.body.removeChild(document.getElementById('dragclear')); } catch (err) { }
    try { document.body.removeChild(document.getElementById('dragclr')); } catch (err) { }

    if (trg.id.toLowerCase().indexOf('sector') != -1)
        return;
    if (trg.id.toLowerCase().indexOf('bgclrblock') != -1)
        return;

    selectedKey = null;
    selectedColor = null;

}






var offsetxkey = 0;
var offsetykey = -45;

var offsetxclr = 0;
var offsetyclr = -45;

var offsetxclear = -23;
var offsetyclear = -25;

var mouse_x_pos = 0;
var mouse_y_pos = 0;

var keyelem;
var cltelem;
var dragok;

var counter = 0;


/////////////////////////////////////////////////////////
// 
/////////////////////////////////////////////////////////
mouseDownToolKey = function (e, msg) {
    if (document.getElementById('dragclear') != null)
        document.body.removeChild(document.getElementById('dragclear'));
    if (document.getElementById('dragkey') != null)
        document.body.removeChild(document.getElementById('dragkey'));

    getMousePositionCoords(e);

    keyelem = document.createElement('div');
    keyelem.id = 'dragkey';
    keyelem.style.position = 'absolute';
    keyelem.style.height = '22px';
    keyelem.style.paddingTop = '18px';
    //		keyelem.style.font            = 'normal 11px "Arial"';
    //		keyelem.style.lineHeight      = '20px';
    //		keyelem.style.border          = 'solid 1px #333';
    //		keyelem.style.background      = 'url(images/dvbg.gif) bottom right no-repeat #ffffff';
    //		keyelem.style.color           = '#000';
    //		keyelem.style.paddingLeft     = '10px';
    //		keyelem.style.paddingRight    = '40px';
    //keyelem.innerHTML             = msg;
    keyelem.innerHTML = "<div style='height: 20px; font: normal 11px \"Arial\"; line-height: 20px; border: solid 1px #333; color: #000;" +
                                    "padding-left: 10px; padding-right: 40px; background: url(../images/dvbg.gif) bottom right no-repeat #ffffff' >" +
                                    msg + "</div>" +
                                    "<img id='notifyright' style='display: none; position: absolute; bottom: 5px; right: 0px;' src='../images/Ok-32x32.png' alt=''/>" +
                                    "<img id='notifywrong' style='display: none; position: absolute; bottom: 5px; right: 0px;' src='../images/Delete-32x32.png' alt=''/>";
    keyelem.style.opacity = 8 / 10;
    keyelem.style.filter = 'alpha(opacity:' + 8 * 10 + ')'; // ie/Win

    document.body.appendChild(keyelem);
    dragok = true;
    document.onmousemove = getmouseposition;
    document.onmouseup = new Function("event", "releaseDraggedElement(event)");
    //document.onclick = new Function ("evt", "releaseDraggedElement(evt)" );

    offsetxkey = -keyelem.offsetWidth;
    keyelem.style.left = (mouse_x_pos + offsetxkey) + 'px';
    keyelem.style.top = (mouse_y_pos + offsetykey) + 'px';
}

/////////////////////////////////////////////////////////
// 
/////////////////////////////////////////////////////////
mouseDownToolClr = function (e, clr) {
    if (document.getElementById('dragclear') != null)
        document.body.removeChild(document.getElementById('dragclear'));
    if (document.getElementById('dragclr') != null)
        document.body.removeChild(document.getElementById('dragclr'));

    getMousePositionCoords(e);

    cltelem = document.createElement('div');
    cltelem.id = 'dragclr';
    cltelem.style.position = 'absolute';

    cltelem.style.width = '40px';
    cltelem.style.height = '22px';
    cltelem.style.paddingTop = '18px';
    cltelem.style.textAlign = 'left';
    //		cltelem.style.font            = 'normal 11px "Arial"';
    //		cltelem.style.lineHeight      = '20px';
    //		cltelem.style.border          = 'solid 1px #333';
    //		cltelem.style.backgroundColor = '#' + clr;
    //		cltelem.style.color           = '#000';
    cltelem.style.opacity = 8 / 10;
    cltelem.style.filter = 'alpha(opacity:' + 8 * 10 + ')'; // ie/Win
    cltelem.innerHTML = "<div style='width: 20px; height: 20px; border: solid 1px #333; color: #000; background-color: #" + clr + "' >" +
                                    "</div>" +
                                    "<img id='notifyright' style='display: none; position: absolute; bottom: 5px; right: 0px;' src='../images/Ok-32x32.png' alt=''/>" +
                                    "<img id='notifywrong' style='display: none; position: absolute; bottom: 5px; right: 0px;' src='../images/Delete-32x32.png' alt=''/>";

    document.body.appendChild(cltelem);
    dragok = true;
    document.onmousemove = getmouseposition;
    document.onmouseup = new Function("event", "releaseDraggedElement(event)");
    //document.onclick = new Function ("evt", "releaseDraggedElement(evt)" );

    offsetxclr = 1;
    cltelem.style.left = (mouse_x_pos + offsetxclr) + 'px';
    cltelem.style.top = (mouse_y_pos + offsetyclr) + 'px';
}

/////////////////////////////////////////////////////////
// 
/////////////////////////////////////////////////////////
var clearbox;
mouseDownToolClear = function (e, arg) {
    //alert(e);

    if (document.getElementById('dragclear') != null) {
        document.body.removeChild(document.getElementById('dragclear'));
        return;
    }

    getMousePositionCoords(e);

    clearbox = document.createElement('div');
    clearbox.id = 'dragclear';
    clearbox.style.position = 'absolute';
    clearbox.style.height = '20px';
    clearbox.style.width = '20px';
    clearbox.style.font = 'normal 11px "Arial"';
    clearbox.style.lineHeight = '20px';
    clearbox.style.border = 'solid 1px #333';
    clearbox.style.color = '#f00';
    clearbox.style.background = 'url(../images/eraser.gif)';
    if (arg == 'text')
        clearbox.style.background = 'url(../images/erasertext.gif)';
    clearbox.style.opacity = 8 / 10;
    clearbox.style.filter = 'alpha(opacity:' + 8 * 10 + ')'; // ie/Win

    clearbox.setAttribute('mode', arg);

    document.body.appendChild(clearbox);
    dragok = true;
    document.onmousemove = getmouseposition;
    //document.onmouseup = releaseDraggedElement;

    offsetxclr = 1;
    clearbox.style.left = (mouse_x_pos + offsetxclear) + 'px';
    clearbox.style.top = (mouse_y_pos + offsetyclear) + 'px';
}

/////////////////////////////////////////////////////////
// 
/////////////////////////////////////////////////////////
mouseReleaseTool = function (e) {
    dragok = false;
    keyelem = null;
    document.onmousemove = null;
}



/////////////////////////////////////////////////////////
// 
/////////////////////////////////////////////////////////
function getmouseposition(e) {
    getMousePositionCoords(e);

    /////////////////////////////////////////////////////////    	
    /////////////////////////////////////////////////////////
    try {
        if (getEventTarget(e).id.indexOf("sector_") != -1) {
            // from 'sector_3' takes '3'
            var secnr = getEventTarget(e).id.substring(7);
            // from 'sector_text_3_0' takes '3'
            if (getEventTarget(e).id.indexOf("sector_text_") != -1) {
                secnr = getEventTarget(e).id.substring(12);
                secnr = secnr.replace('_0', '');
            }

            // if center sector
            if (secnr == "center")
                secnr = -1;

            if (secnr >= nextsectortofill) {
                document.getElementById('notifywrong').style.display = 'block';
                document.getElementById('notifyright').style.display = 'none';
            }
            else {
                document.getElementById('notifywrong').style.display = 'none';
                document.getElementById('notifyright').style.display = 'none';
            }
            if ((secnr == nextsectortofill - 1) ||
                 ((getEventTarget(e).id == "sector_center") && (nextsectortofill == 0))) {
                document.getElementById('notifyright').style.display = 'block';
                document.getElementById('notifywrong').style.display = 'none';

                // if express, can't change center text
                if ((isExpress()) && ((document.getElementById('dragkey') != null)) && (getEventTarget(e).id == "sector_center")) {
                    document.getElementById('notifyright').style.display = 'none';
                    document.getElementById('notifywrong').style.display = 'block';
                }
            }
        }
        else {
            document.getElementById('notifyright').style.display = 'none';
            document.getElementById('notifywrong').style.display = 'none';
        }
    }
    catch (err) { }
    /////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////		
    try {
        keyelem.style.left = (mouse_x_pos + offsetxkey) + 'px';
        keyelem.style.top = (mouse_y_pos + offsetykey) + 'px';
    }
    catch (err) { }
    try {
        cltelem.style.left = (mouse_x_pos + offsetxclr) + 'px';
        cltelem.style.top = (mouse_y_pos + offsetyclr) + 'px';
    }
    catch (err) { }
    try {
        clearbox.style.left = (mouse_x_pos + offsetxclear) + 'px';
        clearbox.style.top = (mouse_y_pos + offsetyclear) + 'px';
    }
    catch (err) { }
}

///////////////////////////////////////////////////////////////
// get element the event fired on
///////////////////////////////////////////////////////////////
getEventTarget = function (ev) {
    ev = ev || window.event;
    var trg;
    try {
        trg = ev.target || ev.srcElement;
    }
    catch (err) { }
    return trg;
}




/////////////////////////////////////////////////////////
// getOffsets
/////////////////////////////////////////////////////////
function getMousePositionCoords(evt) {
    try {
        if (!evt)
            evt = window.event;

        ////////////////////////////////////////////////////////////////
        var scrOfX = 0, scrOfY = 0;
        if (typeof (window.pageYOffset) == 'number') {
            //Netscape compliant
            scrOfY = window.pageYOffset;
            scrOfX = window.pageXOffset;
        }
        else
            if (document.body && (document.body.scrollLeft || document.body.scrollTop)) {
                //DOM compliant
                scrOfY = document.body.scrollTop;
                scrOfX = document.body.scrollLeft;
            }
            else
                if (document.documentElement && (document.documentElement.scrollLeft || document.documentElement.scrollTop)) {
                    //IE6 standards compliant mode
                    scrOfY = document.documentElement.scrollTop;
                    scrOfX = document.documentElement.scrollLeft;
                }
        var eventCoords =
        {
            x: scrOfX + evt.clientX,
            y: scrOfY + evt.clientY
        };

        mouse_x_pos = eventCoords.x;
        mouse_y_pos = eventCoords.y;
    }
    catch (err) {
        //alert('me2' + err);
    }
}

////////////////////////////////////////////////////////////////////////
// Calculation of opposite colors
////////////////////////////////////////////////////////////////////////
doAnalize = function () {
    for (var i = 0; i < highlights.length; i++)
        highlights[i].remove();
    highlights = new Array();

    //Погрешность 128
    var rate = 128;
    var outp = document.getElementById('output');
    outp.innerHTML = '';
    var problemsector = new Array();
    problemsector[0] = -1;
    problemsector[1] = -1;
    var suspectedsectors = new Array();
    var mindifference = 999;


    for (var i = 0; i < piePieces.length / 2; i++) {
        var r = HexToR(colors[i]);
        var g = HexToG(colors[i]);
        var b = HexToB(colors[i]);

        //alert(piePieces.length%2);
        //alert(i);

        // in case of 2 opposing sectors
        for (var j = 0; j <= piePieces.length % 2; j++) {
            var oposedsectorindex = (i + (piePieces.length - piePieces.length % 2) / 2 + j) % piePieces.length;
            if (oposedsectorindex == 0)
                break;

            //alert( (i+1) + ' - ' + (oposedsectorindex+1) );

            var rr = HexToR(colors[oposedsectorindex]);
            var gg = HexToG(colors[oposedsectorindex]);
            var bb = HexToB(colors[oposedsectorindex]);


            outp.innerHTML = outp.innerHTML + 'sector' + 1 * (piePieces[i].node.id.split('_')[1] * 1 + 1) + ' - oposite to - sector' + 1 * (piePieces[oposedsectorindex].node.id.split('_')[1] * 1 + 1);
            outp.innerHTML = outp.innerHTML + ' [invert - #' + RGBtoHex(255 - r, 255 - g, 255 - b) + ' dif - ' + '#' + RGBtoHex(Math.abs(255 - r - rr), Math.abs(255 - g - gg), Math.abs(255 - b - bb)) + ']\n';
            outp.innerHTML = outp.innerHTML + ' [' + 1 * (Math.abs(255 - r - rr) + Math.abs(255 - g - gg) + Math.abs(255 - b - bb)) + ']<br/>\n';

            if ((Math.abs(255 - r - rr) <= rate) &&
                (Math.abs(255 - g - gg) <= rate) &&
                (Math.abs(255 - b - bb) <= rate) &&
                ((r > (g + b) / 2) != (rr > (gg + bb) / 2))) {
                suspectedsectors[suspectedsectors.length] = i;
                suspectedsectors[suspectedsectors.length] = oposedsectorindex;

                if (mindifference > 1 * (Math.abs(255 - r - rr) + Math.abs(255 - g - gg) + Math.abs(255 - b - bb))) {
                    mindifference = 1 * (Math.abs(255 - r - rr) + Math.abs(255 - g - gg) + Math.abs(255 - b - bb))
                    problemsector[0] = i;
                    problemsector[1] = oposedsectorindex;
                }
            }
        }
    }


    outp.innerHTML = outp.innerHTML + '<br/>';

    for (var i = 0; i < suspectedsectors.length; i += 2) {
        for (var k = i; k <= i + 1; k++) {
            var rad = Math.PI / 180;
            var r = papersize / 2.4;
            var r1 = 270;
            var angle = 360 / labels.length
            var startAngle = 90 - angle * (suspectedsectors[k]);
            var endAngle = startAngle - angle;
            var cx = papersize / 2;
            var cy = papersize / 2;
            var x1 = cx + r * Math.cos(-startAngle * rad),
                x2 = cx + r * Math.cos(-endAngle * rad),
                x3 = cx + r1 * Math.cos(-startAngle * rad),
                x4 = cx + r1 * Math.cos(-endAngle * rad),

                y1 = cy + r * Math.sin(-startAngle * rad),
                y2 = cy + r * Math.sin(-endAngle * rad),
                y3 = cy + r1 * Math.sin(-startAngle * rad),
                y4 = cy + r1 * Math.sin(-endAngle * rad);

            var highlightclr = '#FFD1D5';
            if ((suspectedsectors[k] == problemsector[0]) || (suspectedsectors[k] == problemsector[1]))
                highlightclr = '#CA1525';

            highlights[highlights.length] = paper.path({ fill: highlightclr, stroke: '#000000' }).moveTo(x2, y2).arcTo(r, r, (endAngle - startAngle > 180 ? 1 : 0), 0, x1, y1).lineTo(x3, y3).arcTo(r1, r1, (endAngle - startAngle > 180 ? 1 : 0), 1, x4, y4).andClose();
        }
    }




    outp.innerHTML = outp.innerHTML + 'the most contrasting sectors: ' + (problemsector[0] + 1) + ' and ' + (problemsector[1] + 1) + '<br/>';

    outp.innerHTML = outp.innerHTML + '<br/>';
    for (var i = 0; i < piePieces.length; i++) {
        var r = HexToR(colors[i]);
        var g = HexToG(colors[i]);
        var b = HexToB(colors[i]);

        outp.innerHTML = outp.innerHTML + 'sector' + 1 * (piePieces[i].node.id.split('_')[1] * 1 + 1) + ' ' + colors[i];

        if (r > (g + b) / 2)
            outp.innerHTML = outp.innerHTML + ' - warm color<br/>';
        else
            outp.innerHTML = outp.innerHTML + ' - cold color<br/>';
    }

    // works directly with page content
    // to make mesages visible [results.aspx page] [user.aspx page]
    if (suspectedsectors.length == 0) {
        try {
            document.getElementById(noconflictmsgholder).style.display = 'block';
        }
        catch (err) { }
    }
    else {
        try {
            document.getElementById(conflictmsgholder).style.display = 'block';
            var sects = document.getElementById('probsectors');
            var prblms = new Array();

            sects.innerHTML += "<a href='onlinediagnose.aspx?startkey=" + replaceAll(labels[suspectedsectors[0]], ' ', '_') + "'>" + labels[suspectedsectors[0]].capitalize() + '</a>, ';
            sects.innerHTML += "<a href='onlinediagnose.aspx?startkey=" + replaceAll(labels[suspectedsectors[1]], ' ', '_') + "'>" + labels[suspectedsectors[1]].capitalize() + "</a>.";

            sects = document.getElementById('additionalmsg');
            for (var i = 2; i < suspectedsectors.length; i++) {
                sects.style.display = 'block';
                prblms[prblms.length] = suspectedsectors[i];
            }

            for (var i = 0; i < prblms.length; i++) {
                if ((prblms[i] != suspectedsectors[0]) && (prblms[i] != suspectedsectors[1]))
                    sects.innerHTML += "<a href='onlinediagnose.aspx?startkey=" + replaceAll(labels[prblms[i]], ' ', '_') + "'>" + labels[prblms[i]].capitalize() + '</a>, ';
            }
            sects.innerHTML += '.';
            sects.innerHTML = sects.innerHTML.replace(', .', '.')
        }
        catch (err) { }
    }

}

////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////	
collectDataToSave = function () {
    var hidinp = document.getElementById(hiddenid);

    hidinp.value += centercircletxtstr + '@' + centercircleclr + ';';
    for (var i = 0; i < piePieces.length; i++) {
        hidinp.value += labels[i] + '@' + colors[i] + ';';
    }

    if (!isExpress()) {
        hidinp.value += 'bgclrblock@' + bgclrblockcolor + ';';
    }
}


////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////
AlwaysFireBeforeFormSubmit = function (eventTarget, eventArgument) {
    // do whatever pre-form submission
    // chores you need here
    // finally, let the original __doPostBack do its work
    //if( eventTarget.replace(/$/g, '_').toLowerCase() == proceedid.toLowerCase() )
    //if( eventTarget.replace(new RegExp("$","g"), '_').toLowerCase() == proceedid.toLowerCase() )
    if (replaceAll(eventTarget, '$', '_').toLowerCase() == proceedid.toLowerCase()) {
        collectDataToSave();
        //alert('here');

        //			if ( !isExpress() )
        //			{
        //			    //alert('not express');
        //			    return;
        //			}
    }
    return __oldDoPostBack(eventTarget, eventArgument);
}


////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////	
replaceAll = function (text, strA, strB) {
    while (text.indexOf(strA) != -1) {
        text = text.replace(strA, strB);
    }
    return text;
}


////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////
disableProceedLinkButton = function () {
    var btn = document.getElementById(proceedid);
    var tmp;


    tmp = btn.getAttribute("href");
    btn.setAttribute("href", "javascript:");
    if ((btn.getAttribute("serverhref") == null) || (btn.getAttribute("serverhref").length == 0))
        btn.setAttribute("serverhref", tmp);
    btn.style.color = "#aaa";
    btn.style.cursor = "default";
}

////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////
enableProceedLinkButton = function () {
    var btn = document.getElementById(proceedid);

    if ((btn.getAttribute("serverhref") != null) && (btn.getAttribute("serverhref").length > 0)) {
        btn.setAttribute("href", btn.getAttribute("serverhref"));
        btn.setAttribute("serverhref", "");
    }
    btn.style.color = "#000000";
    btn.style.cursor = "pointer";
}








/////////////////////////////////////////////////////////
// 
/////////////////////////////////////////////////////////
getQueryString = function (name) {
    hu = window.location.search.substring(1);
    gy = hu.split("&");

    for (i = 0; i < gy.length; i++) {
        ft = gy[i].split("=");
        if (ft[0].toLowerCase() == name.toLowerCase()) {
            return ft[1];
        }
    }
    return null;
}


/////////////////////////////////////////////////////////
// 
/////////////////////////////////////////////////////////
setHashValue = function (name, value) {
    var isnewpair = true;
    var chashpairs = window.location.hash.replace('#', '').split('&');

    for (var i = 0; i < chashpairs.length; i++) {
        var nm = chashpairs[i].split('=')[0];
        var vl = chashpairs[i].split('=')[1];

        if (nm == name) {
            chashpairs[i] = nm + "=" + value;
            isnewpair = false;
            break;
        }
    }

    if (isnewpair) {
        chashpairs[chashpairs.length] = name + '=' + value;
    }

    var hsh = '';
    for (var i = 0; i < chashpairs.length; i++) {
        hsh += chashpairs[i];
        if ((i != chashpairs.length - 1) && (chashpairs[i] != ''))
            hsh += '&';
    }

    window.location.hash = hsh;
    hashstring = window.location.hash;
}




/////////////////////////////////////////////////////////
// 
/////////////////////////////////////////////////////////
controlChartFromHash = function () {

}






//var hash = location.hash;

setInterval(function () {
    var hashcurrent = location.hash.replace('#', '');
    hashstring = hashstring.replace('#', '');
    if ((hashcurrent != hashstring) && (hashstring != '')) {
        //alert("Changed from " + hashstring + " to " + location.hash);

        var oldpairs = hashstring.split('&');
        var newpairs = hashcurrent.split('&');


        // check if number of sectors changed
        if (oldpairs.length > newpairs.length) {
            removeSector();
            return;
        }
        if (oldpairs.length < newpairs.length) {
            addSector();
            return;
        }

        // parse all old and new hash pairs
        for (var i = 0; i < oldpairs.length; i++) {
            var oldname = oldpairs[i].split('=')[0];
            var oldvalue = oldpairs[i].split('=')[1];

            // 
            for (var j = 0; j < oldpairs.length; j++) {
                var newname = newpairs[j].split('=')[0];
                var newvalue = newpairs[j].split('=')[1];

                // if name matches
                if (oldname == newname) {
                    // if value changed
                    if (oldvalue != newvalue) {
                        var idnr = oldname.replace('str', '').replace('clr', '');

                        //alert(idnr + ' - changed from ' + oldvalue + " to " + newvalue);
                        //alert(oldname.replace('str', '').replace('clr', ''));

                        if (oldname.indexOf('str') != -1) {
                            if (newvalue == emptyString)
                                mouseDownToolClear(null, 'text');
                            else
                                selectedKey = newvalue;

                            //                                if ( oldname.indexOf('center') != -1 )
                            //                                    document.getElementById('sector_' + idnr).onclick();                                    
                            //                                else
                            document.getElementById('sector_' + idnr).onclick();
                        }
                        if (oldname.indexOf('clr') != -1) {
                            if (newvalue.toLowerCase() == 'ffffff')
                                mouseDownToolClear(null, 'color');
                            else
                                selectedColor = '#' + newvalue;

                            //                                if ( oldname.indexOf('center') != -1 )
                            //                                    document.getElementById('sector_center' + idnr).onclick();                                    
                            //                                else
                            document.getElementById('sector_' + idnr).onclick();
                        }

                    }
                    break;
                }
            }
        }







        hashstring = location.hash;
    }
}, 100);





/*
function decode(str) 
{
     var result = "";

     for (var i = 0; i < str.length; i++) 
     {
          if (str.charAt(i) == "+") 
            result += " ";
          else 
            result += str.charAt(i);

          return unescape(result);
     }
}
*/




/* RGB <-> HSV */
/* R. McFarland 20060601 */

function RGBHSV(cache) {
    this.R = 0;
    this.G = 0;
    this.B = 0;
    this.H = 0;
    this.S = 0;
    this.V = 0;
    this.caching = cache;

    this.rgbdict = Array();
    this.hsvdict = Array();

    this.hash = function (arr) {
        h = 360 * 360 * arr[0] + 256 * arr[1] + arr[2];
        return h;
    }

    this.setRGB = function (r, g, b) { // rgb 0-255
        this.R = r;
        if (this.R < 0) this.R = 0;
        if (this.R > 255) this.R = 255;
        this.R /= 255.0;
        this.G = g;
        if (this.G < 0) this.G = 0;
        if (this.G > 255) this.G = 255;
        this.G /= 255.0;
        this.B = b;
        if (this.B < 0) this.B = 0;
        if (this.B > 255) this.B = 255;
        this.B /= 255.0;
        h = this.hash(Array(r, g, b));
        if (this.caching && this.hsvdict[h]) {
            this.H = this.hsvdict[h][0];
            this.S = this.hsvdict[h][1];
            this.V = this.hsvdict[h][2];
        } else {
            this.calcHSV();
            this.hsvdict[h] = Array(this.H, this.S, this.V);
        }
    }

    this.setHSV = function (h, s, v) { // hsv is hue (0-360),saturation (0-100),value (0-100)
        this.H = h % 360; //degrees
        this.S = s;
        if (this.S < 0) this.S = 0;
        if (this.S > 100) this.S = 100;
        this.S /= 100.0;
        this.V = v;
        if (this.V < 0) this.V = 0;
        if (this.V > 100) this.V = 100;
        this.V /= 100.0;
        h = this.hash(Array(h, s, v));
        if (this.caching && this.rgbdict[h]) {
            this.R = this.rgbdict[h][0];
            this.G = this.rgbdict[h][1];
            this.B = this.rgbdict[h][2];
        } else {
            this.calcRGB();
            this.rgbdict[h] = Array(this.R, this.G, this.B);
        }
    }

    this.calcRGB = function () {
        if (this.S == 0.0) {   // color is on black-and-white center line
            R = this.V;          // achromatic: shades of gray
            G = this.V;          // supposedly invalid for h=0 but who cares
            B = this.V;
        } else { // chromatic color
            if (this.H == 360.0) {// 360 degrees same as 0 degrees
                hTemp = 0.0;
            } else {
                hTemp = this.H;
            }
            hTemp = hTemp / 60.0;   // h is now in [0,6)

            fi = Math.floor(hTemp);  // largest integer <= h
            fr = hTemp - fi;          // fractional part of h 
            p = this.V * (1.0 - this.S);
            q = this.V * (1.0 - (this.S * fr));
            t = this.V * (1.0 - (this.S * (1.0 - fr)));

            if (fi == 0) {
                R = this.V;
                G = t;
                B = p;
            } else if (fi == 1) {
                R = q;
                G = this.V;
                B = p;
            } else if (fi == 2) {
                R = p;
                G = this.V;
                B = t;
            } else if (fi == 3) {
                R = p;
                G = q;
                B = this.V;
            } else if (fi == 4) {
                R = t;
                G = p;
                B = this.V;
            } else {
                R = this.V;
                G = p;
                B = q;
            }
        }
        this.R = R;
        this.G = G;
        this.B = B;
        return;
    }

    this.calcHSV = function () {
        minVal = Math.min(Math.min(this.R, this.G), this.B);
        V = Math.max(Math.max(this.R, this.G), this.B);
        Delta = V - minVal;

        // Calculate saturation: saturation is 0 if r, g and b are all 0
        if (V == 0.0) {
            S = 0.0;
        } else {
            S = Delta / V;
        }
        if (S == 0.0) {
            H = 0.0;    // Achromatic: When s = 0, h is undefined but who cares
        } else {       // Chromatic
            if (this.R == V) { // between yellow and magenta [degrees]
                H = 60.0 * (this.G - this.B) / Delta;
            } else {
                if (this.G == V) { // between cyan and yellow
                    H = 120.0 + 60.0 * (this.B - this.R) / Delta;
                } else { // between magenta and cyan
                    H = 240.0 + 60.0 * (this.R - this.G) / Delta;
                }
            }
        }
        if (H < 0.0) H = H + 360.0;
        this.H = H; this.S = S; this.V = V;
        return;
    }

    this.value = function () {
        string = 'rgb('
			+ Math.floor(this.R * 255) + ','
			+ Math.floor(this.G * 255) + ','
			+ Math.floor(this.B * 255) + ')';
        return string;
    }
}








function printStackTrace() {
    var callstack = [];
    var isCallstackPopulated = false;
    try {
        i.dont.exist += 0; //doesn't exist- that's the point
    } catch (e) {
        if (e.stack) { //Firefox
            var lines = e.stack.split("\n");
            for (var i = 0, len = lines.length; i < len; i++) {
                if (lines[i].match(/^\s*[A-Za-z0-9\-_\$]+\(/)) {
                    callstack.push(lines[i]);
                }
            }
            //Remove call to printStackTrace()
            callstack.shift();
            isCallstackPopulated = true;
        }
        else if (window.opera && e.message) { //Opera
            var lines = e.message.split("\n");
            for (var i = 0, len = lines.length; i < len; i++) {
                if (lines[i].match(/^\s*[A-Za-z0-9\-_\$]+\(/)) {
                    var entry = lines[i];
                    //Append next line also since it has the file info
                    if (lines[i + 1]) {
                        entry += " at " + lines[i + 1];
                        i++;
                    }
                    callstack.push(entry);
                }
            }
            //Remove call to printStackTrace()
            callstack.shift();
            isCallstackPopulated = true;
        }
    }
    if (!isCallstackPopulated) { //IE and Safari
        var currentFunction = arguments.callee.caller;
        while (currentFunction) {
            var fn = currentFunction.toString();
            var fname = fn.substring(fn.indexOf("function") + 8, fn.indexOf("(")) || "anonymous";
            callstack.push(fname);
            currentFunction = currentFunction.caller;
        }
    }
    output(callstack);
}

function output(arr) {
    //Optput however you want
    alert(arr.join("nn"));
    prompt(arr.join("\n"));
}
