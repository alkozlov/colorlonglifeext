﻿$(document).ready(function () {
    $("#ddlCountries").change(function () {
        var countryId = $(this).val();
        if (countryId) {
            $.get("../HealthPanel/cities?countryId=" + countryId, function (data) {
                if (data) {
                    var items = '<option>Выберите город</option>';
                    $.each(data, function (i, state) {
                        items += "<option value='" + state.cityId + "'>" + state.name + "</option>";
                    });
                    $('#ddlCities').html(items);
                }
            });
        }
    });

    $('.cbxGender').click(function () {
        $(this).closest('label').prev('input:radio').prop("checked", true);
    });

    $('#InputBirthday').datetimepicker(
        { pickTime: false, language: 'ru' }
    );
});