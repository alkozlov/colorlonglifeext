﻿function Doctor(data) {
    this.doctorId = data.DoctorId;
    this.firstName = data.FirstName;
    this.lastName = data.LastName;
    this.middleName = data.MiddleName;
    this.description = data.Description;
    this.pathToImage = data.PathToImage;
}