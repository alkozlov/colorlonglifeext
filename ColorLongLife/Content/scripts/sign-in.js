﻿$(document).ready(function () {
    $('.cbxRememberMe').click(function () {
        if ($(this).closest('label').prev('input:checkbox').prop('checked')) {
            $(this).closest('label').prev('input:checkbox').prop("checked", false);
        } else {
            $(this).closest('label').prev('input:checkbox').prop("checked", true);
        }
    });
});