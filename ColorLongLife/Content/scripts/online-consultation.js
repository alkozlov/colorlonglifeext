﻿/// <reference path="jquery-1.11.0.js" />
/// <reference path="bootstrap.js" />
/// <reference path="dto-models.js" />

$(document).ready(function () {
    var checkDate = null;

    var consultantsModal = $("#consultantsModal");
    consultantsModal.on('show.bs.modal', function (event) {
        // Http request to get free consultants for specified date and time
        var url = '/cll/api/v1/onlineconsultation/doctors?checkDate=' + checkDate;
        $.get(url)
            .done(function (data) {
                if (data.length > 0) {
                    bindDoctors(data);
                }
            })
            .fail(function () {
                alert('Sorry, something went wrong!');
            });
    });

    $('td.timeslot').click(function () {
        var time = $(this).attr('data-start-time');
        var date = $(this).closest('table').attr('data-date');
        checkDate = date + ' ' + time;

        if (!$(this).hasClass('active')) {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                $(this).addClass('selected');
                consultantsModal.modal({
                    keyboard: false
                });
            }
        }
    });

    function bindDoctors(data) {
        var doctors = [];
        var modalContent = $('.modal-body');
        modalContent.empty();

        $.each(data, function (index, value) {
            var doctor = new Doctor(value);
            doctors.push(doctor);
            console.table(doctors);
        });

        $.each(doctors, function(index, value) {
            var image = jQuery('<img/>', {
                src: value.pathToImage,
                alt: '',
                width: '75',
                height: '75',

            }).addClass('media-object');

            var imageLink = jQuery('<a/>', {
                href: 'javascript:void(0)'
            }).addClass('pull-left');

            var doctorName = jQuery('<h4/>', {
                text: value.firstName + ' ' + value.lastName
            }).addClass('media-heading');

            var doctorDescription = jQuery('<p/>', {
                text: value.description
            });

            var doctorDescriptionContainer = jQuery('<div/>').addClass('media-body');

            var doctorContainer = jQuery('<div/>').addClass('media');

            imageLink.append(image);
            doctorDescriptionContainer.append(doctorName);
            doctorDescriptionContainer.append(doctorDescription);
            doctorContainer.append(imageLink);
            doctorContainer.append(doctorDescriptionContainer);
            modalContent.append(doctorContainer);
        });
    }
});