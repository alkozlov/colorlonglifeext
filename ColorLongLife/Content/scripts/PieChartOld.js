﻿function Sector(categoryId, categoryName, subcategoryId, subcategoryName, emotionId, emotionName, color, opacity) {
    var categoryId = categoryId;
    var categoryName = categoryName;
    var subcategoryId = subcategoryId;
    var subcategoryName = subcategoryName;
    var emotionId = emotionId;
    var emotionName = emotionName;
    var color = color;
    var opacity = opacity;
};

$(function () {
    var sectors = [];
    var colorRgb = "rgb(0,0,0)";
    var opacity = 0.2;

    // Fill the background with white circle
    $("#centerRing").css("fill", "white");
    // Fill the circle with white outline
    $("#externalCenterRing path").css("fill", "white");

    for (i = 0; i < 8; i++) {
        var sector = "#sector" + [i + 1] + " " + "path";
        var circleEdit = "#circleEdit" + [i + 1];
        var step = "#step" + [i + 1] + " " + "circle";
        var textCategory = "#textCategory" + [i + 1];
        var textEmotion = "#textEmotion" + [i + 1];
        var textAddCategorySector = ".textAddCategorySector" + [i + 1];

        $(sector).prop("disabled", true).css("fill", "rgb(244, 244, 244)");
        $(circleEdit).css("display", "none");
        $(step).css("fill", "rgb(255, 255, 255)");
        $(textCategory).css("display", "none");
        $(textEmotion).css("display", "none");
        $(textAddCategorySector).css("display", "none");
    }

    // Change the text color to black
    $("#textCenterCircle").css("fill", "black");
    // Hide panel with categories
    $(".kategories").css("display", "none");

    setColor();
    console.log("1");
    //// Choose a color from the palette
    //$("div .color").click(function() {
    //    // Get the color in RGB format
    //    var color = $(this).css('background-color');
    //    var shade = $("rect .shade").attr('fill-opacity');
    //    //var fn = hex2rgb(color);
    //    console.log(color);
    //    console.log(shade);

    //    $(".shade").css("fill", color).click(function() {

    $("#centerRing").click(function () {
        // Change the text color to white
        $("#textCenterCircle").css("fill", "white");
        // Circle fill the selected color
        $(this).css({ "fill": colorRgb });
        // Circle Contour fill 20% lighter
        $("#externalCenterRing path").css(colorRgb = "opacity", 0.2);


        // if color is choose
        if ($("circle").css("fill") != "rgb(255, 255, 255)") {
            //console.log('11111111');
            $(".textAddColor").css("display", "none");
            // colors don't display
            $(".colorsMenu").css("display", "none");
            // but catigories is display
            $(".kategories").css("display", "inline");

            $("#sector1 path").prop("disabled", false).css("fill", "none");

            $("#circleEdit1").css("display", "inline");

            $("#step1 circle").css({ "stroke": "rgb(40,170,191)", "stroke-width": "2" });

            $(".textAddColor").css("display", "none");
            $(".textAddCategorySector1").css("display", "inline");

            $(".kat-name a").click(function (event) {
                event.preventDefault();
                // Get the text from button
                var name = $(this).text();
                console.log(name);
                var categoryId = $(this).attr('data-category-id');
                //console.log(categoryId);

                //THE FIRST POP-UP
                $.get("../FreeExpressDiagnostics/SubcategoriesGet?categoryId=" + categoryId, function (data) {
                    if (data) {
                        //console.log("Subcategories");
                        $(".modal-title").text(categoryId + ". " + name);
                        var items = "";
                        $("#myModal .problems").empty();
                        $.each(data, function (i, value) {
                            items += '<div class="col-md-4"><div class="kategory-block"><div class="kat-number pull-left"><span>' + (i + 1) +
                                '</span></div><div class="kat-name pull-right"><a href="#" class="subcategory">' + value.Name + '</a></div></div></div>';
                        });
                        $("#myModal .problems").html(items);

                        $(".subcategory").click(function (event) {
                            event.preventDefault();
                            $('#myModal').modal("hide").css("display", "none");
                            $('#myModal1').css({ "overflow-x": "hidden", "overflow-y": "auto", "margin-left": "-21px" });
                            var subcategory = $(this).text();
                            var parent = $(this).closest(".kategory-block");
                            var value = $('div:first span', parent).text();
                            $(".modal-title").text(categoryId + ". " + name);
                            $(".modal-header span").text(categoryId + "." + value + " " + subcategory);
                            //console.log(value);
                            //THE SECOND POP-UP
                            $.get("../FreeExpressDiagnostics/EmotionGet", function (data) {
                                if (data) {
                                    //console.log(data);
                                    var items = "";
                                    $("#myModal1 .problems").empty();
                                    $.each(data, function (i, value) {
                                        items += '<div class="width20"><div class="col-md-12"><div class="emotion-head"></div><div class="emotion-name text-center" ><a class="" >' + value.Title + '</a></div></div></div>';
                                    });
                                    $("#myModal1 .problems").html(items);

                                    //THE THIRD POP-UP
                                    $(".emotion-name").click(function () {
                                        $('#myModal1').modal("hide").css("display", "none");
                                        $('#myModal2').css({ "overflow-x": "hidden", "overflow-y": "auto" });
                                        $(".colors .modalStep3").css("display", "block");

                                        var emotion = $(this).text();
                                        console.log(emotion);

                                        $(".modalStep3 div .color").click(function () {
                                            // Get the color in RGB format
                                            var colorSector = $(this).css("background-color");
                                            console.log(colorSector);

                                            $(".btn-next").click(function () {
                                                $('#myModal2').modal("hide").css("display", "none");
                                                $('#Confirmation').css({ "overflow-x": "hidden", "overflow-y": "auto" });
                                                $("#Confirmation .table-modal").html('<tbody><tr><td>Категория</td><td class="red">' + categoryId + ". " + name
                                                    + '</td></tr><tr><td>Подкатегория</td><td class="red">' + categoryId + "." + value + " " + subcategory
                                                    + '</td></tr><tr><td>Эмоция</td><td class="red">' + emotion
                                                    + '</td></tr><tr><td>Цвет</td><td style = "background-color:' + colorSector + '"></td></tr></tbody>');

                                                $(".btnConfirmation").click(function (event) {
                                                    event.preventDefault();
                                                    $(".textAddCategorySector1").css("display", "none");
                                                    $("#sector1 path").css("fill", colorSector);
                                                    $("#externalSector1 path").css("fill", colorSector).css(colorSector = "opacity", 0.2);
                                                    $("#textCategory1").html(categoryId + "." + value + " " + subcategory).css("display", "block");
                                                    $("#textEmotion1").html(emotion).css("display", "block");
                                                    $("#Confirmation").modal("hide").css("display", "none");
                                                });

                                                $("#Confirmation").modal({
                                                    // wire up the actual modal functionality and show the dialog
                                                    "backdrop": "static",
                                                    "keyboard": true,
                                                    "show": true // ensure the modal is shown immediately
                                                });

                                                //SECTOR TWO
                                                $("#sector1 path").prop("disabled", true);
                                                $("#sector2 path").prop("disabled", false).css("fill", "none");
                                                $("#circleEdit2").css("display", "inline");
                                                $("#step1 circle").css({ "stroke": "#CDCDCD", "stroke-width": "1" });
                                                $("#step2 circle").css({ "stroke": "rgb(40,170,191)", "stroke-width": "2" });
                                                $(".textAddCategorySector2").css("display", "inline");

                                                $(".kat-name a").click(function (event) {
                                                    event.preventDefault();
                                                    // Get the text from button
                                                    var name = $(this).text();
                                                    console.log(name);
                                                    var categoryId = $(this).attr('data-category-id');
                                                    //console.log(categoryId);

                                                    //THE FIRST POP-UP
                                                    $.get("../FreeExpressDiagnostics/SubcategoriesGet?categoryId=" + categoryId, function (data) {
                                                        if (data) {
                                                            //console.log("Subcategories");
                                                            $(".modal-title").text(categoryId + ". " + name);
                                                            var items = "";
                                                            $("#myModal .problems").empty();
                                                            $.each(data, function (i, value) {
                                                                items += '<div class="col-md-4"><div class="kategory-block"><div class="kat-number pull-left"><span>' + (i + 1) +
                                                                    '</span></div><div class="kat-name pull-right"><a href="#" class="subcategory">' + value.Name + '</a></div></div></div>';
                                                            });
                                                            $("#myModal .problems").html(items);

                                                            $(".subcategory").click(function (event) {
                                                                event.preventDefault();
                                                                $('#myModal').modal("hide").css("display", "none");
                                                                $('#myModal1').css({ "overflow-x": "hidden", "overflow-y": "auto", "margin-left": "-21px" });
                                                                var subcategory = $(this).text();
                                                                var parent = $(this).closest(".kategory-block");
                                                                var value = $('div:first span', parent).text();
                                                                $(".modal-title").text(categoryId + ". " + name);
                                                                $(".modal-header span").text(categoryId + "." + value + " " + subcategory);
                                                                //console.log(value);
                                                                //THE SECOND POP-UP
                                                                $.get("../FreeExpressDiagnostics/EmotionGet", function (data) {
                                                                    if (data) {
                                                                        //console.log(data);
                                                                        var items = "";
                                                                        $("#myModal1 .problems").empty();
                                                                        $.each(data, function (i, value) {
                                                                            items += '<div class="width20"><div class="col-md-12"><div class="emotion-head"></div><div class="emotion-name text-center" ><a class="" >' + value.Title + '</a></div></div></div>';
                                                                        });
                                                                        $("#myModal1 .problems").html(items);

                                                                        //THE THIRD POP-UP
                                                                        $(".emotion-name").click(function () {
                                                                            $('#myModal1').modal("hide").css("display", "none");
                                                                            $('#myModal2').css({ "overflow-x": "hidden", "overflow-y": "auto" });
                                                                            $(".colors .modalStep3").css("display", "block");

                                                                            var emotion = $(this).text();
                                                                            console.log(emotion);

                                                                            $(".modalStep3 div .color").click(function () {
                                                                                $("#sector1 path").prop("disabled", true);
                                                                                // Get the color in RGB format
                                                                                var colorSector2 = $(this).css("background-color");
                                                                                console.log(colorSector);

                                                                                console.log(colorSector2);

                                                                                $(".btn-next").click(function () {
                                                                                    $('#myModal2').modal("hide").css("display", "none");
                                                                                    $('#Confirmation').css({ "overflow-x": "hidden", "overflow-y": "auto" });
                                                                                    $("#Confirmation .table-modal").html('<tbody><tr><td>Категория</td><td class="red">' + categoryId + ". " + name
                                                                                        + '</td></tr><tr><td>Подкатегория</td><td class="red">' + categoryId + "." + value + " " + subcategory
                                                                                        + '</td></tr><tr><td>Эмоция</td><td class="red">' + emotion
                                                                                        + '</td></tr><tr><td>Цвет</td><td style = "background-color:' + colorSector2 + '"></td></tr></tbody>');

                                                                                    $(".btnConfirmation").click(function (event) {
                                                                                        event.preventDefault();
                                                                                        console.log(colorSector2);

                                                                                        $(".textAddCategorySector2").css("display", "none");
                                                                                        $("#sector2 path").css("fill", colorSector2);
                                                                                        $("#externalSector2 path").css("fill", colorSector2).css(colorSector2 = "opacity", 0.2);
                                                                                        $("#textCategory2").html(categoryId + "." + value + " " + subcategory).css("display", "block");
                                                                                        $("#textEmotion2").html(emotion).css("display", "block");
                                                                                        $("#Confirmation").modal("hide").css("display", "none");

                                                                                        //SECTOR TWO
                                                                                        $("#sector3 path").prop("disabled", false).css("fill", "none");
                                                                                        $("#circleEdit2").css("display", "inline");
                                                                                        $("#step2 circle").css({ "stroke": "#CDCDCD", "stroke-width": "1" });
                                                                                        $("#step3 circle").css({ "stroke": "rgb(40,170,191)", "stroke-width": "2" });
                                                                                        $(".textAddCategorySector2").css("display", "inline");
                                                                                    });

                                                                                    $("#Confirmation").modal({
                                                                                        // wire up the actual modal functionality and show the dialog
                                                                                        "backdrop": "static",
                                                                                        "keyboard": true,
                                                                                        "show": true // ensure the modal is shown immediately
                                                                                    });
                                                                                });
                                                                            });

                                                                            $("#myModal2").modal({
                                                                                // wire up the actual modal functionality and show the dialog
                                                                                "backdrop": "static",
                                                                                "keyboard": true,
                                                                                "show": true // ensure the modal is shown immediately
                                                                            });

                                                                        });
                                                                        //endEMOTION

                                                                    }
                                                                });
                                                                $("#myModal1").modal({
                                                                    // wire up the actual modal functionality and show the dialog
                                                                    "backdrop": "static",
                                                                    "keyboard": true,
                                                                    "show": true // ensure the modal is shown immediately
                                                                });
                                                            });

                                                        }
                                                    });

                                                    $("#myModal").modal({
                                                        // wire up the actual modal functionality and show the dialog
                                                        "backdrop": "static",
                                                        "keyboard": true,
                                                        "show": true // ensure the modal is shown immediately
                                                    });
                                                });
                                            });
                                        });

                                        $("#myModal2").modal({
                                            // wire up the actual modal functionality and show the dialog
                                            "backdrop": "static",
                                            "keyboard": true,
                                            "show": true // ensure the modal is shown immediately
                                        });

                                    });
                                    //endEMOTION

                                }
                            });
                            $("#myModal1").modal({
                                // wire up the actual modal functionality and show the dialog
                                "backdrop": "static",
                                "keyboard": true,
                                "show": true // ensure the modal is shown immediately
                            });
                        });

                    }
                });

                $("#myModal").modal({
                    // wire up the actual modal functionality and show the dialog
                    "backdrop": "static",
                    "keyboard": true,
                    "show": true // ensure the modal is shown immediately
                });
            });
        }
    });

    function setColor() {
        $("div .color").click(function () {
            // Get the color in RGB format
            colorRgb = $(this).css('background-color');
            console.log(colorRgb);
            $(".shade").css("fill", colorRgb);
        });

        $("div.ottenki rect.shade").click(function () {
            console.log("1245");

            opacity = $(this).attr('fill-opacity'); //0.2 - для примера
            console.log(opacity);
            var rgba = colorRgb.replace(')', ', ' + opacity + ')');
            colorRgb = rgba.replace('rgb', 'rgba');
            console.log(rgba);
        });
    };
});
//    });
//});

function hex2rgb(col) {
    var r, g, b;
    if (col.charAt(0) == '#') {
        col = col.substr(1);
    }
    r = col.charAt(0) + col.charAt(1);
    g = col.charAt(2) + col.charAt(3);
    b = col.charAt(4) + col.charAt(5);
    r = parseInt(r, 16);
    g = parseInt(g, 16);
    b = parseInt(b, 16);
    return 'rgb(' + r + ',' + g + ',' + b + ')';
}

