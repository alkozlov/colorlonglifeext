﻿$(document).ready(function() {
    $("#country").change(function () {
        console.log("countries");
        var countryId = $(this).val();
        if (countryId) {
            console.log("country");
            $.get("../HealthPanel/cities?countryId=" + countryId, function (data) {
                if (data) {
                    console.log("cities");
                    var items = '<option>Выберите город</option>';
                    $.each(data, function (i, state) {
                        console.log("city");
                        items += "<option value='" + state.cityId + "'>" + state.name + "</option>";
                    });
                    $('#city').html(items);
                }
            });
        }
    });
});