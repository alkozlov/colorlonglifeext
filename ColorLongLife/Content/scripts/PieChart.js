﻿/// <reference path="jquery-1.11.0.js" />
/// <reference path="jquery-1.11.0.intellisense.js" />
var maxSectorsCount = 16;
var currentSectorsCount;
var currentSectorIndex = 0;
var sectors = new Array(maxSectorsCount);
var centerCircle = null;
var selectedHappiness = null;
var defaultOpacity = 1;
var selectedColor = null;
var selectedOpacity = null;
var isCircleHaveColor = false;

var isEditMode = false;
var actionType = null;
var editSectorIndex = null;

var selectedCategory = null;
var selectedSubcategory = null;
var selectedEmotion = null;

function Sector(categoryId, categoryName, categoryIndex, subcategoryId, subcategoryName, subcategoryIndex, emotionId, emotionName, color, opacity) {
    this.categoryId = categoryId;
    this.categoryName = categoryName;
    this.categoryIndex = categoryIndex;
    this.subcategoryId = subcategoryId;
    this.subcategoryName = subcategoryName;
    this.subcategoryIndex = subcategoryIndex;
    this.emotionId = emotionId;
    this.emotionName = emotionName;
    this.color = color;
    this.opacity = opacity;
};

function Circle(color, opacity) {
    this.color = color;
    this.opacity = opacity;
}

(function () {

    if (typeof window.CustomEvent === "function") return false;

    function CustomEvent(event, params) {
        params = params || { bubbles: false, cancelable: false, detail: undefined };
        var evt = document.createEvent('CustomEvent');
        evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
        return evt;
    }

    CustomEvent.prototype = window.Event.prototype;

    window.CustomEvent = CustomEvent;
})();

// jquery extend function
$.extend(
{
    redirectPost: function (location, args) {
        var form = '';
        $.each(args, function (key, value) {
            form += '<input type="hidden" name="' + key + '" value="' + value + '">';
        });
        $('<form action="' + location + '" method="POST">' + form + '</form>').appendTo('body').submit();
    }
});

// test data
var testSectors = [
    new Sector('45', 'Деньги/Недвижимость', 5, '4905', 'Не в деньгах счастье', 9, '40', 'Отчаянье', 'rgb(255, 102, 0)', '0.7'),
    new Sector('46', 'Бизнес, карьера, социальный статус', 6, '4979', 'Желание делать всё плохо', 4, '35', 'Замешательство', 'rgb(0, 128, 0)', '0.5'),
    new Sector('47', 'Духовность, предназначение, жизненная цель', 7, '5160', 'Затаённая злоба на душе', 7, '40', 'Отчаянье', 'rgb(0, 0, 0)', '0.4'),
    new Sector('48', 'Яркость жизни', 8, '5290', 'Мне некогда думать о себе, много срочных дел', 3, '67', 'Горечь', 'rgb(0, 0, 255)', '0.5')
];

var testCenterCircle = new Circle('rgb(0, 255, 0)', '0.5');

$(document).ready(function () {
    // Select all modals
    var subcategoriesModal = $('#subcategoriesModal');
    var emotionsModal = $('#emotionsModal');
    var colorsModal = $('#colorsModal');
    var confirmationModal = $('#confirmationModal');
    var happinessModal = $('#happinessModal');
    var increaseSectorsModal = $('#increaseSectorsModal');
    var diagnosticCompleteModal = $('#diagnosticCompleteModal');

    var editSectorButton = $("g[class^='circleEdit']");

    /*------ Do this when modal popups will be shown --------*/
    subcategoriesModal.on('shown.bs.modal', function (event) {
        var modal = $(this);

        $('div.steps ul.pager a', modal).click(function(e) {
            e.preventDefault();
        });

        var modalCaption = isEditMode ?
            sectors[editSectorIndex - 1].categoryIndex + ". " + sectors[editSectorIndex - 1].categoryName :
            selectedCategory.index + ". " + selectedCategory.name;
        $(".modal-title", modal).text(modalCaption);
        $(".problems", modal).empty();

        $.get("../FreeExpressDiagnostics/SubcategoriesGet?categoryId=" + selectedCategory.categoryId, function (data) {
            if (data) {
                var items = "";
                $.each(data, function (i, value) {
                    items += '<div class="col-md-4"><div class="kategory-block"><div class="kat-number pull-left"><span>' + (i + 1) +
                        '</span></div><div class="kat-name pull-right"><a href="#" data-index="' + i + '" data-subcategoryId="' + value.SubcategoryId + '" class="subcategory">' + value.Name + '</a></div></div></div>';
                });
                $(".problems", modal).html(items);

                $('a.subcategory', modal).click(function (e) {
                    e.preventDefault();
                    var subcategoryId = $(this).attr('data-subcategoryId');
                    var subcategoryName = $(this).text();
                    var subcategoryIndex = parseInt($(this).attr('data-index')) + 1;
                    selectedSubcategory = {
                        subcategoryId: subcategoryId,
                        name: subcategoryName,
                        index: subcategoryIndex
                    };

                    if (isEditMode) {
                        sectors[editSectorIndex - 1].categoryId = selectedCategory.categoryId;
                        sectors[editSectorIndex - 1].categoryName = selectedCategory.name;
                        sectors[editSectorIndex - 1].subcategoryId = subcategoryId;
                        sectors[editSectorIndex - 1].subcategoryName = subcategoryName;
                        fireUpdateDiagram();
                        modal.modal('hide');
                    } else {
                        modal.modal('hide');
                        emotionsModal.modal({
                            'backdrop': false,
                            'show': true
                        });
                    }

                });
            }
        });

        $('div.modal-header button.close', modal).click(function (e) {
            e.preventDefault();
            fireUpdateDiagram();
            modal.modal('hide');
        });
    });

    subcategoriesModal.on('hide.bs.modal', function (event) {
        var modal = $(this);

        $('div.modal-header button.close', modal).off('click');
        $('div.steps ul.pager a', modal).off('click');
    });

    emotionsModal.on('shown.bs.modal', function () {
        var modal = $(this);
        var modalCaption = isEditMode ?
            sectors[editSectorIndex - 1].categoryIndex + '.' + sectors[editSectorIndex - 1].subcategoryIndex + ' ' + sectors[editSectorIndex - 1].categoryName :
            selectedCategory.index + selectedSubcategory.index + '. ' + selectedCategory.name;
        $(".modal-title", modal).text(modalCaption);
        $("div.problems", modal).empty();

        $.get("../FreeExpressDiagnostics/EmotionGet", function (data) {
            if (data) {
                var items = "";
                $.each(data, function (i, value) {
                    items += '<div class="width20"><div class="col-md-12"><div class="emotion-head">' + value.Title + '</div>';
                    $.each(value.childs, function (index, child) {
                        items += '<div class="emotion-name text-center"><a class="" href="#" data-index="' + index + '" data-emotionId="' + child.EmotionId + '">' + child.Title + '</a></div>';
                    });
                    items += '</div></div>';
                });
                $("div.problems", modal).html(items);

                $("div.emotion-name a", modal).click(function (e) {
                    e.preventDefault();
                    var emotionId = $(this).attr('data-emotionId');
                    var emotionName = $(this).text();
                    selectedEmotion = {
                        emotionId: emotionId,
                        name: emotionName
                    };

                    if (isEditMode) {
                        sectors[editSectorIndex - 1].emotionId = emotionId;
                        sectors[editSectorIndex - 1].emotionName = emotionName;
                        fireUpdateDiagram();
                        modal.modal('hide');
                    } else {
                        modal.modal('hide');
                        colorsModal.modal({
                            'backdrop': false,
                            'show': true
                        });
                    }
                });
            }
        });

        $('div.modal-header button.close', modal).click(function (e) {
            e.preventDefault();
            fireUpdateDiagram();
            modal.modal('hide');
        });

        $('div.modal-footer button.btn-prev', modal).click(function (e) {
            e.preventDefault();
            modal.modal('hide');
            subcategoriesModal.modal({
                'backdrop': false,
                'show': true
            });
        });
    });

    emotionsModal.on('hide.bs.modal', function (event) {
        var modal = $(this);

        $('div.modal-header button.close', modal).off('click');
        $('div.modal-footer button.btn-prev', modal).off('click');
    });

    colorsModal.on('shown.bs.modal', function (event) {
        var modal = $(this);
        var color = null;
        var opacity = 1;
        var modalCaption = isEditMode ? sectors[editSectorIndex - 1].emotionId : selectedEmotion.name;
        $(".modal-title", modal).text(modalCaption);

        $('div.colors div.color', modal).click(function (e) {
            e.preventDefault();
            color = $(this).css("background-color");
            $('div.block-with-add-color div.add-color', modal).css('background-color', color);
            $('div.block-with-add-color div.add-color', modal).css('border', '10px solid ' + createColorRgba(color, 0.3));

            // Apply selected color to opacity control
            $.each($("div.ottenki-step .ottenok", modal), function (index, value) {
                var currentOpacity = $(value).attr('data-opacity');
                var colorRgba = createColorRgba(color, currentOpacity);
                $(value).css('background-color', colorRgba);
            });
        });

        $('div.ottenki-step div.ottenok', modal).click(function (e) {
            e.preventDefault();
            opacity = $(this).attr('data-opacity');
            $('div.block-with-add-color div.add-color', modal).css('background-color', createColorRgba(color, opacity));
            $('div.block-with-add-color div.add-color', modal).css('border', '10px solid ' + createColorRgba(color, 0.3));
        });

        $('div.modal-header button.close', modal).click(function (e) {
            e.preventDefault();
            fireUpdateDiagram();
            modal.modal('hide');
        });

        $('div.modal-footer button.btn-prev', modal).click(function (e) {
            e.preventDefault();
            modal.modal('hide');
            emotionsModal.modal({
                'backdrop': false,
                'show': true
            });
        });

        $('div.modal-footer button.btn-next', modal).click(function (e) {
            e.preventDefault();
            if (color != null) {
                selectedColor = {
                    color: color,
                    opacity: opacity
                };

                if (isEditMode) {
                    sectors[editSectorIndex - 1].color = color;
                    sectors[editSectorIndex - 1].opacity = opacity;
                    fireUpdateDiagram();
                    modal.modal('hide');
                } else {
                    modal.modal('hide');
                    confirmationModal.modal({
                        'backdrop': false,
                        'show': true
                    });
                }
            } else {
                alert('Выберите цвет!');
            }
        });
    });

    colorsModal.on('hide.bs.modal', function (event) {
        var modal = $(this);

        $('div.colors div.color', modal).off('click');
        $('div.ottenki-step div.ottenok', modal).off('click');
        $('div.modal-header button.close', modal).off('click');
        $('div.modal-footer button.btn-prev', modal).off('click');
        $('div.modal-footer button.btn-next', modal).off('click');
    });

    confirmationModal.on('shown.bs.modal', function (event) {
        var modal = $(this);
        $('span.sector-index', modal).text(currentSectorIndex + 1);

        $(".table-modal", modal).html('<tbody><tr><td>Категория</td><td class="red">' + selectedCategory.index + ". " + selectedCategory.name
            + '</td></tr><tr><td>Подкатегория</td><td class="red">' + selectedCategory.index + "." + selectedSubcategory.index + " " + selectedSubcategory.name
            + '</td></tr><tr><td>Эмоция</td><td class="red">' + selectedEmotion.name
            + '</td></tr><tr><td>Цвет</td><td style = "background-color:' + createColorRgba(selectedColor.color, selectedColor.opacity) + '"></td></tr></tbody>');

        $('div.modal-header button.close', modal).click(function (e) {
            e.preventDefault();
            fireUpdateDiagram();
            modal.modal('hide');
        });

        $('div.modal-body a.readmore', modal).click(function (e) {
            e.preventDefault();
            modal.modal('hide');
            colorsModal.modal({
                'backdrop': false,
                'show': true
            });
        });

        // OK button
        $('div.modal-body a.btnConfirmation', modal).click(function (e) {
            e.preventDefault();
            var newSector = new Sector(selectedCategory.categoryId,
                selectedCategory.name,
                selectedCategory.index,
                selectedSubcategory.subcategoryId,
                selectedSubcategory.name,
                selectedSubcategory.index,
                selectedEmotion.emotionId,
                selectedEmotion.name,
                selectedColor.color,
                selectedColor.opacity);
            sectors[currentSectorIndex] = newSector;
            if (currentSectorIndex < currentSectorsCount) {
                currentSectorIndex += 1;
            }

            fireUpdateDiagram();
            modal.modal('hide');
        });
    });

    confirmationModal.on('hide.bs.modal', function (event) {
        var modal = $(this);

        $('div.modal-header button.close', modal).off('click');
        $('div.modal-body a.readmore', modal).off('click');
        $('div.modal-body a.btnConfirmation', modal).off('click');
    });

    diagnosticCompleteModal.on('shown.bs.modal', function (event) {
        var modal = $(this);

        $('div.paddingleft0 a.btn-buy', modal).click(function(e) {
            e.preventDefault();
            increaseSectorsModal.modal({
                'backdrop': false,
                'show': true
            });
            modal.modal('hide');
        });

        $('div.padding0 > a.btn-buy', modal).click(function (e) {
            e.preventDefault();
            happinessModal.modal({
                'backdrop': false,
                'show': true
            });
            modal.modal('hide');
        });

        $('div.modal-header button.close', modal).click(function (e) {
            e.preventDefault();
        });
    });

    diagnosticCompleteModal.on('hide.bs.modal', function (event) {
        var modal = $(this);

        $('div.paddingleft0 a.btn-buy', modal).off('click');
        $('div.padding0 > a.btn-buy', modal).off('click');
        $('div.modal-header button.close', modal).off('click');
    });

    increaseSectorsModal.on('shown.bs.modal', function (event) {
        var modal = $(this);
        var inputSectorsCount = currentSectorsCount;
        $('input.numeric', modal).val(inputSectorsCount);

        // -
        $('button.sector-minus', modal).click(function(e) {
            e.preventDefault();
            var sectorsCount = parseInt($('input.numeric', modal).val());
            if (sectorsCount > 4) {
                sectorsCount -= 2;
                inputSectorsCount = sectorsCount;
                $('input.numeric', modal).val(sectorsCount);
            }
        });

        // +
        $('button.sector-plus', modal).click(function (e) {
            e.preventDefault();
            var sectorsCount = parseInt($('input.numeric', modal).val());
            if (sectorsCount < 16) {
                sectorsCount += 2;
                inputSectorsCount = sectorsCount;
                $('input.numeric', modal).val(sectorsCount);
            }
        });

        // OK
        $('div.padding0 a.btn-buy:first', modal).click(function(e) {
            e.preventDefault();
            currentSectorsCount = inputSectorsCount;
            modal.modal('hide');
            fireUpdateDiagram();
        });
        // Cancel
        $('div.padding0 a.btn-buy:last', modal).click(function(e) {
            e.preventDefault();
            diagnosticCompleteModal.modal({
                'backdrop': false,
                'show': true
            });
            modal.modal('hide');
        });
        $('div.modal-header button.close', modal).click(function(e) {
            e.preventDefault();
        });
    });

    increaseSectorsModal.on('hide.bs.modal', function (event) {
        var modal = $(this);

        $('button.sector-minus', modal).off('click');
        $('button.sector-plus', modal).off('click');
        $('div.padding0 a.btn-buy:first', modal).off('click');
        $('div.padding0 a.btn-buy:last', modal).off('click');
        $('div.modal-header button.close', modal).off('click');
    });

    happinessModal.on('shown.bs.modal', function (event) {
        var modal = $(this);

        $('div.happy-blocks a.btn-buy', modal).click(function(e) {
            e.preventDefault();
            selectedHappiness = {
                happinessId: $(this).attr('data-happinessId')
            };
            $('div.happy-blocks div.body-part').removeClass('checked-block');
            $(this, modal).closest('div.body-part', modal).addClass('checked-block');
        });

        $('div.modal-footer button.btn', modal).click(function(e) {
            e.preventDefault();
            // Build object to request
            var resultSectors = [];
            $.each(sectors, function(index, value) {
                if (value != null) {
                    resultSectors.push({
                        CategoryId: value.categoryId,
                        SubcategoryId: value.subcategoryId,
                        EmotionId: value.emotionId,
                        Color: value.color,
                        Opacity: value.opacity
                    });
                }
            });
            var data = {
                HappinessId: selectedHappiness.happinessId,
                CircleColor: centerCircle.color,
                CircleOpacity: centerCircle.opacity,
                Sectors: resultSectors
            };
            console.log(data);

            //$.redirectPost('expressdiagnosticsresults', data);
            $.post('expressdiagnosticsresults', data)
                .done(function(data) {
                    window.location.replace('expressdiagnosticsresults');
                });
        });
    });

    happinessModal.on('hide.bs.modal', function (event) {
        var modal = $(this);
        $('div.happy-blocks a.btn-by', modal).off('click');
    });

    /*------ Do this when modal popups will be shown - END --------*/

    /*-------------  Update diagram event and listener  -----------------*/

    var updateDiagramEvent = new CustomEvent('updateDiagramEvent');
    $("svg[class^='pieChart']").each(function (index, value) {
        value.addEventListener('updateDiagramEvent', updateDiagramCallback);
    });

    function updateDiagramCallback(e) {
        resetEditParams();
        updateDiagram();
        resetSelectedItems();
        resetDiagramView();
    }

    function fireUpdateDiagram() {
        console.log('Event: updateDiagramEvent');
        $("svg[class^='pieChart']").each(function (index, value) {
            value.dispatchEvent(updateDiagramEvent);
        });
    }

    /*-------------  Update diagram event and listener - END  -----------------*/

    /*--------------------------------------- Reset diagram ------------------------------------------------*/
    // Reset circle
    $(".centerRing").css("fill", "white");
    $(".externalCenterRing path").css("fill", "white");

    for (var i = 0; i < maxSectorsCount; i++) {
        var sector = ".sector" + [i + 1] + " " + "path";
        var circleEdit = ".circleEdit" + [i + 1];
        var step = ".step" + [i + 1] + " " + "circle";
        var textCategory = ".textCategory" + [i + 1];
        var textEmotion = ".textEmotion" + [i + 1];
        var textAddCategorySector = ".textAddCategorySector" + [i + 1];

        $(sector).prop("disabled", true).css("fill", "rgb(244, 244, 244)");
        $(circleEdit).css("display", "none");
        $(step).css("fill", "rgb(255, 255, 255)");
        $(textCategory).css("display", "none");
        $(textEmotion).css("display", "none");
        $(textAddCategorySector).css("display", "none");
    }

    // Change the text color to black
    $(".textCenterCircle").css("fill", "black");
    // Hide panel with categories
    $(".kategories").css("display", "none");

    /*--------------------------------------- Reset diagram - END ------------------------------------------------*/

    $("div.colorsMenu div.color").click(function () {
        // Take color
        selectedColor = $(this).css('background-color');
        // Apply selected color to opacity control
        $.each($("div.ottenki-menu .ottenok"), function (index, value) {
            var opacity = $(value).attr('data-opacity');
            var colorRgba = createColorRgba(selectedColor, opacity);
            $(value).css('background-color', colorRgba);
        });
    });

    $("div.ottenki-menu .ottenok").click(function () {
        var opacity = $(this).attr('data-opacity');
        selectedOpacity = opacity;
    });

    $(".centerRing").click(function () {
        fillCenterRing($(this));
    });

    $('div.kategories .category').click(function (event) {
        event.preventDefault();
        if (currentSectorIndex < currentSectorsCount || isEditMode) {
            var name = $(this).text();
            var categoryId = $(this).attr('data-category-id');
            var categoryIndex = parseInt($(this).attr('data-index')) + 1;
            selectedCategory = {
                categoryId: categoryId,
                name: name,
                index: categoryIndex
            };
            subcategoriesModal.modal({
                'backdrop': false,
                'show': true
            });
        }
    });

    $("g[class^='circleEdit']").on('show.bs.popover', function () {

    });

    $("g[class^='circleEdit']").on('hide.bs.popover', function () {

    });

    // Всплывающее меню для кнопок редактирования
    editSectorButton.popover({
        'trigger': 'click',
        'container': 'body',
        'placement': 'top',
        'white-space': 'nowrap',
        'html': 'true',
        'content': $('div.popover-editor:first')
    });

    // Клик по всем кнопкам редактирования сектора
    editSectorButton.click(function (event) {
        event.preventDefault();
        var currentButton = $(this);
        editSectorIndex = currentButton.data('sectorId');
    });

    editSectorButton.hover(
        function() {
            $('svg g:first', $(this)).show();
            $('svg g:last', $(this)).hide();
        },
        function() {
            $('svg g:first', $(this)).hide();
            $('svg g:last', $(this)).show();
        });

    editSectorButton.mousedown(function () {
        $('svg g:first', $(this)).attr('fill', 'white');
        $('circle:last', $(this)).attr('fill', 'rgb(40,170,191)');
    });

    editSectorButton.mouseup(function () {
        $('svg g:first', $(this)).attr('fill', 'rgb(40,170,191)');
        $('circle:last', $(this)).attr('fill', 'white');
    });

    $('div.popover-editor a.list-group-item').click(function (event) {
        event.preventDefault();
        actionType = $(this).attr('data-action');
        isEditMode = true;
        editSectorButton.popover('hide');
        switch (actionType) {
            case '1': // Change category
                {
                    alert('Выберите одну из категорий списка слева.');
                } break;
            case '2': // Change emotion
                {
                    emotionsModal.modal({
                        'backdrop': false,
                        'show': true
                    });
                } break;
            case '3': // Change color
                {
                    colorsModal.modal({
                        'backdrop': false,
                        'show': true
                    });
                } break;
        }
    });

    $('.sectors-counter-panel .decrease-button').click(function() {
        if (currentSectorsCount > 4) {
            currentSectorsCount -= 2;
            for (var i = currentSectorsCount; i < sectors.length; i++) {
                sectors[i] = null;
            }
            fireUpdateDiagram();
        }
    });

    $('.sectors-counter-panel .increase-button').click(function () {
        if (currentSectorsCount < 16) {
            currentSectorsCount += 2;
            fireUpdateDiagram();
        }
    });

    // -------------------- Helpers
    function createColorRgba(color, opacity) {
        if (opacity === null) {
            opacity = defaultOpacity;
        }
        var rgba = color.replace(')', ', ' + opacity + ')');
        rgba = rgba.replace('rgb', 'rgba');

        return rgba;
    }

    function fillCenterRing(circleSelector) {
        if (selectedColor != null) {
            // Change the text color to white
            $(".textCenterCircle").css("fill", "white");
            var colorRgba = createColorRgba(selectedColor, selectedOpacity);
            // Circle fill the selected color
            //$(circleSelector).css("fill", colorRgba);
            $('.centerRing').css("fill", colorRgba);
            // Circle Contour fill 20% lighter
            $(".externalCenterRing path").css("fill", selectedColor);
            if (selectedOpacity >= 0.5) {
                $(".externalCenterRing path").css(selectedColor = "opacity", 0.2);
            } else {
                $(".externalCenterRing path").css(selectedColor = "opacity", 0.8);
            }
            isCircleHaveColor = true;
            centerCircle = new Circle(selectedColor, selectedOpacity);
            $('.textAddColor').css("display", "none");
            selectedColor = null;
            selectedOpacity = null;
            $('.colorsMenu').hide();
            $('.kategories').show();
        } else {
            if (isCircleHaveColor) {
                $('.colorsMenu').show();
                $('.kategories').hide();
            }
        }

        //updateDiagram();
        fireUpdateDiagram();
    }

    function resetSelectedItems() {
        selectedCategory = null;
        selectedSubcategory = null;
        selectedEmotion = null;
        selectedColor = null;
        selectedOpacity = null;
    }

    function fillNextSector() {
        // Fill next sector
        if (currentSectorIndex <= maxSectorsCount && isCircleHaveColor) {
            var nextSector = $('g.sectors.sector' + (currentSectorIndex + 1));
            $('path', nextSector).css('fill', 'white');
            $('text.textAddCategorySector' + (currentSectorIndex + 1), nextSector).css('display', 'block');
            $('text.textCategory' + (currentSectorIndex + 1), nextSector).css('display', 'none').empty();
            $('text.textEmotion' + (currentSectorIndex + 1), nextSector).css('display', 'none').empty();
            $('g.step' + (currentSectorIndex + 1) + ' circle').attr('class', '');
            $('g.step' + (currentSectorIndex + 1) + ' circle').attr('stroke-width', '2').attr('stroke', 'rgb(40,170,191)');
        }
    }

    function fillSector(index, sectorData) {
        if (sectorData) {
            var sectorNumber = index + 1;
            var currentSector = $('g.sectors.sector' + sectorNumber);

            var categoryCaption = sectorData.categoryIndex + '. ' + (sectorData.categoryName.length > 7 ? sectorData.categoryName.substring(0, 6) + '...' : sectorData.categoryName);
            var subcategoryCaption = sectorData.categoryIndex + '.' + sectorData.subcategoryIndex + ' ' + (sectorData.subcategoryName.length > 7 ? sectorData.subcategoryName.substring(0, 6) + '...' : sectorData.subcategoryName);
            var emotionCaption = sectorData.emotionName.length > 7 ? sectorData.emotionName.substring(0, 6) + '...' : sectorData.emotionName;

            $('path', currentSector).css('fill', createColorRgba(sectorData.color, sectorData.opacity));
            $('text.textAddCategorySector' + sectorNumber, currentSector).css('display', 'none');
            $('text.textCategory' + sectorNumber, currentSector).text(categoryCaption).css('text-anchor', 'center').css('display', 'block').attr('title', sectorData.categoryName);
            $('text.textSubcategory' + sectorNumber, currentSector).text(subcategoryCaption).css('text-anchor', 'center').css('display', 'block').attr('title', sectorData.subcategoryName);
            $('text.textEmotion' + sectorNumber, currentSector).text(emotionCaption).css('text-anchor', 'center').css('display', 'block').attr('title', sectorData.emotionName);
            $('text', currentSector).tooltip({
                'container': 'body',
                'placement': 'top'
            });

            var externalOpacity = sectorData.opacity >= 0.5 ? 0.8 : 0.2;
            $('g.externalSectors.externalSector' + sectorNumber + ' path').css('fill', createColorRgba('rgb(255,255,255)', externalOpacity));
            $('g.step' + sectorNumber + ' circle').attr('class', 'str2');

            var editButton = $('g.circleEdit' + sectorNumber);
            editButton.data('sectorId', sectorNumber);
            editButton.css('display', '');
        }
    }

    function updateDiagram() {
        $.each(sectors, function (index, value) {
            fillSector(index, value);
        });

        // Update next sector
        fillNextSector();
    }

    function resetEditParams() {
        isEditMode = false;
        actionType = null;
        editSectorIndex = null;

        selectedCategory = null;
        selectedSubcategory = null;
        selectedEmotion = null;
    }

    function resetDiagramView() {
        $("svg[class^='pieChart']").hide();
        $('svg.pieChart' + currentSectorsCount).show();
        $('.sectors-counter-panel .sectors-counter').val(currentSectorsCount);

        if (currentSectorIndex === currentSectorsCount) {
            diagnosticCompleteModal.modal({
                'backdrop': false,
                'show': true
            });
        }
    }

    function initialize() {
        currentSectorsCount = 4; // костыль
        
		/* Test data (remove before push) */

        sectors[0] = testSectors[0];
        sectors[1] = testSectors[1];
        sectors[2] = testSectors[2];
        sectors[3] = testSectors[3];
        centerCircle = testCenterCircle;
        fillCenterRing();
        currentSectorIndex = 4;

        /* Test data (remove before push) - END */

        fireUpdateDiagram();
    }
	
	initialize();
});

//Convert from hex to rgb
function hexToRgb(hex) {
    // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    hex = hex.replace(shorthandRegex, function (m, r, g, b) {
        return r + r + g + g + b + b;
    });

    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}