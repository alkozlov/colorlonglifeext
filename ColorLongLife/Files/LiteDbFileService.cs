﻿using System;
using System.IO;
using System.Web;
using LiteDB;

namespace ColorLongLife.Files
{
    public class LiteDbFileService : IFileService
    {
        private readonly String StorageBaseName = "ImageStorage.db";
        private readonly String StorageFolderAbsoute = "/Content/Storage/";
        private String _storageFolder;
        private String _storageDbFile;

        public LiteDbFileService()
        {
            this._storageFolder = HttpContext.Current.Server.MapPath(this.StorageFolderAbsoute);
            this._storageDbFile = String.Concat(this._storageFolder, this.StorageBaseName);
        }

        public String StoreFile(Stream inputStream, String originalFileName)
        {
            // 1 - Create storage file name
            String fileKey = Guid.NewGuid().ToString("D");
            String storageFileName = String.Concat(fileKey, Path.GetExtension(originalFileName));

            // 2 - Save file in file system
            String storageFileFullName = String.Concat(this._storageFolder, storageFileName);
            var fileStream = File.Create(storageFileFullName, (int)inputStream.Length);
            byte[] bytesInStream = new byte[inputStream.Length];
            inputStream.Read(bytesInStream, 0, bytesInStream.Length);
            fileStream.Write(bytesInStream, 0, bytesInStream.Length);

            // 3 - Duplicate file in LiteDB
            LiteDatabase liteDatabase = new LiteDatabase(this._storageDbFile);
            LiteFileInfo liteFileInfo = liteDatabase.FileStorage.Upload(storageFileName, fileStream);
            fileStream.Close();

            return liteFileInfo.Id;
        }

        public void DeleteFile(String storageFileKey)
        {
            // 1 - Delete file from database
            LiteDatabase liteDatabase = new LiteDatabase(this._storageDbFile);
            if (liteDatabase.FileStorage.Exists(storageFileKey))
            {
                liteDatabase.FileStorage.Delete(storageFileKey);
            }

            // 2 - Delete file from fyle system
            String storageFileFullName = String.Concat(this._storageFolder, storageFileKey);
            FileInfo fileInfo = new FileInfo(storageFileFullName);
            if (fileInfo.Exists)
            {
                fileInfo.Delete();
            }
        }

        public string GetFileAccessUrl(String storageFileKey)
        {
            String storageFileFullName = String.Concat(this.StorageFolderAbsoute, storageFileKey);

            return storageFileFullName;
        }
    }
}