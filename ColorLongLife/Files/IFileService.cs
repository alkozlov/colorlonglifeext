﻿using System;
using System.IO;

namespace ColorLongLife.Files
{
    public interface IFileService
    {
        String StoreFile(Stream inputStream, String originalFileName);
        void DeleteFile(String storageFileKey);
        String GetFileAccessUrl(String storageFileKey);
    }
}