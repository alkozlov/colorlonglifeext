﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ColorLongLife.Areas.admin.Models.Positions
{
    public class PositionModel
    {
        public Int32 PositionId { get; set; }

        [Required(ErrorMessage = "Поле \"Заголовок\" обязательное")]
        [MaxLength(128, ErrorMessage = "Слишком длинный заголовок")]
        [MinLength(3, ErrorMessage = "Слишком короткий заголовок")]
        public String Title { get; set; }

        [Required(ErrorMessage = "Поле \"Описание\" обязательное")]
        [MaxLength(128, ErrorMessage = "Слишком длинное описание")]
        [MinLength(3, ErrorMessage = "Слишком короткое описание")]
        public String Description { get; set; }
    }
}