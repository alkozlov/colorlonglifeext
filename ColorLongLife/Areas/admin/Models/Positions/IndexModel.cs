﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ColorLongLife.Areas.admin.Models.Positions
{
    public class IndexModel
    {
        public IEnumerable<PositionModel> Positions { get; set; }

        public IndexModel()
        {
            this.Positions = new List<PositionModel>();
        }
    }
}