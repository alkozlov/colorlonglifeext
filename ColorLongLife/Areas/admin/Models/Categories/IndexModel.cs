﻿using System.Collections.Generic;

namespace ColorLongLife.Areas.admin.Models.Categories
{
    public class IndexModel
    {
        public List<CategoryModel> Categories { get; set; }

        public IndexModel()
        {
            this.Categories = new List<CategoryModel>();
        }
    }
}