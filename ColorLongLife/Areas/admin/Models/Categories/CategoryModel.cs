﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ColorLongLife.Areas.admin.Models.Categories
{
    public class CategoryModel
    {
        public Int32 CategoryId { get; set; }

        [Required(ErrorMessage = "Название категории не указано.")]
        [MaxLength(128, ErrorMessage = "Максимальная длина названия 128 символов.")]
        public String Name { get; set; }

        [MaxLength(1024, ErrorMessage = "Максимальная длина описания 1024 символа.")]
        public String Description { get; set; }
    }
}