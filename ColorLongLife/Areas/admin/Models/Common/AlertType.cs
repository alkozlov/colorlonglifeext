﻿namespace ColorLongLife.Areas.admin.Models.Common
{
    public enum AlertType : int
    {
        Success = 0,
        Error = 1,
        Warning = 2,
        Info = 3
    }
}