﻿using System;

namespace ColorLongLife.Areas.admin.Models.Common
{
    public class Alert
    {
        public AlertType AlertType { get; private set; } 

        public String Title { get; private set; }

        public String Message { get; private set; }

        public Alert(AlertType alertType, String title, String message)
        {
            this.AlertType = alertType;
            this.Title = title;
            this.Message = message;
        }
    }
}