﻿using System;

namespace ColorLongLife.Areas.admin.Models.Consultants
{
    public class ConsultantModel
    {
        public Int32 DoctorId { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String MiddleName { get; set; }
        public String Description { get; set; }
        public String PathToImage { get; set; }
        public Int32 PositionId { get; set; }
        public String PositionName { get; set; }
    }
}