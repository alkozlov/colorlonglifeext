﻿using System.Collections.Generic;

namespace ColorLongLife.Areas.admin.Models.Consultants
{
    public class IndexModel
    {
        public List<ConsultantModel> Consultants { get; set; }

        public IndexModel()
        {
            this.Consultants = new List<ConsultantModel>();
        }
    }
}