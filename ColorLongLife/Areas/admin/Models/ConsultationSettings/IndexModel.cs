﻿using System.Collections.Generic;

namespace ColorLongLife.Areas.admin.Models.ConsultationSettings
{
    public class IndexModel
    {
        public List<ConsultationSettingsModel> ConsultationSettings { get; set; }

        public IndexModel()
        {
            this.ConsultationSettings = new List<ConsultationSettingsModel>();
        }
    }
}