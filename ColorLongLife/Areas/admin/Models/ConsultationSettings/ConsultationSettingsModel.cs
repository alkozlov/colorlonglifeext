﻿using System;

namespace ColorLongLife.Areas.admin.Models.ConsultationSettings
{
    public class ConsultationSettingsModel
    {
        public Int32 ConsultationSettingsId { get; set; }

        public DateTime Date { get; set; }

        public TimeSpan StartTime { get; set; }

        public TimeSpan EndTime { get; set; }

        public TimeSpan TimeslotDuration { get; set; }

        public TimeSpan RestDuration { get; set; }

        public Boolean IsActive { get; set; }

        public Int32 ApplyFor { get; set; }
    }
}
