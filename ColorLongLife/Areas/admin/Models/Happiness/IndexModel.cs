﻿using System;
using System.Collections.Generic;

namespace ColorLongLife.Areas.admin.Models.Happiness
{
    public class IndexModel
    {
        public IEnumerable<HappinessModel> HappinessList { get; set; }

        public IndexModel()
        {
            HappinessList = new List<HappinessModel>();
        }
    }
}