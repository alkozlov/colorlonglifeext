﻿using System.Collections.Generic;

namespace ColorLongLife.Areas.admin.Models.Users
{
    public class IndexModel
    {
        public IEnumerable<UserListItem> Users { get; set; }

        public IndexModel()
        {
            this.Users = new List<UserListItem>();
        }
    }
}