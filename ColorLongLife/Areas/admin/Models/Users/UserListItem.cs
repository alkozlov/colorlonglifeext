﻿using System;

namespace ColorLongLife.Areas.admin.Models.Users
{
    public class UserListItem
    {
        public Int32 UserId { get; set; } 

        public String FirstName { get; set; }

        public String LastName { get; set; }

        public String MiddleName { get; set; }

        public String Email { get; set; }
    }
}