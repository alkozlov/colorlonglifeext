﻿using System;
using System.ComponentModel.DataAnnotations;
using ColorLongLife.Utilities;

namespace ColorLongLife.Areas.admin.Models.Users
{
    public class UserModel
    {
        [Required(ErrorMessage = "Укажите имя.")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов.")]
        public String FirstName { get; set; }

        [Required(ErrorMessage = "Укажите фамилию.")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Длина строки должна быть от 2 до 50 символов.")]
        public String LastName { get; set; }

        public String MiddleName { get; set; }

        [Required(ErrorMessage = "Укажите день рождения, например: 25.11.1990.")]
        //[DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = false)]
        public DateTime Birthday { get; set; }

        public Gender Gender { get; set; }

        [Required(ErrorMessage = "Укажите ваш вес.")]
        [Range(40, 300, ErrorMessage = "Вес может быть от 40 до 300кг.")]
        public Int32 Weight { get; set; }

        [Required(ErrorMessage = "Укажите ваш рост.")]
        [Range(100, 250, ErrorMessage = "Рост должен быть от 100 до 250см.")]
        public Int32 Growth { get; set; }

        [Required(ErrorMessage = "Укажите ваш номер телефона.")]
        [StringLength(20, MinimumLength = 7, ErrorMessage = "Длина строки должна быть от 7 до 20 символов.")]
        public String Phone { get; set; }

        [Required(ErrorMessage = "Укажите эл. почту.")]
        [StringLength(50, MinimumLength = 7, ErrorMessage = "Длина эл. почты должна быть от 7 до 50 символов.")]
        [EmailAddress(ErrorMessage = "Эл. почта введена некорректно.")]
        public String Email { get; set; }

        [Required(ErrorMessage = "Укажите пароль.")]
        [StringLength(50, MinimumLength = 6, ErrorMessage = "Длина пароля должна быть от 6 до 50 символов.")]
        public String Password { get; set; }

        [Required(ErrorMessage = "Повторите пароль")]
        [StringLength(50, MinimumLength = 6, ErrorMessage = "Длина пароля должна быть от 6 до 50 символов.")]
        [Compare("Password", ErrorMessage = "Пароли должны совпадать.")]
        public String ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Выберите страну.")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Выберите страну.")]
        public Int32 CountryId { get; set; }

        [Required(ErrorMessage = "Выберите город.")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Выберите город.")]
        public Int32 CityId { get; set; }
    }
}