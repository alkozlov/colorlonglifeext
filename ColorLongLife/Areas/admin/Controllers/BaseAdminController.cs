﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ColorLongLife.Areas.admin.Models.Common;
using ColorLongLife.DataAccessLayer;
using ColorLongLife.DataAccessLayer.Models;

namespace ColorLongLife.Areas.admin.Controllers
{
    public abstract class BaseAdminController : Controller
    {
        protected UnitOfWork UnitOfWork;

        protected BaseAdminController()
        {
            this.UnitOfWork = new UnitOfWork();
        }

        protected void ShowSuccessAlert(String title, String message)
        {
            this.ShowAlert(AlertType.Success, title, message);
        }

        protected void ShowErrorAlert(String title, String message)
        {
            this.ShowAlert(AlertType.Error, title, message);
        }

        protected void ShowInfoAlert(String title, String message)
        {
            this.ShowAlert(AlertType.Info, title, message);
        }

        protected void ShowWarningAlert(String title, String message)
        {
            this.ShowAlert(AlertType.Warning, title, message);
        }

        private void ShowAlert(AlertType alertType, String title, String message)
        {
            Alert alert = new Alert(alertType, title, message);
            this.TempData["alert"] = alert;
        }

        #region Helpers

        protected SelectList GetCountrySelectList()
        {
            IEnumerable<Country> countries = this.UnitOfWork.CountryRepository.Get();
            if (countries.Any())
            {
                return new SelectList(countries, "CountryId", "Name");
            }

            return new SelectList(new List<Country>());
        }

        protected SelectList GetCitiesSelectList(Int32 countryId)
        {
            IEnumerable<City> cities = this.UnitOfWork.CityRepository.Get(x => x.CountryId.Equals(countryId),
                query => query.OrderBy(x => x.Name));
            if (cities.Any())
            {
                return new SelectList(cities, "CityId", "Name");
            }

            return new SelectList(new List<City>());
        }
        #endregion

        protected User GetUser()
        {
            UnitOfWork unitOfWork = new UnitOfWork();
            var mainUser = HttpContext.User.Identity.Name;
            User user = UnitOfWork.UserRepository.Get(x => String.Compare(x.Email, mainUser, StringComparison.OrdinalIgnoreCase) == 0 && x.UserStatus != 3)
                    .FirstOrDefault();
            return user;
        }
    }
}