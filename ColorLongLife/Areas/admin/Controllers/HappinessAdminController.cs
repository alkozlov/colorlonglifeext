﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using ColorLongLife.Areas.admin.Models.Happiness;
using ColorLongLife.DataAccessLayer.Models;
using IndexModel = ColorLongLife.Areas.admin.Models.Happiness.IndexModel;

namespace ColorLongLife.Areas.admin.Controllers
{
    public class HappinessAdminController : BaseAdminController
    {
        private const Int32 MaxUploadImageSize = 5242880;

        // GET: admin/HappinessAdmin
        public ActionResult Index()
        {
            IndexModel model = new IndexModel();
            model.HappinessList = this.UnitOfWork.HappinessRepository.Get().Select(x => new HappinessModel
            {
                HappinessId = x.HappinessId,
                Title = x.Title,
                Description = x.Description,
                PathToImage = x.PathToImage
            });

            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            HappinessModel model = new HappinessModel();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(HappinessModel happiness)
        {
            if (this.ModelState.IsValid)
            {
                var existingHappiness =
                    this.UnitOfWork.HappinessRepository.Get(x => x.Title.ToLower().Equals(happiness.Title))
                        .SingleOrDefault();
                if (existingHappiness != null)
                {
                    base.ShowErrorAlert("Дубликат", "Данная позиция уже существует!");
                    return this.View(happiness);
                }

                if (this.HttpContext.Request.Files.Count == 0 ||
                    this.HttpContext.Request.Files[0] == null ||
                    this.HttpContext.Request.Files[0].ContentLength == 0 ||
                    this.HttpContext.Request.Files[0].ContentLength > MaxUploadImageSize)
                {
                    base.ShowErrorAlert("Ошибка", "Вы не выбрали изображение счастья или файл превысил добустимый размер в 5Mb!");
                    return View(happiness);
                }
                string folder = HttpContext.ApplicationInstance.Server.MapPath(@"\Content\storage");
                string fileName = HttpContext.Request.Files[0].FileName;
                string path = Path.Combine(folder, fileName);
                HttpContext.Request.Files[0].SaveAs(path);

                Happiness dataHappiness = new Happiness
                {
                    Title = happiness.Title,
                    Description = happiness.Description,
                    PathToImage = Path.Combine("/Content/storage", fileName)
                };

                this.UnitOfWork.HappinessRepository.Insert(dataHappiness);
                await this.UnitOfWork.SaveAsync();

                base.ShowSuccessAlert("Успешно", "Новоя позиция создана.");
                return RedirectToRoute("happiness");
            }

            base.ShowErrorAlert("Input error", "Plase check input fields.");
            return this.View(happiness);
        }

        [HttpGet]
        public ActionResult Edit(Int32 happinessId)
        {
            Happiness happiness = this.UnitOfWork.HappinessRepository.GetById(happinessId);
            if (happiness == null)
            {
                base.ShowErrorAlert("Ошибка", "Данные не найдены.");
                return RedirectToRoute("happiness");
            }

            HappinessModel model = new HappinessModel
            {
                HappinessId = happinessId,
                Title = happiness.Title,
                Description = happiness.Description,
                PathToImage = happiness.PathToImage
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(HappinessModel model)
        {
            if (!this.ModelState.IsValid)
            {
                base.ShowErrorAlert("Косяк", "Проверьте введенные данные.");
                return View(model);
            }

            Happiness happiness = this.UnitOfWork.HappinessRepository.GetById(model.HappinessId);
            if (happiness == null)
            {
                base.ShowErrorAlert("Косяк", "Данные в базе не найдены.");
                return View(model);
            }

            happiness.Title = model.Title;
            happiness.Description = model.Description;
            this.UnitOfWork.HappinessRepository.Update(happiness);
            await this.UnitOfWork.SaveAsync();

            base.ShowSuccessAlert("Успех", "Данные успешно обновлены.");
            return View(model);
        }

        public async Task<ActionResult> Delete(Int32 happinessId)
        {
            Happiness happiness = this.UnitOfWork.HappinessRepository.GetById(happinessId);
            if (happiness == null)
            {
                base.ShowErrorAlert("Ошибка", "Данные не найдены.");
                return RedirectToRoute("happiness");
            }

            this.UnitOfWork.HappinessRepository.Delete(happiness);
            await this.UnitOfWork.SaveAsync();

            base.ShowSuccessAlert("Успех", "Данные успешно удалены.");
            return RedirectToRoute("happiness");
        }
    }
}