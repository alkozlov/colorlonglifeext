﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ColorLongLife.Areas.admin.Models.Categories;
using ColorLongLife.DataAccessLayer.Models;

namespace ColorLongLife.Areas.admin.Controllers
{
    public class CategoriesAdminController : BaseAdminController
    {
        [HttpGet]
        public ActionResult Index()
        {
            IndexModel model = new IndexModel();
            model.Categories = this.UnitOfWork.CategoryRepository.Get()
                .OrderBy(x => x.Name)
                .Select(x => new CategoryModel
                {
                    CategoryId = x.IdCategory,
                    Name = x.Name,
                    Description = x.Description
                }).ToList();

            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            CategoryModel model = new CategoryModel();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CategoryModel model)
        {
            if (!ModelState.IsValid)
            {
                base.ShowErrorAlert("Ошибка", "Проверьте введенные данные!");
                return View(model);
            }

            Category existingCategory = this.UnitOfWork.CategoryRepository.Get(x => x.Name.Equals(model.Name)).FirstOrDefault();
            if (existingCategory != null)
            {
                base.ShowErrorAlert("Ошибка", "Такая категория уже есть!");
                return View(model);
            }

            Category category = new Category
            {
                Name = model.Name,
                Description = model.Description
            };
            this.UnitOfWork.CategoryRepository.Insert(category);
            await this.UnitOfWork.SaveAsync();

            base.ShowSuccessAlert("OK", "Категория успешно добавлена");
            return RedirectToRoute("categories");
        }

        [HttpGet]
        public async Task<ActionResult> Delete(Int32 categoryId)
        {
            Category category = this.UnitOfWork.CategoryRepository.Get(x => x.IdCategory.Equals(categoryId)).SingleOrDefault();
            if (category == null)
            {
                base.ShowErrorAlert("ERROR", "Категория не найдена!");
                return RedirectToRoute("categories");
            }

            this.UnitOfWork.CategoryRepository.Delete(category);
            await this.UnitOfWork.SaveAsync();
            base.ShowSuccessAlert("OK", "Категория успешно удалена!");

            return RedirectToRoute("categories");
        }

        [HttpGet]
        public ActionResult Edit(Int32 categoryId)
        {
            Category category = this.UnitOfWork.CategoryRepository.GetById(categoryId);
            if (category == null)
            {
                base.ShowErrorAlert("Ошибка", "Данные не найдены.");
                return RedirectToRoute("categories");
            }

            CategoryModel model = new CategoryModel
            {
                CategoryId = categoryId,
                Name = category.Name,
                Description = category.Description
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(CategoryModel model)
        {
            if (!this.ModelState.IsValid)
            {
                base.ShowErrorAlert("Косяк", "Проверьте введенные данные.");
                return View(model);
            }

            Category category = this.UnitOfWork.CategoryRepository.GetById(model.CategoryId);
            if (category == null)
            {
                base.ShowErrorAlert("Косяк", "Данные в базе не найдены.");
                return View(model);
            }

            category.Name = model.Name;
            category.Description = model.Description;
            this.UnitOfWork.CategoryRepository.Update(category);
            await this.UnitOfWork.SaveAsync();

            base.ShowSuccessAlert("Успех", "Данные успешно обновлены.");
            return View(model);
        }
    }
}