﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ColorLongLife.Areas.admin.Models.Home;

namespace ColorLongLife.Areas.admin.Controllers
{
    public class HomeAdminController : BaseAdminController
    {
        // GET: admin/Home
        public ActionResult Index()
        {
            IndexModel model = new IndexModel();

            return View(model);
        }
    }
}