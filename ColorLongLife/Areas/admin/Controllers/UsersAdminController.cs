﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using ColorLongLife.Areas.admin.Models.Users;
using ColorLongLife.DataAccessLayer.Models;
using ColorLongLife.Email;
using ColorLongLife.Utilities;
using IndexModel = ColorLongLife.Areas.admin.Models.Users.IndexModel;

namespace ColorLongLife.Areas.admin.Controllers
{
    public class UsersAdminController : BaseAdminController
    {
        // GET: admin/UserAdmin
        public ActionResult Index()
        {
            IndexModel model = new IndexModel();
            model.Users = this.UnitOfWork.UserRepository.Get().Select(x => new UserListItem
            {
                UserId = x.UserId,
                FirstName = x.FirstName,
                LastName = x.LastName,
                MiddleName = x.MiddleName,
                Email = x.Email
            });


            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            UserModel model = new UserModel();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(UserModel model)
        {
            SelectList tempList;

            // 1 - Валидация
            if (ModelState.IsValid)
            {
                // 2 - Проверяем на дублирование почты пользователя
                User user =
                    this.UnitOfWork.UserRepository.Get(x => x.Email.Equals(model.Email) && !x.IsDeleted)
                        .SingleOrDefault();

                if (user != null)
                {
                    ModelState.AddModelError("user",
                        "Пользователь с таким адресом электронной почты уже зарегистрирован.");
                    model.Password = string.Empty;
                    model.ConfirmPassword = string.Empty;
                    tempList = GetCountrySelectList();
                    ViewBag.Countries = tempList;
                    ViewBag.Cities = GetCitiesSelectList(Int32.Parse(tempList.First().Value));

                    return View(model);
                }

                //DateTime birthday;
                //var isDateValid = DateTime.TryParse(model.Birthday, out birthday);
                //if (!isDateValid) ModelState.AddModelError("Birthday", "Birthday needs to be a valid date.");


                // 3 - Валидация дня рождения
                DateTime currentDate = DateTime.UtcNow;
                if (model.Birthday < currentDate.AddYears(-100) || model.Birthday > currentDate.AddYears(-16))
                {
                    ModelState.AddModelError("model.Birthday", "Допустимый возраст пользователей от 16 и старше.");
                    model.Password = string.Empty;
                    model.ConfirmPassword = string.Empty;
                    tempList = GetCountrySelectList();
                    ViewBag.Countries = tempList;
                    ViewBag.Cities = GetCitiesSelectList(Int32.Parse(tempList.First().Value));

                    return View(model);
                }

                // 4 - Шифруем пароль
                String hashedPassword = CryptographyHelper.HashPassword(model.Password);

                // 5 - Генерируем ключ активации
                String activationToken = CryptographyHelper.GenerateActivationToken();

                // 6 - Создаем пользователя и сохраняем его в БД
                User dataUser = new User
                {
                    FirstName = model.FirstName,
                    MiddleName = model.MiddleName,
                    LastName = model.LastName,
                    CountryId = model.CountryId,
                    CityId = model.CityId,
                    Birthday = model.Birthday,
                    CreateDate = DateTime.UtcNow,
                    Email = model.Email,
                    ConfirmationToken = activationToken,
                    UserPasswordHash = hashedPassword,
                    Gender = (int) model.Gender,
                    Growth = model.Growth,
                    Weight = model.Weight,
                    IsDeleted = false,
                    UserAccountType = (int) AccountType.Customer,
                    UserStatus = (int) AccountStatus.PrepareActivation,
                    LastVisitDate = DateTime.UtcNow,
                    Phone = model.Phone
                };
                this.UnitOfWork.UserRepository.Insert(dataUser);
                await this.UnitOfWork.SaveAsync();

                // 7 - Отправляем письмо для подтверждения регистрации
                if (HttpContext.Request.Url != null)
                {
                    String siteUrl = HttpContext.Request.Url.AbsoluteUri.Replace(HttpContext.Request.Url.AbsolutePath, "");
                    BaseMailMessage mailMessage =
                        new ConfirmRegistrationMailMessage(dataUser, siteUrl);
                    await Emailer.SendMailAsync(mailMessage);
                }

                return RedirectToAction("RegistrationSuccess");
            }
            tempList = GetCountrySelectList();
            ViewBag.Countries = tempList;
            ViewBag.Cities = GetCitiesSelectList(Int32.Parse(tempList.First().Value));

            model.Password = string.Empty;
            model.ConfirmPassword = string.Empty;

            base.ShowSuccessAlert("OK", "Пользователь успешно добавлен");
            return RedirectToRoute("users");
        }

        [HttpGet]
        public async Task<ActionResult> Delete(Int32 userId)
        {
            User user = this.UnitOfWork.UserRepository.Get(x => x.UserId.Equals(userId)).SingleOrDefault();
            if (user == null)
            {
                base.ShowErrorAlert("ERROR", "Категория не найдена!");
                return RedirectToRoute("users");
            }

            this.UnitOfWork.UserRepository.Delete(user);
            await this.UnitOfWork.SaveAsync();
            base.ShowSuccessAlert("OK", "Категория успешно удалена!");

            return RedirectToRoute("users");
        }

        [HttpGet]
        public ActionResult Edit(Int32 userId)
        {
            User user = this.UnitOfWork.UserRepository.GetById(userId);
            if (user == null)
            {
                base.ShowErrorAlert("Ошибка", "Данные не найдены.");
                return RedirectToRoute("users");
            }

            UserModel model = new UserModel
            {
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                MiddleName = user.MiddleName,
                CountryId = (int) user.CountryId,
                CityId = (int) user.CityId,
                Birthday = user.Birthday,
                Gender = (Gender) user.Gender,
                Growth = user.Growth,
                Weight = user.Weight,
                Phone = user.Phone
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(UserModel model)
        {
            if (!this.ModelState.IsValid)
            {
                base.ShowErrorAlert("Косяк", "Проверьте введенные данные.");
                return View(model);
            }

            User user = UnitOfWork.UserRepository.Get(x=>x.Email.Equals(model.Email)).FirstOrDefault();
            if (user == null)
            {
                base.ShowErrorAlert("Косяк", "Данные в базе не найдены.");
                return View(model);
            }

            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.MiddleName = model.MiddleName;
            user.CountryId = model.CountryId;
            user.CityId = model.CityId;
            user.Birthday = model.Birthday;
            user.CreateDate = DateTime.UtcNow;
            user.Email = model.Email;
            //user.ConfirmationToken = activationToken;
            //user.UserPasswordHash = hashedPassword;
            user.Gender = (int) model.Gender;
            user.Growth = model.Growth;
            user.Weight = model.Weight;
            user.IsDeleted = false;
            user.UserAccountType = (int) AccountType.Customer;
            user.UserStatus = (int) AccountStatus.PrepareActivation;
            user.LastVisitDate = DateTime.UtcNow;
            user.Phone = model.Phone;

            this.UnitOfWork.UserRepository.Update(user);
            await this.UnitOfWork.SaveAsync();

            base.ShowSuccessAlert("Успех", "Данные успешно обновлены.");
            return View(model);
        }

        public ActionResult RegistrationSuccess()
        {
            return View();
        }
    }
}