﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using ColorLongLife.Areas.admin.Models.Positions;
using ColorLongLife.DataAccessLayer.Models;

namespace ColorLongLife.Areas.admin.Controllers
{
    public class PositionsAdminController : BaseAdminController
    {
        [HttpGet]
        // GET: admin/PositionsAdmin
        public ActionResult Index()
        {
            IndexModel model = new IndexModel();
            model.Positions = this.UnitOfWork.PositionRepository.Get().Select(x => new PositionModel
            {
                PositionId = x.PositionId,
                Title = x.TitlePosition,
                Description = x.Description
            });

            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            PositionModel model = new PositionModel();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(PositionModel position)
        {   
            if (this.ModelState.IsValid)
            {
                var existingPosition =
                    this.UnitOfWork.PositionRepository.Get(x => x.TitlePosition.ToLower().Equals(position.Title))
                        .SingleOrDefault();
                if (existingPosition != null)
                {
                    base.ShowErrorAlert("Duplicate", "That position already exists!");
                    return this.View(position);
                }

                Position dataPosition = new Position
                {
                    TitlePosition = position.Title,
                    Description = position.Description
                };
                this.UnitOfWork.PositionRepository.Insert(dataPosition);
                await this.UnitOfWork.SaveAsync();

                base.ShowSuccessAlert("Success", "New position successfully created.");
                return RedirectToRoute("positions");
            }

            base.ShowErrorAlert("Input error", "Plase check input fields.");
            return this.View(position);
        }

        [HttpGet]
        public ActionResult Edit(Int32 positionId)
        {
            Position position = this.UnitOfWork.PositionRepository.GetById(positionId);
            if (position == null)
            {
                base.ShowErrorAlert("Ошибка", "Данные не найдены.");
                return RedirectToRoute("positions");
            }

            PositionModel model = new PositionModel
            {
                PositionId = positionId,
                Title = position.TitlePosition,
                Description = position.Description
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(PositionModel model)
        {
            if (!this.ModelState.IsValid)
            {
                base.ShowErrorAlert("Косяк", "Проверьте введенные данные.");
                return View(model);
            }

            Position position = this.UnitOfWork.PositionRepository.GetById(model.PositionId);
            if (position == null)
            {
                base.ShowErrorAlert("Косяк", "Данные в базе не найдены.");
                return View(model);
            }

            position.TitlePosition = model.Title;
            position.Description = model.Description;
            this.UnitOfWork.PositionRepository.Update(position);
            await this.UnitOfWork.SaveAsync();

            base.ShowSuccessAlert("Успех", "Данные успешно обновлены.");
            return View(model);
        }

        public async Task<ActionResult> Delete(Int32 positionId)
        {
            Position position = this.UnitOfWork.PositionRepository.GetById(positionId);
            if (position == null)
            {
                base.ShowErrorAlert("Ошибка", "Данные не найдены.");
                return RedirectToRoute("positions");
            }

            this.UnitOfWork.PositionRepository.Delete(position);
            await this.UnitOfWork.SaveAsync();

            base.ShowSuccessAlert("Успех", "Данные успешно удалены.");
            return RedirectToRoute("positions");
        }
    }
}