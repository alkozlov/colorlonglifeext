﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ColorLongLife.Areas.admin.Models.ConsultationSettings;
using ColorLongLife.DataAccessLayer.Models;

namespace ColorLongLife.Areas.admin.Controllers
{
    public class ConsultationSettingsAdminController : BaseAdminController
    {
        #region Default settings

        private readonly TimeSpan DefaultConsultationsStartTime = new TimeSpan(9, 0, 0);
        private readonly TimeSpan DefaultConsultationsEndTime = new TimeSpan(18, 0, 0);
        private readonly TimeSpan DefaultConsultationDuration = new TimeSpan(0, 30, 0);
        private readonly TimeSpan DefaultRest = new TimeSpan(0, 30, 0);

        private readonly SelectList DefaultConsultationDurationList = new SelectList(new List<TimeSpan>
        {
            new TimeSpan(0, 15, 0),
            new TimeSpan(0, 30, 0),
            new TimeSpan(0, 45, 0)
        });

        private readonly SelectList DefaultRestDurationList = new SelectList(new List<TimeSpan>
        {
            new TimeSpan(0, 15, 0),
            new TimeSpan(0, 30, 0),
            new TimeSpan(0, 45, 0)
        });

        private readonly SelectList ApplyForList = new SelectList(new List<Int32> { 7, 30 });

        #endregion

        [HttpGet]
        public ActionResult Index(DateTime? startDate)
        {
            IndexModel model = new IndexModel();
            DateTime compareDate = startDate ?? DateTime.UtcNow.Date;
            List<ConsultationSettingsModel> settings =
                this.UnitOfWork.ConsultationsSettingsRepository.Get(x => DbFunctions.TruncateTime(x.Date) >= compareDate).Select(x => new ConsultationSettingsModel
                {
                    ConsultationSettingsId = x.ConsultationSettingsId,
                    Date = x.Date,
                    StartTime = x.StartTime,
                    EndTime = x.EndTime,
                    TimeslotDuration = x.TimeslotDuration,
                    RestDuration = x.RestDuration,
                    IsActive = x.IsActive
                }).ToList();
            model.ConsultationSettings = settings;

            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            ConsultationSettingsModel model = new ConsultationSettingsModel
            {
                Date = DateTime.UtcNow.Date,
                StartTime = this.DefaultConsultationsStartTime,
                EndTime = this.DefaultConsultationsEndTime,
                TimeslotDuration = this.DefaultConsultationDuration,
                RestDuration = this.DefaultRest,
                ApplyFor = 7
            };

            ViewBag.ConsultationDuration = this.DefaultConsultationDurationList;
            ViewBag.RestDuration = this.DefaultRestDurationList;
            ViewBag.ApplyForList = this.ApplyForList;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(ConsultationSettingsModel model)
        {
            if (!ModelState.IsValid)
            {
                base.ShowErrorAlert("Ошибка", "Проверьте введенные данные");
                ViewBag.ConsultationDuration = this.DefaultConsultationDurationList;
                ViewBag.RestDuration = this.DefaultRestDurationList;
                ViewBag.ApplyForList = this.ApplyForList;
                return View(model);
            }

            DateTime startPeriod = model.Date;
            DateTime endPeriod = model.Date.AddDays(model.ApplyFor).Date;
            var existingSettings =
                this.UnitOfWork.ConsultationsSettingsRepository.Get(
                    x =>
                        DbFunctions.TruncateTime(x.Date) >= startPeriod && DbFunctions.TruncateTime(x.Date) <= endPeriod)
                    .ToList();

            List<ConsultationSettings> settingToInserts = new List<ConsultationSettings>();
            List<ConsultationSettings> settingsToUpdate = new List<ConsultationSettings>();
            for (int i = 0; i < model.ApplyFor; i++)
            {
                DateTime settingsDate = model.Date.AddDays(i).Date;
                if (existingSettings.Any(x => x.Date == settingsDate))
                {
                    ConsultationSettings existingSetting = existingSettings.First(x => x.Date == settingsDate);
                    existingSetting.StartTime = model.StartTime;
                    existingSetting.EndTime = model.EndTime;
                    existingSetting.TimeslotDuration = model.TimeslotDuration;
                    existingSetting.RestDuration = model.RestDuration;
                    settingsToUpdate.Add(existingSetting);
                }
                else
                {
                    settingToInserts.Add(new ConsultationSettings
                    {
                        Date = model.Date.AddDays(i),
                        StartTime = model.StartTime,
                        EndTime = model.EndTime,
                        TimeslotDuration = model.TimeslotDuration,
                        RestDuration = model.RestDuration,
                        IsActive = true
                    });
                }
            }

            this.UnitOfWork.ConsultationsSettingsRepository.InsertRange(settingToInserts);
            foreach (var entity in settingsToUpdate)
            {
                this.UnitOfWork.ConsultationsSettingsRepository.Update(entity);
            }
            await this.UnitOfWork.SaveAsync();

            base.ShowSuccessAlert("OK", "Данные успешно добавлены.");
            return RedirectToRoute("consultationsettings");
        }

        public async Task<ActionResult> Delete(Int32 settingsId)
        {
            if (settingsId < 0)
            {
                base.ShowErrorAlert("Ошибка", "Неверные данные.");
                return RedirectToRoute("consultationsettings");
            }

            ConsultationSettings consultationSettings =
                this.UnitOfWork.ConsultationsSettingsRepository.Get(x => x.ConsultationSettingsId.Equals(settingsId))
                    .FirstOrDefault();
            if (consultationSettings == null)
            {
                base.ShowErrorAlert("Ошибка", "Информация не найдена.");
                return RedirectToRoute("consultationsettings");
            }

            this.UnitOfWork.ConsultationsSettingsRepository.Delete(consultationSettings);
            await this.UnitOfWork.SaveAsync();

            base.ShowSuccessAlert("OK", "Данные успешно удалены.");
            return RedirectToRoute("consultationsettings");
        }
    }
}