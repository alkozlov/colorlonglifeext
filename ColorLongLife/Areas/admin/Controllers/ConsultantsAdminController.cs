﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ColorLongLife.Areas.admin.Models.Consultants;
using ColorLongLife.DataAccessLayer.Models;
using ColorLongLife.Files;

namespace ColorLongLife.Areas.admin.Controllers
{
    public class ConsultantsAdminController : BaseAdminController
    {
        private const Int32 MaxUploadImageSize = 5242880;

        private IFileService _fileService;

        public ConsultantsAdminController()
        {
            this._fileService = new LiteDbFileService();
        }

        // GET: admin/ConsultantsAdmin
        public ActionResult Index()
        {
            IndexModel model = new IndexModel();
            model.Consultants = this.UnitOfWork.DoctorRepository.Get(null, null, "Position").Select(x => new ConsultantModel
            {
                DoctorId = x.DoctorId,
                FirstName = x.FirstName,
                LastName = x.LastName,
                MiddleName = x.MiddleName,
                Description = x.Description,
                PositionName = x.Position.TitlePosition,
                PathToImage = this._fileService.GetFileAccessUrl(x.PhotoStorageKey)
            }).ToList();

            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            ConsultantModel model = new ConsultantModel();
            var positions = this.UnitOfWork.PositionRepository.Get().OrderBy(x => x.TitlePosition);
            this.ViewBag.Positions = new SelectList(positions, "PositionId", "TitlePosition");

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(ConsultantModel consultant)
        {
            if (!this.ModelState.IsValid)
            {
                var positions = this.UnitOfWork.PositionRepository.Get().OrderBy(x => x.TitlePosition);
                this.ViewBag.Positions = new SelectList(positions, "PositionId", "TitlePosition");

                base.ShowErrorAlert("Ошибка", "Проверьте введенные данные!");
                return View(consultant);
            }

            if (this.HttpContext.Request.Files.Count == 0 ||
                this.HttpContext.Request.Files[0] == null ||
                this.HttpContext.Request.Files[0].ContentLength == 0 ||
                this.HttpContext.Request.Files[0].ContentLength > MaxUploadImageSize)
            {
                var positions = this.UnitOfWork.PositionRepository.Get().OrderBy(x => x.TitlePosition);
                this.ViewBag.Positions = new SelectList(positions, "PositionId", "TitlePosition");

                base.ShowErrorAlert("Ошибка", "Вы не выбрали фото специалиста или файл превысил добустимый размер в 5Mb!");
                return View(consultant);
            }

            // Save file and get key
            HttpPostedFileBase file = this.HttpContext.Request.Files[0];
            String storageKey = this._fileService.StoreFile(file.InputStream, file.FileName);

            Doctor doctor = new Doctor
            {
                FirstName = consultant.FirstName,
                MiddleName = consultant.MiddleName,
                LastName = consultant.LastName,
                PositionId = consultant.PositionId,
                Description = consultant.Description,
                PhotoStorageKey = storageKey
            };
            this.UnitOfWork.DoctorRepository.Insert(doctor);
            await this.UnitOfWork.SaveAsync();

            base.ShowSuccessAlert("OK", "Доктор успешно добавлен");
            return RedirectToRoute("consultants");
        }

        [HttpGet]
        public async Task<ActionResult> Delete(Int32 consultantId)
        {
            Doctor doctor = this.UnitOfWork.DoctorRepository.Get(x => x.DoctorId.Equals(consultantId)).SingleOrDefault();
            if (doctor == null)
            {
                base.ShowErrorAlert("ERROR", "Доктор не найден!");
                return RedirectToRoute("consultants");
            }

            this._fileService.DeleteFile(doctor.PhotoStorageKey);
            this.UnitOfWork.DoctorRepository.Delete(doctor);
            await this.UnitOfWork.SaveAsync();

            base.ShowSuccessAlert("OK", "Доктор успешно удален");
            return RedirectToRoute("consultants");
        }
    }
}