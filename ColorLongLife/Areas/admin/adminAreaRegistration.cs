﻿using System.Web.Mvc;

namespace ColorLongLife.Areas.admin
{
    public class adminAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            #region Happiness

            context.MapRoute(
                "happiness",
                "admin/database/happiness",
                new { controller = "HappinessAdmin", action = "Index" }
                );

            context.MapRoute(
                "happiness_create",
                "admin/database/happiness/create",
                new { controller = "HappinessAdmin", action = "Create" }
                );

            context.MapRoute(
                "happiness_edit",
                "admin/database/happiness/{happinessId}/edit",
                new { controller = "HappinessAdmin", action = "Edit", userId = UrlParameter.Optional }
                );

            context.MapRoute(
                "happiness_delete",
                "admin/database/happiness/{happinessId}/delete",
                new { controller = "HappinessAdmin", action = "Delete", userId = UrlParameter.Optional }
                );

            #endregion

            #region Users

            context.MapRoute(
                "users",
                "admin/database/users",
                new { controller = "UsersAdmin", action = "Index" }
                );

            context.MapRoute(
                "users_create",
                "admin/database/users/create",
                new { controller = "UsersAdmin", action = "Create" }
                );

            context.MapRoute(
                "users_edit",
                "admin/database/users/{userId}/edit",
                new { controller = "UsersAdmin", action = "Edit", userId = UrlParameter.Optional }
                );

            context.MapRoute(
                "users_delete",
                "admin/database/users/{userId}/delete",
                new { controller = "UsersAdmin", action = "Delete", userId = UrlParameter.Optional }
                );

            #endregion

            #region Positions

            context.MapRoute(
                "positions_create",
                "admin/database/positions/create",
                new { controller = "PositionsAdmin", action = "Create" }
                );

            context.MapRoute(
                "positions_edit",
                "admin/database/positions/{positionId}/edit",
                new { controller = "PositionsAdmin", action = "Edit", positionId = UrlParameter.Optional }
                );

            context.MapRoute(
                "positions_delete",
                "admin/database/positions/{positionId}/delete",
                new { controller = "PositionsAdmin", action = "Delete", positionId = UrlParameter.Optional }
                );

            context.MapRoute(
                "positions",
                "admin/database/positions",
                new { controller = "PositionsAdmin", action = "Index" }
                );

            #endregion

            #region Consultants

            context.MapRoute(
                "consultants_create",
                "admin/database/consultants/create",
                new { controller = "ConsultantsAdmin", action = "Create" }
                );

            context.MapRoute(
                "consultants_edit",
                "admin/database/consultants/{consultantId}/edit",
                new { controller = "ConsultantsAdmin", action = "Edit", consultantId = UrlParameter.Optional }
                );

            context.MapRoute(
                "consultants_delete",
                "admin/database/consultants/{consultantId}/delete",
                new { controller = "ConsultantsAdmin", action = "Delete", consultantId = UrlParameter.Optional }
                );

            context.MapRoute(
                "consultants",
                "admin/database/consultants",
                new { controller = "ConsultantsAdmin", action = "Index" }
                );

            #endregion

            #region Сonsultation settings

            context.MapRoute(
                "consultationsettings_create",
                "admin/database/consultationsettings/create",
                new { controller = "ConsultationSettingsAdmin", action = "Create" }
                );

            context.MapRoute(
                "consultationsettings_edit",
                "admin/database/consultationsettings/{settingsId}/edit",
                new { controller = "ConsultationSettingsAdmin", action = "Edit", settingsId = UrlParameter.Optional }
                );

            context.MapRoute(
                "consultationsettings_delete",
                "admin/database/consultationsettings/{settingsId}/delete",
                new { controller = "ConsultationSettingsAdmin", action = "Delete", settingsId = UrlParameter.Optional }
                );

            context.MapRoute(
                "consultationsettings",
                "admin/database/consultationsettings",
                new { controller = "ConsultationSettingsAdmin", action = "Index" }
                );

            #endregion

            #region Categories

            context.MapRoute(
                "categories_create",
                "admin/database/categories/create",
                new { controller = "CategoriesAdmin", action = "Create" }
                );

            context.MapRoute(
                "categories_edit",
                "admin/database/categories/{categoryId}/edit",
                new { controller = "CategoriesAdmin", action = "Edit", categoryId = UrlParameter.Optional }
                );

            context.MapRoute(
                "categories_delete",
                "admin/database/categories/{categoryId}/delete",
                new { controller = "CategoriesAdmin", action = "Delete", categoryId = UrlParameter.Optional }
                );

            context.MapRoute(
                "categories",
                "admin/database/categories",
                new { controller = "CategoriesAdmin", action = "Index" }
                );

            #endregion

            context.MapRoute(
                "admin_default",
                "admin/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}